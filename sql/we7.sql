-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: we7
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ims_account`
--

DROP TABLE IF EXISTS `ims_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account` (
  `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `hash` varchar(8) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `isconnect` tinyint(4) NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account`
--

LOCK TABLES `ims_account` WRITE;
/*!40000 ALTER TABLE `ims_account` DISABLE KEYS */;
INSERT INTO `ims_account` VALUES (2,2,'YAasZ9C9',1,1);
/*!40000 ALTER TABLE `ims_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_account_wechats`
--

DROP TABLE IF EXISTS `ims_account_wechats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account_wechats` (
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `encodingaeskey` varchar(255) NOT NULL,
  `access_token` varchar(1000) NOT NULL,
  `jsapi_ticket` varchar(1000) NOT NULL,
  `level` tinyint(4) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `account` varchar(30) NOT NULL,
  `original` varchar(50) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `country` varchar(10) NOT NULL,
  `province` varchar(3) NOT NULL,
  `city` varchar(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `subscribeurl` varchar(120) NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account_wechats`
--

LOCK TABLES `ims_account_wechats` WRITE;
/*!40000 ALTER TABLE `ims_account_wechats` DISABLE KEYS */;
INSERT INTO `ims_account_wechats` VALUES (2,2,'ONBOsrobRpsg01z44tr40ZN1455111VT','BqD4h5ss564Su65I336X66I6QDGiQRkqu65s66uHGKS','a:2:{s:5:\"token\";s:107:\"VR-FSXvfWmKX2DhodwsjQb-Ol6cAW-lSI4-zV3ykZl-jxGtbfbYjIiWBZR7ypSO_YjQpOuAp-iEvX_vMXPuXoBFm-5ZB75ivtOfr7E7DX5E\";s:6:\"expire\";i:1423991353;}','a:2:{s:6:\"ticket\";s:86:\"sM4AOVdWfPE4DxkXGEs8VANf7M0B3wOxaGNz7hcofIVswgzsAA44i1rHeamdM5UYLo0-lsfJcEUXM4bcHuNS9g\";s:6:\"expire\";i:1423991353;}',4,'维客旺CRM','vikvon','gh_222b83f57fb3','','','','','','',0,'wxff1e4034f5f9f549','83276e5bcb1026951d6d5db655956b70',0,'');
/*!40000 ALTER TABLE `ims_account_wechats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_account_yixin`
--

DROP TABLE IF EXISTS `ims_account_yixin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account_yixin` (
  `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `access_token` varchar(1000) NOT NULL,
  `level` tinyint(4) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `account` varchar(30) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account_yixin`
--

LOCK TABLES `ims_account_yixin` WRITE;
/*!40000 ALTER TABLE `ims_account_yixin` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_account_yixin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon`
--

DROP TABLE IF EXISTS `ims_activity_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon` (
  `couponid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(30) NOT NULL,
  `couponsn` varchar(50) NOT NULL,
  `description` text,
  `discount` decimal(10,2) NOT NULL,
  `condition` decimal(10,2) NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL,
  `limit` int(11) NOT NULL,
  `dosage` int(11) unsigned NOT NULL,
  `amount` int(11) unsigned NOT NULL,
  `thumb` varchar(500) NOT NULL,
  `credit` int(10) unsigned NOT NULL,
  `credittype` varchar(20) NOT NULL,
  PRIMARY KEY (`couponid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon`
--

LOCK TABLES `ims_activity_coupon` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_allocation`
--

DROP TABLE IF EXISTS `ims_activity_coupon_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_allocation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `couponid` int(10) unsigned NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`couponid`,`groupid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_allocation`
--

LOCK TABLES `ims_activity_coupon_allocation` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_password`
--

DROP TABLE IF EXISTS `ims_activity_coupon_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_password` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_password`
--

LOCK TABLES `ims_activity_coupon_password` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_record`
--

DROP TABLE IF EXISTS `ims_activity_coupon_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_record` (
  `recid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `grantmodule` varchar(50) NOT NULL,
  `granttime` int(10) unsigned NOT NULL,
  `usemodule` varchar(50) NOT NULL,
  `usetime` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `operator` varchar(30) NOT NULL,
  `remark` varchar(300) NOT NULL,
  `couponid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`recid`),
  KEY `couponid` (`uid`,`grantmodule`,`usemodule`,`status`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_record`
--

LOCK TABLES `ims_activity_coupon_record` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange`
--

DROP TABLE IF EXISTS `ims_activity_exchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `couponid` int(10) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `thumb` varchar(500) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `extra` varchar(3000) NOT NULL,
  `credit` int(10) unsigned NOT NULL,
  `credittype` varchar(10) NOT NULL,
  `pretotal` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange`
--

LOCK TABLES `ims_activity_exchange` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange_trades`
--

DROP TABLE IF EXISTS `ims_activity_exchange_trades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange_trades` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `uniacid` (`uniacid`,`uid`,`exid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange_trades`
--

LOCK TABLES `ims_activity_exchange_trades` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange_trades` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange_trades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange_trades_shipping`
--

DROP TABLE IF EXISTS `ims_activity_exchange_trades_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange_trades_shipping` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `district` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `uniacid` (`uniacid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange_trades_shipping`
--

LOCK TABLES `ims_activity_exchange_trades_shipping` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange_trades_shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange_trades_shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_modules`
--

DROP TABLE IF EXISTS `ims_activity_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_modules` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `available` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `uniacid` (`uniacid`),
  KEY `module` (`module`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_modules`
--

LOCK TABLES `ims_activity_modules` WRITE;
/*!40000 ALTER TABLE `ims_activity_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_modules_record`
--

DROP TABLE IF EXISTS `ims_activity_modules_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_modules_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(10) unsigned NOT NULL,
  `num` tinyint(3) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_modules_record`
--

LOCK TABLES `ims_activity_modules_record` WRITE;
/*!40000 ALTER TABLE `ims_activity_modules_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_modules_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_article_reply`
--

DROP TABLE IF EXISTS `ims_article_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_article_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `isfill` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_article_reply`
--

LOCK TABLES `ims_article_reply` WRITE;
/*!40000 ALTER TABLE `ims_article_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_article_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_basic_reply`
--

DROP TABLE IF EXISTS `ims_basic_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_basic_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `content` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_basic_reply`
--

LOCK TABLES `ims_basic_reply` WRITE;
/*!40000 ALTER TABLE `ims_basic_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_basic_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_business`
--

DROP TABLE IF EXISTS `ims_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `weid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `province` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `lng` varchar(10) NOT NULL,
  `lat` varchar(10) NOT NULL,
  `industry1` varchar(10) NOT NULL,
  `industry2` varchar(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lat_lng` (`lng`,`lat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_business`
--

LOCK TABLES `ims_business` WRITE;
/*!40000 ALTER TABLE `ims_business` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_business` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_attachment`
--

DROP TABLE IF EXISTS `ims_core_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_attachment`
--

LOCK TABLES `ims_core_attachment` WRITE;
/*!40000 ALTER TABLE `ims_core_attachment` DISABLE KEYS */;
INSERT INTO `ims_core_attachment` VALUES (1,2,1,'fenxiao.jpg','images/2/2015/02/XcU88Cp8PufmAaSBmXsOoicasciDzZ.jpg',1,1423873535),(2,2,1,'fenxiao.jpg','images/2/2015/02/HYf1jSE1Of6zEMMGMeE266pE9SEcO6.jpg',1,1423873543);
/*!40000 ALTER TABLE `ims_core_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_cache`
--

DROP TABLE IF EXISTS `ims_core_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_cache` (
  `key` varchar(50) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_cache`
--

LOCK TABLES `ims_core_cache` WRITE;
/*!40000 ALTER TABLE `ims_core_cache` DISABLE KEYS */;
INSERT INTO `ims_core_cache` VALUES ('setting','a:3:{s:8:\"authmode\";i:1;s:5:\"close\";a:2:{s:6:\"status\";s:1:\"0\";s:6:\"reason\";s:0:\"\";}s:8:\"register\";a:4:{s:4:\"open\";i:1;s:6:\"verify\";i:0;s:4:\"code\";i:1;s:7:\"groupid\";i:1;}}'),('menus:platform','a:0:{}'),('menus:site','a:0:{}'),('modules','a:11:{s:5:\"basic\";a:16:{s:3:\"mid\";s:1:\"1\";s:4:\"name\";s:5:\"basic\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本文字回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:24:\"和您进行简单对话\";s:11:\"description\";s:201:\"一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:4:\"news\";a:16:{s:3:\"mid\";s:1:\"2\";s:4:\"name\";s:4:\"news\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:24:\"基本混合图文回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:33:\"为你提供生动的图文资讯\";s:11:\"description\";s:272:\"一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"music\";a:16:{s:3:\"mid\";s:1:\"3\";s:4:\"name\";s:5:\"music\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本音乐回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:39:\"提供语音、音乐等音频类回复\";s:11:\"description\";s:183:\"在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:7:\"userapi\";a:16:{s:3:\"mid\";s:1:\"4\";s:4:\"name\";s:7:\"userapi\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:21:\"自定义接口回复\";s:7:\"version\";s:3:\"1.1\";s:7:\"ability\";s:33:\"更方便的第三方接口设置\";s:11:\"description\";s:141:\"自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:8:\"recharge\";a:16:{s:3:\"mid\";s:1:\"5\";s:4:\"name\";s:8:\"recharge\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:24:\"会员中心充值模块\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:24:\"提供会员充值功能\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:6:\"custom\";a:16:{s:3:\"mid\";s:1:\"6\";s:4:\"name\";s:6:\"custom\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:15:\"多客服转接\";s:7:\"version\";s:5:\"1.0.0\";s:7:\"ability\";s:36:\"用来接入腾讯的多客服系统\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:17:\"http://bbs.we7.cc\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";a:0:{}s:7:\"handles\";a:6:{i:0;s:5:\"image\";i:1;s:5:\"voice\";i:2;s:5:\"video\";i:3;s:8:\"location\";i:4;s:4:\"link\";i:5;s:4:\"text\";}s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:6:\"images\";a:16:{s:3:\"mid\";s:1:\"7\";s:4:\"name\";s:6:\"images\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本图片回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供图片回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"video\";a:16:{s:3:\"mid\";s:1:\"8\";s:4:\"name\";s:5:\"video\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本视频回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供图片回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"voice\";a:16:{s:3:\"mid\";s:1:\"9\";s:4:\"name\";s:5:\"voice\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本语音回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供语音回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"chats\";a:16:{s:3:\"mid\";s:2:\"10\";s:4:\"name\";s:5:\"chats\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"发送客服消息\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:77:\"公众号可以在粉丝最后发送消息的48小时内无限制发送消息\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:4:\"sale\";a:16:{s:3:\"mid\";s:2:\"11\";s:4:\"name\";s:4:\"sale\";s:4:\"type\";s:8:\"business\";s:5:\"title\";s:12:\"分销系统\";s:7:\"version\";s:3:\"3.2\";s:7:\"ability\";s:12:\"分销系统\";s:11:\"description\";s:12:\"分销系统\";s:6:\"author\";s:0:\"\";s:3:\"url\";s:0:\"\";s:8:\"settings\";s:1:\"1\";s:10:\"subscribes\";a:0:{}s:7:\"handles\";a:1:{i:0;s:4:\"text\";}s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"0\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}}'),('usersfields','a:51:{i:0;s:3:\"uid\";i:1;s:7:\"uniacid\";i:2;s:6:\"mobile\";i:3;s:5:\"email\";i:4;s:8:\"password\";i:5;s:4:\"salt\";i:6;s:7:\"groupid\";i:7;s:7:\"credit1\";i:8;s:7:\"credit2\";i:9;s:7:\"credit3\";i:10;s:7:\"credit4\";i:11;s:7:\"credit5\";i:12;s:10:\"createtime\";i:13;s:8:\"realname\";i:14;s:8:\"nickname\";i:15;s:6:\"avatar\";i:16;s:2:\"qq\";i:17;s:3:\"vip\";i:18;s:6:\"gender\";i:19;s:9:\"birthyear\";i:20;s:10:\"birthmonth\";i:21;s:8:\"birthday\";i:22;s:13:\"constellation\";i:23;s:6:\"zodiac\";i:24;s:9:\"telephone\";i:25;s:6:\"idcard\";i:26;s:9:\"studentid\";i:27;s:5:\"grade\";i:28;s:7:\"address\";i:29;s:7:\"zipcode\";i:30;s:11:\"nationality\";i:31;s:14:\"resideprovince\";i:32;s:10:\"residecity\";i:33;s:10:\"residedist\";i:34;s:14:\"graduateschool\";i:35;s:7:\"company\";i:36;s:9:\"education\";i:37;s:10:\"occupation\";i:38;s:8:\"position\";i:39;s:7:\"revenue\";i:40;s:15:\"affectivestatus\";i:41;s:10:\"lookingfor\";i:42;s:9:\"bloodtype\";i:43;s:6:\"height\";i:44;s:6:\"weight\";i:45;s:6:\"alipay\";i:46;s:3:\"msn\";i:47;s:6:\"taobao\";i:48;s:4:\"site\";i:49;s:3:\"bio\";i:50;s:8:\"interest\";}');
/*!40000 ALTER TABLE `ims_core_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_paylog`
--

DROP TABLE IF EXISTS `ims_core_paylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_paylog` (
  `plid` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `tid` varchar(64) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `tag` varchar(2000) NOT NULL,
  `paymentsn` varchar(145),
  PRIMARY KEY (`plid`),
  KEY `idx_openid` (`openid`),
  KEY `idx_tid` (`tid`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=2015021411 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_paylog`
--

LOCK TABLES `ims_core_paylog` WRITE;
/*!40000 ALTER TABLE `ims_core_paylog` DISABLE KEYS */;
INSERT INTO `ims_core_paylog` VALUES (2015021408,'alipay',2,'1','4',8898.00,0,'sale','a:1:{s:4:\"acid\";i:2;}','');
/*!40000 ALTER TABLE `ims_core_paylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_performance`
--

DROP TABLE IF EXISTS `ims_core_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_performance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  `runtime` varchar(10) NOT NULL,
  `runurl` varchar(512) NOT NULL,
  `runsql` varchar(512) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_performance`
--

LOCK TABLES `ims_core_performance` WRITE;
/*!40000 ALTER TABLE `ims_core_performance` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_queue`
--

DROP TABLE IF EXISTS `ims_core_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_queue` (
  `qid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `message` varchar(2000) NOT NULL,
  `params` varchar(1000) NOT NULL,
  `keyword` varchar(1000) NOT NULL,
  `response` varchar(2000) NOT NULL,
  `module` varchar(50) NOT NULL,
  `dateline` int(10) unsigned NOT NULL,
  PRIMARY KEY (`qid`),
  KEY `uniacid` (`uniacid`,`acid`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_queue`
--

LOCK TABLES `ims_core_queue` WRITE;
/*!40000 ALTER TABLE `ims_core_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_resource`
--

DROP TABLE IF EXISTS `ims_core_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_resource` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `media_id` varchar(100) NOT NULL,
  `trunk` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `dateline` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `acid` (`uniacid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_resource`
--

LOCK TABLES `ims_core_resource` WRITE;
/*!40000 ALTER TABLE `ims_core_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_sessions`
--

DROP TABLE IF EXISTS `ims_core_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_sessions` (
  `sid` char(32) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `data` varchar(5000) NOT NULL,
  `expiretime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_sessions`
--

LOCK TABLES `ims_core_sessions` WRITE;
/*!40000 ALTER TABLE `ims_core_sessions` DISABLE KEYS */;
INSERT INTO `ims_core_sessions` VALUES ('2e6bfa0910943bb226d85e86800763d6',2,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','',1423994163),('7sn5e23e3518nb520itecgubk7',2,'APP','',1423826058),('b06se1p3kp98msvknjh9oqt7v2',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";',1423826136),('b4c80mrjaruv0pdstpjslug6l7',2,'APP','ceba9a2048cbd440|s:4:\"aT0y\";',1423826154),('ls533up9c123bkj7q0rap2fct4',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423826259),('nmonnoo7foe0diaupeptqv3ho4',2,'APP','',1423826346),('eu8da70u731ki720p7d5qt6sm1',2,'APP','',1423826410),('ij16ubf2mc3na647e5n3cotaj6',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423829492),('e6gd677ql0tvdsvl3e3a1dv8r1',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423829707),('i7cglvardpgl5e8umq6qci1uv5',2,'APP','24ffeb1b5a7acc05|s:40:\"aT0yJmo9MiZjPWVudHJ5JmRvPWxpc3QmbT1zYWxl\";',1423829618),('gubdvqdbttdrsk6t2099ie0sf2',2,'APP','',1423829619),('lgah0cvt1t6eolv0af1sqgb0f2',2,'APP','c86962de48f62e2e|s:44:\"aT0yJmo9MiZjPWVudHJ5JmRvPW15b3JkZXImbT1zYWxl\";',1423829625),('siijkma1sho8uu50m2890trj23',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423829689),('dqtlt83d41mjf24sbrk9hp6sm6',2,'APP','43554f7b31e95fcf|s:80:\"aT0yJmo9MiZjPWVudHJ5JmRvPWZhbnNpbmRleCZtPXNhbGUmd3hyZWY9bXAud2VpeGluLnFxLmNvbQ==\";',1423829700),('56lcubdkr70bh4bha0sravvep5',2,'APP','',1423829999),('pn09fekpoleuulenpm2s8211u7',2,'APP','',1423830481),('u3g3lokjbfvpm59o9is2lujq54',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423840887),('o28i4i4bikoq5kh928i3158dq2',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423876327),('o1qtk2ueiku30k0ofk5rhh5st6',2,'APP','592e7d86229dad73|s:48:\"aT0yJmo9MiZjPWVudHJ5Jm1pZD0xJmRvPWxpc3QmbT1zYWxl\";',1423876327),('7gi38nc5lr4qo4l10gqpu3eja1',2,'APP','',1423876327),('skabk76dvd4o1ecifq6kmhlbr5',2,'APP','acid|s:1:\"2\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423877287),('amal2bvqvl70u5nsljj0cfgtq3',2,'APP','eac64b41a0db2fc3|s:72:\"aT0yJmo9MiZjPWVudHJ5JmRvPWxpc3QmbT1zYWxlJnd4cmVmPW1wLndlaXhpbi5xcS5jb20=\";',1423877157),('ldamqkh8aghnhig6bdq6fhlsd1',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423879244),('ntou6ak9g4m721loluga1phps3',2,'APP','',1423877848),('7drp3lu6g7efrtpne13tpsqme2',2,'APP','',1423877848),('qh8sok6et79li7bahfjv1jgi43',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";',1423877859),('qba61l30ak6ajahrc9p33cc9c5',2,'APP','',1423877865),('o48qmarsath3guri4007dru5b7',2,'APP','02a8eab3ae695a78|s:52:\"aT0yJmo9MiZjPWVudHJ5JmlkPTEmZG89ZGV0YWlsJm09c2FsZQ==\";',1423877998),('olkti6er7fog4b2lapl4jjdaj3',2,'APP','c9ccb538a500f53c|s:88:\"aT0yJmo9MiZjPWVudHJ5JmlkPTEmZG89Y29uZmlybSZtPXNhbGUmb3B0aW9uaWQ9dW5kZWZpbmVkJnRvdGFsPTE=\";',1423878002),('lag8h5ogibk9n98iagmbpooip6',2,'APP','c9ccb538a500f53c|s:88:\"aT0yJmo9MiZjPWVudHJ5JmlkPTEmZG89Y29uZmlybSZtPXNhbGUmb3B0aW9uaWQ9dW5kZWZpbmVkJnRvdGFsPTE=\";',1423878002),('0dhf6jqd9d7rokgecc617nool6',2,'APP','',1423878061),('u36t4fglft6ef5aog3spfb7b74',2,'APP','',1423878229),('163lan65ddal061953fl9s5gs3',2,'APP','',1423878254),('4is2jlpusgarksvp570v4n8ug5',2,'APP','',1423878272),('861cav3h5sbf1kole0utia2ci0',2,'APP','',1423878340),('2vr3odn29e5pf826pjqp40r2k6',2,'APP','',1423878496),('qum28t4f7l9edo9smqgo7k3g73',2,'APP','',1423878531),('c4vde45op59d4m1so54knbvk54',2,'APP','',1423878720),('uctmveud1opl337qdmcv2kvsr7',2,'APP','',1423878791),('b0d5b45019617f1455123b40b8d1954b',2,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','',1423878828),('vunit7lq14t0qt1vlptdm0p6k3',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot7Enl-kYHGMKdf3Nh5PEG9k\";uid|s:1:\"3\";',1423878824),('q8tg9uhil7h5fab2lcnmk7ail3',2,'APP','febc407fc4e3125e|s:68:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGUmd3hyZWY9bXAud2VpeGluLnFxLmNvbQ==\";',1423878811),('a5lcvra98k50fkmo1mp4vnm7u2',2,'APP','9d4edbd689c06da2|s:36:\"aT0yJmM9ZW50cnkmZG89YXdhcmQmbT1zYWxl\";',1423878819),('a547b721b71fc4d054a7d89a4595621b',2,'oO0vot03OPAAzjfySGT6jUMVkMhM','',1423879168),('rbklo0eul2emrut33rs6uot5h1',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";',1423879171),('n5g9h78l3j367klnc6j3495cs5',2,'APP','',1423879245),('2f9cbcee66153feca14303e4f2182239',2,'oO0votzG_RsUsDv52aEIhU4LLH6Y','',1423879291),('mhhru1l1animkri6dl07ejqib6',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0votzG_RsUsDv52aEIhU4LLH6Y\";uid|s:1:\"5\";',1423879334),('1lej1o1jrn2p178dn6a2qjkbg4',2,'APP','',1423879683),('1077lneem710bc3qujoefeson4',2,'APP','',1423879996),('28ccf4eec6fcc6f0fbcf20c294c411f6',2,'oO0vot7lM4ACySQFbZpDwfwWa5OU','',1423882615),('aer9hnabkcqgihu5nsq48g1bt2',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot7lM4ACySQFbZpDwfwWa5OU\";uid|s:1:\"6\";',1423882603),('b63b74f3dd45476cee62a5bacfc30650',2,'oO0votylO7fzB3vrn-eYEToxV80U','',1423882921),('jct9j9hhai6k3gk7o1hgsd77a7',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0votylO7fzB3vrn-eYEToxV80U\";uid|s:1:\"7\";',1423883320),('v8170u5tufujo1u78aviuhdar7',2,'APP','010734b7dcc1127a|s:52:\"aT0yJmo9MiZjPWVudHJ5JmlzbmV3PTEmZG89bGlzdDImbT1zYWxl\";',1423882955),('g27ds2me7f9that9saelnl8jd5',2,'APP','f533bd0023cf57b5|s:44:\"aT0yJmo9MiZjPWVudHJ5JmRvPW15Y2FydCZtPXNhbGU=\";',1423882974),('h90ocr56fnie719e2mvr57cc61',2,'APP','',1423883037),('tct06o6ui054j73melsb30im52',2,'APP','',1423883391),('d037rn9ojsbvkjlhbqsbtda4t6',2,'APP','',1423884142),('46vm7bcdja0e9ps4mfvectbri7',2,'APP','',1423884410),('21e0466d155ccb8e04f11fabf36f8f09',2,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','',1423968664),('s4eb30cfjceifu54reppfq6po5',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot6yLu-hYLNW1CxL5i_sB6bg\";uid|s:1:\"8\";',1423893421),('bgdgbqdi1s1oc4u8vh0268g5r4',2,'APP','c86962de48f62e2e|s:44:\"aT0yJmo9MiZjPWVudHJ5JmRvPW15b3JkZXImbT1zYWxl\";',1423892529),('7q1jgscbg28u02kj5va89dqpu2',2,'APP','24ffeb1b5a7acc05|s:40:\"aT0yJmo9MiZjPWVudHJ5JmRvPWxpc3QmbT1zYWxl\";',1423892541),('mftnsabqtsjsllkmt1o8tdq4v6',2,'APP','',1423892541),('0c998re7k03m9kaujpvgnsorh2',2,'APP','f533bd0023cf57b5|s:44:\"aT0yJmo9MiZjPWVudHJ5JmRvPW15Y2FydCZtPXNhbGU=\";',1423893420),('k32j92samt1gifgtcdftgnuno5',2,'APP','',1423893612),('lvg089bflrvs460ve95q0e2oh6',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot6yLu-hYLNW1CxL5i_sB6bg\";uid|s:1:\"8\";',1423893729),('465b4559200ce7f2514a66bcd94098c9',2,'oO0votxc_I61yuZUG5xiUoy-gn6U','',1423928289),('8jbqa8gc6k2lpomshbjlavhdl4',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0votxc_I61yuZUG5xiUoy-gn6U\";uid|s:1:\"9\";',1423927046),('61pv35h9kjc2l62n0qns544oo5',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0votxc_I61yuZUG5xiUoy-gn6U\";uid|s:1:\"9\";',1423927119),('cmvldb6poprf585ri90mrevt64',2,'APP','febc407fc4e3125e|s:68:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGUmd3hyZWY9bXAud2VpeGluLnFxLmNvbQ==\";',1423927111),('berigm0vjoko8v7oheadr1q1f3',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";',1423927116),('pa37e5l8sd8vc50l5glcom0qs4',2,'APP','78f81e5a87950ad9|s:44:\"aT0yJmM9ZW50cnkmZG89ZmFuc2luZGV4Jm09c2FsZQ==\";openid|s:28:\"oO0vot6yLu-hYLNW1CxL5i_sB6bg\";uid|s:1:\"8\";',1423968664),('25r3v339etlgbe6t089h9h2g67',2,'APP','',1423968585),('7iohokbm25vb3b8g6j9jlaamk7',2,'APP','',1423968689),('dl0e2e5dafa26taosj858o2gg6',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423988322),('3cs0h7v15f9cbrjb736f4crao5',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";',1423988056),('v0e4r4dgj18032hq35ac3ledu3',2,'APP','02a8eab3ae695a78|s:52:\"aT0yJmo9MiZjPWVudHJ5JmlkPTEmZG89ZGV0YWlsJm09c2FsZQ==\";',1423988063),('kn0u527qu4lmrmteh8cnec6ld2',2,'APP','be40f2c1b2e4fb28|s:64:\"aT0yJmo9MiZjPWVudHJ5JmdpZD0xJm1pZD0xJmRvPXR1aWd1YW5nJm09c2FsZQ==\";',1423988094),('o0sffuq1p0kh78pa3ghv2739m6',2,'APP','78f81e5a87950ad9|s:44:\"aT0yJmM9ZW50cnkmZG89ZmFuc2luZGV4Jm09c2FsZQ==\";',1423988291),('obov1q6hj1pnf2mj29356l6jj2',2,'APP','',1423988505),('9lsgof3ks1oetccfebg464qvr1',2,'APP','',1423988669),('ar6hk3m98n3eifk0b8kbrcipo0',2,'APP','14f711da57e10cd8|s:36:\"aT0yJmM9ZW50cnkmZG89bGlzdCZtPXNhbGU=\";openid|s:28:\"oO0vot0Fp7hy9Blpi4NQwMZn3AJU\";uid|s:1:\"1\";',1423994263),('27bboidn96t0km07hl1vreefv4',2,'APP','',1423994165);
/*!40000 ALTER TABLE `ims_core_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_settings`
--

DROP TABLE IF EXISTS `ims_core_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_settings` (
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_settings`
--

LOCK TABLES `ims_core_settings` WRITE;
/*!40000 ALTER TABLE `ims_core_settings` DISABLE KEYS */;
INSERT INTO `ims_core_settings` VALUES ('authmode','i:1;'),('close','a:2:{s:6:\"status\";s:1:\"0\";s:6:\"reason\";s:0:\"\";}'),('register','a:4:{s:4:\"open\";i:1;s:6:\"verify\";i:0;s:4:\"code\";i:1;s:7:\"groupid\";i:1;}');
/*!40000 ALTER TABLE `ims_core_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_wechats_attachment`
--

DROP TABLE IF EXISTS `ims_core_wechats_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_wechats_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `media_id` varchar(255) NOT NULL,
  `type` varchar(15) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_wechats_attachment`
--

LOCK TABLES `ims_core_wechats_attachment` WRITE;
/*!40000 ALTER TABLE `ims_core_wechats_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_wechats_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_cover_reply`
--

DROP TABLE IF EXISTS `ims_cover_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_cover_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `module` varchar(30) NOT NULL,
  `do` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_cover_reply`
--

LOCK TABLES `ims_cover_reply` WRITE;
/*!40000 ALTER TABLE `ims_cover_reply` DISABLE KEYS */;
INSERT INTO `ims_cover_reply` VALUES (4,2,0,10,'sale','fansindex','代理入口','','','./index.php?i=2&c=entry&do=fansindex&m=sale'),(3,2,0,9,'sale','list','购物入口设置','','','./index.php?i=2&c=entry&do=list&m=sale');
/*!40000 ALTER TABLE `ims_cover_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_custom_reply`
--

DROP TABLE IF EXISTS `ims_custom_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_custom_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `start1` int(10) NOT NULL,
  `end1` int(10) NOT NULL,
  `start2` int(10) NOT NULL,
  `end2` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_custom_reply`
--

LOCK TABLES `ims_custom_reply` WRITE;
/*!40000 ALTER TABLE `ims_custom_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_custom_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_images_reply`
--

DROP TABLE IF EXISTS `ims_images_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_images_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_images_reply`
--

LOCK TABLES `ims_images_reply` WRITE;
/*!40000 ALTER TABLE `ims_images_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_images_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_card`
--

DROP TABLE IF EXISTS `ims_mc_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `color` varchar(255) NOT NULL,
  `background` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `format` varchar(50) NOT NULL,
  `fields` varchar(1000) NOT NULL,
  `snpos` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `business` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_card`
--

LOCK TABLES `ims_mc_card` WRITE;
/*!40000 ALTER TABLE `ims_mc_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_card_members`
--

DROP TABLE IF EXISTS `ims_mc_card_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_card_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) DEFAULT NULL,
  `cid` int(10) NOT NULL,
  `cardsn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_card_members`
--

LOCK TABLES `ims_mc_card_members` WRITE;
/*!40000 ALTER TABLE `ims_mc_card_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_card_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_chats_record`
--

DROP TABLE IF EXISTS `ims_mc_chats_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_chats_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL,
  `openid` varchar(32) NOT NULL,
  `msgtype` varchar(15) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`acid`),
  KEY `openid` (`openid`),
  KEY `createtime` (`createtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_chats_record`
--

LOCK TABLES `ims_mc_chats_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_chats_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_chats_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_credits_recharge`
--

DROP TABLE IF EXISTS `ims_mc_credits_recharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_credits_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `tid` varchar(20) NOT NULL,
  `transid` varchar(30) NOT NULL,
  `fee` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_uniacid_uid` (`uniacid`,`uid`),
  KEY `idx_tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_credits_recharge`
--

LOCK TABLES `ims_mc_credits_recharge` WRITE;
/*!40000 ALTER TABLE `ims_mc_credits_recharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_credits_recharge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_credits_record`
--

DROP TABLE IF EXISTS `ims_mc_credits_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_credits_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `uniacid` int(11) NOT NULL,
  `credittype` varchar(10) NOT NULL,
  `num` int(10) NOT NULL,
  `operator` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_credits_record`
--

LOCK TABLES `ims_mc_credits_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_credits_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_credits_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_fans_groups`
--

DROP TABLE IF EXISTS `ims_mc_fans_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_fans_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groups` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_fans_groups`
--

LOCK TABLES `ims_mc_fans_groups` WRITE;
/*!40000 ALTER TABLE `ims_mc_fans_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_fans_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_groups`
--

DROP TABLE IF EXISTS `ims_mc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `orderlist` tinyint(4) unsigned NOT NULL,
  `isdefault` tinyint(4) NOT NULL,
  PRIMARY KEY (`groupid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_groups`
--

LOCK TABLES `ims_mc_groups` WRITE;
/*!40000 ALTER TABLE `ims_mc_groups` DISABLE KEYS */;
INSERT INTO `ims_mc_groups` VALUES (2,2,'默认会员组',0,1);
/*!40000 ALTER TABLE `ims_mc_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_handsel`
--

DROP TABLE IF EXISTS `ims_mc_handsel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_handsel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `touid` int(10) unsigned NOT NULL,
  `fromuid` varchar(32) NOT NULL,
  `module` varchar(30) NOT NULL,
  `sign` varchar(100) NOT NULL,
  `action` varchar(20) NOT NULL,
  `credit_value` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`touid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_handsel`
--

LOCK TABLES `ims_mc_handsel` WRITE;
/*!40000 ALTER TABLE `ims_mc_handsel` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_handsel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mapping_fans`
--

DROP TABLE IF EXISTS `ims_mc_mapping_fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mapping_fans` (
  `fanid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  `salt` char(8) NOT NULL,
  `follow` tinyint(1) unsigned NOT NULL,
  `followtime` int(10) unsigned NOT NULL,
  `unfollowtime` int(10) unsigned NOT NULL,
  `tag` varchar(1000) NOT NULL,
  `updatetime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`fanid`),
  KEY `acid` (`acid`),
  KEY `uniacid` (`uniacid`),
  KEY `openid` (`openid`),
  KEY `updatetime` (`updatetime`),
  KEY `nickname` (`nickname`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mapping_fans`
--

LOCK TABLES `ims_mc_mapping_fans` WRITE;
/*!40000 ALTER TABLE `ims_mc_mapping_fans` DISABLE KEYS */;
INSERT INTO `ims_mc_mapping_fans` VALUES (1,2,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','',0,'vIN93bxd',1,1423874243,0,'',NULL),(2,2,2,2,'oO0vot3U8NO6NgG6-sirMG4U1A74','',0,'HhsdFhzi',0,0,1423828860,'',NULL),(3,2,2,3,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','',0,'wQYqyOl7',1,1423875204,0,'',NULL),(4,2,2,4,'oO0vot03OPAAzjfySGT6jUMVkMhM','',0,'H822upne',1,1423875568,0,'',NULL),(5,2,2,5,'oO0votzG_RsUsDv52aEIhU4LLH6Y','',0,'cCuvX65x',1,1423875680,0,'',NULL),(6,2,2,6,'oO0vot7lM4ACySQFbZpDwfwWa5OU','',0,'hOJK1PQj',1,1423878994,0,'',NULL),(7,2,2,7,'oO0votylO7fzB3vrn-eYEToxV80U','',0,'A4dsRlm7',1,1423879260,0,'',NULL),(8,2,2,8,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','',0,'CRTQhqhT',1,1423888909,0,'',NULL),(9,2,2,9,'oO0votxc_I61yuZUG5xiUoy-gn6U','',0,'bC7EmP00',1,1423923442,0,'',NULL);
/*!40000 ALTER TABLE `ims_mc_mapping_fans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mapping_ucenter`
--

DROP TABLE IF EXISTS `ims_mc_mapping_ucenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mapping_ucenter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `centeruid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mapping_ucenter`
--

LOCK TABLES `ims_mc_mapping_ucenter` WRITE;
/*!40000 ALTER TABLE `ims_mc_mapping_ucenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_mapping_ucenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mass_record`
--

DROP TABLE IF EXISTS `ims_mc_mass_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mass_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groupname` varchar(50) NOT NULL,
  `fansnum` int(10) unsigned NOT NULL,
  `msgtype` varchar(10) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`acid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mass_record`
--

LOCK TABLES `ims_mc_mass_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_mass_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_mass_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_members`
--

DROP TABLE IF EXISTS `ims_mc_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_members` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(8) NOT NULL,
  `groupid` int(11) NOT NULL,
  `credit1` decimal(10,2) unsigned NOT NULL,
  `credit2` decimal(10,2) unsigned NOT NULL,
  `credit3` decimal(10,2) unsigned NOT NULL,
  `credit4` decimal(10,2) unsigned NOT NULL,
  `credit5` decimal(10,2) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `groupid` (`groupid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_members`
--

LOCK TABLES `ims_mc_members` WRITE;
/*!40000 ALTER TABLE `ims_mc_members` DISABLE KEYS */;
INSERT INTO `ims_mc_members` VALUES (1,2,'18621519910','36f33c90fb0c0e5f327748fab3e4b34f@we7.cc','a2b31d955f02df6ebad28c474f51144e','mkKvI4lv',2,0.00,30000.00,0.00,0.00,0.00,1423822395,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(2,2,'','6c3be2cab5f070a6788d20a4ab5fb56c@we7.cc','ce0ebb94ccb08999e2f912a58ebe9dd1','lko7IDHt',2,0.00,0.00,0.00,0.00,0.00,1423828860,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(3,2,'','775946cfc97191a95e50f558bc7c6bc5@we7.cc','8a4edb7f483a2d57bbe38ff81aca200b','hohV5mvG',2,0.00,0.00,0.00,0.00,0.00,1423875204,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(4,2,'','0707ce8af87dc7bb41fd8ed338499230@we7.cc','8387727f775058338963e643573c1007','eDnWllgi',2,0.00,0.00,0.00,0.00,0.00,1423875568,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(5,2,'','74abfde66576b91eb3fcd02d6f3a8ae5@we7.cc','c46eb491b0b98e6efa2723704365790d','uHltTigJ',2,0.00,0.00,0.00,0.00,0.00,1423875680,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(6,2,'','9c1371a9fe9069d9cfe690cd004da8a2@we7.cc','d6300591345259a739fc92033dcf2721','k7pF3wWb',2,0.00,0.00,0.00,0.00,0.00,1423878994,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(7,2,'','1d9decd43ad2152a2c7c6ca69c9a6862@we7.cc','8f1daa05314dc36d28d0b0c54bdab0a9','Dg9RkOrV',2,0.00,0.00,0.00,0.00,0.00,1423879261,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(8,2,'','0b2a444d8b6f717dde4c5ffd97327a2f@we7.cc','db736c6fcbceb5c70abb85d8dc9ac23f','Mqx4ZmCq',2,0.00,0.00,0.00,0.00,0.00,1423888909,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(9,2,'','3f793db0709d1b6edda69ce475ed9808@we7.cc','6a56b4f23da90f58a9fb080b9085721b','cYucsh2Q',2,0.00,0.00,0.00,0.00,0.00,1423923442,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `ims_mc_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_menu_event`
--

DROP TABLE IF EXISTS `ims_menu_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_menu_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `keyword` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `picmd5` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `picmd5` (`picmd5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_menu_event`
--

LOCK TABLES `ims_menu_event` WRITE;
/*!40000 ALTER TABLE `ims_menu_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_menu_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mobilenumber`
--

DROP TABLE IF EXISTS `ims_mobilenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mobilenumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(10) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `dateline` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mobilenumber`
--

LOCK TABLES `ims_mobilenumber` WRITE;
/*!40000 ALTER TABLE `ims_mobilenumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mobilenumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_modules`
--

DROP TABLE IF EXISTS `ims_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_modules` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `version` varchar(10) NOT NULL,
  `ability` varchar(500) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `settings` tinyint(1) NOT NULL,
  `subscribes` varchar(500) NOT NULL,
  `handles` varchar(500) NOT NULL,
  `isrulefields` tinyint(1) NOT NULL,
  `issystem` tinyint(1) unsigned NOT NULL,
  `issolution` tinyint(1) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `idx_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_modules`
--

LOCK TABLES `ims_modules` WRITE;
/*!40000 ALTER TABLE `ims_modules` DISABLE KEYS */;
INSERT INTO `ims_modules` VALUES (1,'basic','system','基本文字回复','1.0','和您进行简单对话','一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(2,'news','system','基本混合图文回复','1.0','为你提供生动的图文资讯','一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(3,'music','system','基本音乐回复','1.0','提供语音、音乐等音频类回复','在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(4,'userapi','system','自定义接口回复','1.1','更方便的第三方接口设置','自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(5,'recharge','system','会员中心充值模块','1.0','提供会员充值功能','','WeEngine Team','http://www.we7.cc/',0,'','',0,1,0,0),(6,'custom','system','多客服转接','1.0.0','用来接入腾讯的多客服系统','','WeEngine Team','http://bbs.we7.cc',0,'a:0:{}','a:6:{i:0;s:5:\"image\";i:1;s:5:\"voice\";i:2;s:5:\"video\";i:3;s:8:\"location\";i:4;s:4:\"link\";i:5;s:4:\"text\";}',1,1,0,0),(7,'images','system','基本图片回复','1.0','提供图片回复','在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(8,'video','system','基本视频回复','1.0','提供图片回复','在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(9,'voice','system','基本语音回复','1.0','提供语音回复','在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(10,'chats','system','发送客服消息','1.0','公众号可以在粉丝最后发送消息的48小时内无限制发送消息','','WeEngine Team','http://www.we7.cc/',0,'','',0,1,0,0),(11,'sale','business','分销系统','3.2','分销系统','分销系统','','',1,'a:0:{}','a:1:{i:0;s:4:\"text\";}',0,0,0,0);
/*!40000 ALTER TABLE `ims_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_modules_bindings`
--

DROP TABLE IF EXISTS `ims_modules_bindings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_modules_bindings` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(30) NOT NULL,
  `entry` varchar(10) NOT NULL,
  `call` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `do` varchar(30) NOT NULL,
  `state` varchar(200) NOT NULL,
  `direct` int(11) NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `idx_module` (`module`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_modules_bindings`
--

LOCK TABLES `ims_modules_bindings` WRITE;
/*!40000 ALTER TABLE `ims_modules_bindings` DISABLE KEYS */;
INSERT INTO `ims_modules_bindings` VALUES (1,'sale','cover','','购物入口设置','list','',0),(2,'sale','cover','','代理入口','fansindex','',0),(3,'sale','cover','','排行榜入口设置','phb','',0),(4,'sale','cover','','积分兑换入口设置','award','',0),(5,'sale','menu','','订单管理','order','',0),(6,'sale','menu','','CRM会员管理','charge','',0),(7,'sale','menu','','商品管理','goods','',0),(8,'sale','menu','','商品分类','category','',0),(9,'sale','menu','','积分商品设置','award','',0),(10,'sale','menu','','积分兑换管理','credit','',0),(11,'sale','menu','','代理管理','fansmanager','',0),(12,'sale','menu','','佣金审核','commission','',0),(13,'sale','menu','','物流配送设置','dispatch','',0),(14,'sale','menu','','基础设置','rules','',0),(15,'sale','menu','','首页广告设置','adv','',0);
/*!40000 ALTER TABLE `ims_modules_bindings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_music_reply`
--

DROP TABLE IF EXISTS `ims_music_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_music_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(300) NOT NULL,
  `hqurl` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_music_reply`
--

LOCK TABLES `ims_music_reply` WRITE;
/*!40000 ALTER TABLE `ims_music_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_music_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_news_reply`
--

DROP TABLE IF EXISTS `ims_news_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_news_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `displayorder` int(10) NOT NULL,
  `incontent` tinyint(1) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_news_reply`
--

LOCK TABLES `ims_news_reply` WRITE;
/*!40000 ALTER TABLE `ims_news_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_news_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_profile_fields`
--

DROP TABLE IF EXISTS `ims_profile_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_profile_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `displayorder` smallint(6) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `unchangeable` tinyint(1) NOT NULL,
  `showinregister` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_profile_fields`
--

LOCK TABLES `ims_profile_fields` WRITE;
/*!40000 ALTER TABLE `ims_profile_fields` DISABLE KEYS */;
INSERT INTO `ims_profile_fields` VALUES (1,'realname',1,'真实姓名','',0,1,1,1),(2,'nickname',1,'昵称','',1,1,0,1),(3,'avatar',1,'头像','',1,0,0,0),(4,'qq',1,'QQ号','',0,0,0,1),(5,'mobile',1,'手机号码','',0,0,0,0),(6,'vip',1,'VIP级别','',0,0,0,0),(7,'gender',1,'性别','',0,0,0,0),(8,'birthyear',1,'出生生日','',0,0,0,0),(9,'constellation',1,'星座','',0,0,0,0),(10,'zodiac',1,'生肖','',0,0,0,0),(11,'telephone',1,'固定电话','',0,0,0,0),(12,'idcard',1,'证件号码','',0,0,0,0),(13,'studentid',1,'学号','',0,0,0,0),(14,'grade',1,'班级','',0,0,0,0),(15,'address',1,'邮寄地址','',0,0,0,0),(16,'zipcode',1,'邮编','',0,0,0,0),(17,'nationality',1,'国籍','',0,0,0,0),(18,'resideprovince',1,'居住地址','',0,0,0,0),(19,'graduateschool',1,'毕业学校','',0,0,0,0),(20,'company',1,'公司','',0,0,0,0),(21,'education',1,'学历','',0,0,0,0),(22,'occupation',1,'职业','',0,0,0,0),(23,'position',1,'职位','',0,0,0,0),(24,'revenue',1,'年收入','',0,0,0,0),(25,'affectivestatus',1,'情感状态','',0,0,0,0),(26,'lookingfor',1,' 交友目的','',0,0,0,0),(27,'bloodtype',1,'血型','',0,0,0,0),(28,'height',1,'身高','',0,0,0,0),(29,'weight',1,'体重','',0,0,0,0),(30,'alipay',1,'支付宝帐号','',0,0,0,0),(31,'msn',1,'MSN','',0,0,0,0),(32,'email',1,'电子邮箱','',0,0,0,0),(33,'taobao',1,'阿里旺旺','',0,0,0,0),(34,'site',1,'主页','',0,0,0,0),(35,'bio',1,'自我介绍','',0,0,0,0),(36,'interest',1,'兴趣爱好','',0,0,0,0);
/*!40000 ALTER TABLE `ims_profile_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_qrcode`
--

DROP TABLE IF EXISTS `ims_qrcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_qrcode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `qrcid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `model` tinyint(1) unsigned NOT NULL,
  `ticket` varchar(250) NOT NULL,
  `expire` int(10) unsigned NOT NULL,
  `subnum` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_qrcid` (`qrcid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_qrcode`
--

LOCK TABLES `ims_qrcode` WRITE;
/*!40000 ALTER TABLE `ims_qrcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_qrcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_qrcode_stat`
--

DROP TABLE IF EXISTS `ims_qrcode_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_qrcode_stat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `qid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `qrcid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_qrcode_stat`
--

LOCK TABLES `ims_qrcode_stat` WRITE;
/*!40000 ALTER TABLE `ims_qrcode_stat` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_qrcode_stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_rule`
--

DROP TABLE IF EXISTS `ims_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_rule`
--

LOCK TABLES `ims_rule` WRITE;
/*!40000 ALTER TABLE `ims_rule` DISABLE KEYS */;
INSERT INTO `ims_rule` VALUES (1,0,'城市天气','userapi',255,1),(2,0,'百度百科','userapi',255,1),(3,0,'即时翻译','userapi',255,1),(4,0,'今日老黄历','userapi',255,1),(5,0,'看新闻','userapi',255,1),(6,0,'快递查询','userapi',255,1),(10,2,'代理入口','cover',0,1),(9,2,'购物入口设置','cover',0,1);
/*!40000 ALTER TABLE `ims_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_rule_keyword`
--

DROP TABLE IF EXISTS `ims_rule_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_rule_keyword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_content` (`content`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_rule_keyword`
--

LOCK TABLES `ims_rule_keyword` WRITE;
/*!40000 ALTER TABLE `ims_rule_keyword` DISABLE KEYS */;
INSERT INTO `ims_rule_keyword` VALUES (1,1,0,'userapi','^.+天气$',3,255,1),(2,2,0,'userapi','^百科.+$',3,255,1),(3,2,0,'userapi','^定义.+$',3,255,1),(4,3,0,'userapi','^@.+$',3,255,1),(5,4,0,'userapi','日历',1,255,1),(6,4,0,'userapi','万年历',1,255,1),(7,4,0,'userapi','黄历',1,255,1),(8,4,0,'userapi','几号',1,255,1),(9,5,0,'userapi','新闻',1,255,1),(10,6,0,'userapi','^(申通|圆通|中通|汇通|韵达|顺丰|EMS) *[a-z0-9]{1,}$',3,255,1),(14,10,2,'cover','daili',1,0,1),(13,9,2,'cover','购物',1,0,1);
/*!40000 ALTER TABLE `ims_rule_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_address`
--

DROP TABLE IF EXISTS `ims_sale_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `area` varchar(30) NOT NULL,
  `address` varchar(300) NOT NULL,
  `zipcode` varchar(6) NOT NULL default '100000',
  `isdefault` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_address`
--

LOCK TABLES `ims_sale_address` WRITE;
/*!40000 ALTER TABLE `ims_sale_address` DISABLE KEYS */;
INSERT INTO `ims_sale_address` VALUES (1,2,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','施懿民','18621519910','上海市','上海辖区','浦东新区','蓝村路60弄',1,0,'200000');
/*!40000 ALTER TABLE `ims_sale_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_adv`
--

DROP TABLE IF EXISTS `ims_sale_adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_enabled` (`enabled`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_adv`
--

LOCK TABLES `ims_sale_adv` WRITE;
/*!40000 ALTER TABLE `ims_sale_adv` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_adv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_cart`
--

DROP TABLE IF EXISTS `ims_sale_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `goodsid` int(11) NOT NULL,
  `goodstype` tinyint(1) NOT NULL DEFAULT '1',
  `from_user` varchar(50) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `optionid` int(10) DEFAULT '0',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `idx_openid` (`from_user`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_cart`
--

LOCK TABLES `ims_sale_cart` WRITE;
/*!40000 ALTER TABLE `ims_sale_cart` DISABLE KEYS */;
INSERT INTO `ims_sale_cart` VALUES (1,2,1,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',1,0,8888.00);
/*!40000 ALTER TABLE `ims_sale_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_category`
--

DROP TABLE IF EXISTS `ims_sale_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属帐号',
  `commission` int(10) unsigned DEFAULT '0' COMMENT '推荐该类商品所能获得的佣金',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `thumb` varchar(255) NOT NULL COMMENT '分类图片',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID,0为第一级',
  `isrecommand` int(10) DEFAULT '0',
  `description` varchar(500) NOT NULL COMMENT '分类介绍',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_category`
--

LOCK TABLES `ims_sale_category` WRITE;
/*!40000 ALTER TABLE `ims_sale_category` DISABLE KEYS */;
INSERT INTO `ims_sale_category` VALUES (1,2,0,'微销服务','',0,1,'微信营销相关的服务',0,1),(2,2,0,'茶叶','',0,1,'',1,1);
/*!40000 ALTER TABLE `ims_sale_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_commission`
--

DROP TABLE IF EXISTS `ims_sale_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_commission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL COMMENT '粉丝ID',
  `ogid` int(10) unsigned DEFAULT NULL COMMENT '订单商品ID',
  `commission` decimal(10,2) unsigned NOT NULL COMMENT '佣金',
  `content` text,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为账户充值记录，1为提现记录',
  `isout` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为未导出，1为已导出',
  `isshare` int(11) DEFAULT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_commission`
--

LOCK TABLES `ims_sale_commission` WRITE;
/*!40000 ALTER TABLE `ims_sale_commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_credit_award`
--

DROP TABLE IF EXISTS `ims_sale_credit_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_credit_award` (
  `award_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `deadline` datetime NOT NULL,
  `credit_cost` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '100',
  `content` text NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_credit_award`
--

LOCK TABLES `ims_sale_credit_award` WRITE;
/*!40000 ALTER TABLE `ims_sale_credit_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_credit_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_credit_request`
--

DROP TABLE IF EXISTS `ims_sale_credit_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_credit_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `award_id` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_credit_request`
--

LOCK TABLES `ims_sale_credit_request` WRITE;
/*!40000 ALTER TABLE `ims_sale_credit_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_credit_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_dispatch`
--

DROP TABLE IF EXISTS `ims_sale_dispatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_dispatch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `dispatchname` varchar(50) DEFAULT '',
  `dispatchtype` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `firstprice` decimal(10,2) DEFAULT '0.00',
  `secondprice` decimal(10,2) DEFAULT '0.00',
  `firstweight` int(11) DEFAULT '0',
  `secondweight` int(11) DEFAULT '0',
  `express` int(11) DEFAULT '0',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_dispatch`
--

LOCK TABLES `ims_sale_dispatch` WRITE;
/*!40000 ALTER TABLE `ims_sale_dispatch` DISABLE KEYS */;
INSERT INTO `ims_sale_dispatch` VALUES (1,2,'快递',0,0,10.00,5.00,1000,1000,1,''),(2,2,'送货上门',0,0,0.00,0.00,1000,1000,0,'');
/*!40000 ALTER TABLE `ims_sale_dispatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_express`
--

DROP TABLE IF EXISTS `ims_sale_express`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `express_name` varchar(50) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `express_price` varchar(10) DEFAULT '',
  `express_area` varchar(100) DEFAULT '',
  `express_url` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_express`
--

LOCK TABLES `ims_sale_express` WRITE;
/*!40000 ALTER TABLE `ims_sale_express` DISABLE KEYS */;
INSERT INTO `ims_sale_express` VALUES (1,2,'顺丰',0,'','','shunfeng'),(2,2,'申通',0,'','','shentong'),(3,2,'韵达快运',0,'','','yunda');
/*!40000 ALTER TABLE `ims_sale_express` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_feedback`
--

DROP TABLE IF EXISTS `ims_sale_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1为维权，2为告擎',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态0未解决，1用户同意，2用户拒绝',
  `feedbackid` varchar(30) NOT NULL COMMENT '投诉单号',
  `transid` varchar(30) NOT NULL COMMENT '订单号',
  `reason` varchar(1000) NOT NULL COMMENT '理由',
  `solution` varchar(1000) NOT NULL COMMENT '期待解决方案',
  `remark` varchar(1000) NOT NULL COMMENT '备注',
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_feedbackid` (`feedbackid`),
  KEY `idx_createtime` (`createtime`),
  KEY `idx_transid` (`transid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_feedback`
--

LOCK TABLES `ims_sale_feedback` WRITE;
/*!40000 ALTER TABLE `ims_sale_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_goods`
--

DROP TABLE IF EXISTS `ims_sale_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `pcate` int(10) unsigned NOT NULL DEFAULT '0',
  `ccate` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1为实体，2为虚拟',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `displayorder` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `xsthumb` varchar(255) DEFAULT '',
  `unit` varchar(5) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `goodssn` varchar(50) NOT NULL DEFAULT '',
  `productsn` varchar(50) NOT NULL DEFAULT '',
  `marketprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `productprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `costprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` int(10) NOT NULL DEFAULT '0',
  `totalcnf` int(11) DEFAULT '0' COMMENT '0 拍下减库存 1 付款减库存 2 永久不减',
  `sales` int(10) unsigned NOT NULL DEFAULT '0',
  `spec` varchar(5000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `credit` int(11) DEFAULT '0',
  `maxbuy` int(11) DEFAULT '0',
  `hasoption` int(11) DEFAULT '0',
  `dispatch` int(11) DEFAULT '0',
  `thumb_url` text,
  `isnew` int(11) DEFAULT '0',
  `ishot` int(11) DEFAULT '0',
  `isdiscount` int(11) DEFAULT '0',
  `isrecommand` int(11) DEFAULT '0',
  `istime` int(11) DEFAULT '0',
  `timestart` int(11) DEFAULT '0',
  `timeend` int(11) DEFAULT '0',
  `viewcount` int(11) DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `commission2` int(3) DEFAULT NULL,
  `commission3` int(3) DEFAULT NULL,
  `commission` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_goods`
--

LOCK TABLES `ims_sale_goods` WRITE;
/*!40000 ALTER TABLE `ims_sale_goods` DISABLE KEYS */;
INSERT INTO `ims_sale_goods` VALUES (1,2,1,0,0,1,0,'好货分销','images/2/2015/02/XcU88Cp8PufmAaSBmXsOoicasciDzZ.jpg','images/2/2015/02/HYf1jSE1Of6zEMMGMeE266pE9SEcO6.jpg','','','','WX0001','',1.00,1000.00,0.00,-1,2,1,'',1423873546,0.00,1000,0,0,0,'a:0:{}',1,1,0,1,0,0,0,12,0,15,10,20),(2,2,2,0,0,1,0,'祁门红茶','','','','','','','',0.01,0.01,0.00,5,0,0,'',1423990129,0.00,0,0,0,0,'a:0:{}',0,0,0,1,0,0,0,0,0,16,12,20);
/*!40000 ALTER TABLE `ims_sale_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_goods_option`
--

DROP TABLE IF EXISTS `ims_sale_goods_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_goods_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(10) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `thumb` varchar(60) DEFAULT '',
  `productprice` decimal(10,2) DEFAULT '0.00',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `costprice` decimal(10,2) DEFAULT '0.00',
  `stock` int(11) DEFAULT '0',
  `weight` decimal(10,2) DEFAULT '0.00',
  `displayorder` int(11) DEFAULT '0',
  `specs` text,
  PRIMARY KEY (`id`),
  KEY `indx_goodsid` (`goodsid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_goods_option`
--

LOCK TABLES `ims_sale_goods_option` WRITE;
/*!40000 ALTER TABLE `ims_sale_goods_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_goods_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_goods_param`
--

DROP TABLE IF EXISTS `ims_sale_goods_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_goods_param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(10) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `value` text,
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_goodsid` (`goodsid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_goods_param`
--

LOCK TABLES `ims_sale_goods_param` WRITE;
/*!40000 ALTER TABLE `ims_sale_goods_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_goods_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_member`
--

DROP TABLE IF EXISTS `ims_sale_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `shareid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `from_user` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `pwd` varchar(20) NOT NULL,
  `bankcard` varchar(20) DEFAULT NULL,
  `banktype` varchar(20) DEFAULT NULL,
  `alipay` varchar(100) DEFAULT NULL,
  `wxhao` varchar(100) DEFAULT NULL,
  `commission` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '已结佣佣金',
  `zhifu` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '已打款佣金',
  `content` text,
  `createtime` int(10) NOT NULL,
  `flagtime` int(10) DEFAULT NULL COMMENT '为成推广人的时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '0为禁用，1为可用',
  `flag` tinyint(1) DEFAULT '0' COMMENT '0为会推广人，1为推广人',
  `clickcount` int(11) NOT NULL DEFAULT '0' COMMENT '点击次数',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_member`
--

LOCK TABLES `ims_sale_member` WRITE;
/*!40000 ALTER TABLE `ims_sale_member` DISABLE KEYS */;
INSERT INTO `ims_sale_member` VALUES (1,2,0,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','施懿民','18621519910','',NULL,NULL,'','',0.00,0.00,NULL,1423837281,1423837281,1,1,1),(2,2,0,8,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','方奇斌','13801879187','','','','','',0.00,0.00,NULL,1423965020,1423965020,1,0,1);
/*!40000 ALTER TABLE `ims_sale_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_order`
--

DROP TABLE IF EXISTS `ims_sale_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `shareid` int(10) unsigned DEFAULT '0' COMMENT '推荐人ID',
  `ordersn` varchar(20) NOT NULL,
  `price` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-1取消状态，0普通状态，1为已付款，2为已发货，3为成功',
  `sendtype` tinyint(1) unsigned NOT NULL COMMENT '1为快递，2为自提',
  `paytype` tinyint(1) unsigned NOT NULL COMMENT '1为余额，2为在线，3为到付',
  `transid` varchar(30) NOT NULL DEFAULT '0' COMMENT '微信支付单号',
  `goodstype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `remark` varchar(1000) NOT NULL DEFAULT '',
  `addressid` int(10) unsigned NOT NULL,
  `expresscom` varchar(30) NOT NULL DEFAULT '',
  `expresssn` varchar(50) NOT NULL DEFAULT '',
  `express` varchar(200) NOT NULL DEFAULT '',
  `goodsprice` decimal(10,2) DEFAULT '0.00',
  `dispatchprice` decimal(10,2) DEFAULT '0.00',
  `dispatch` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_order`
--

LOCK TABLES `ims_sale_order` WRITE;
/*!40000 ALTER TABLE `ims_sale_order` DISABLE KEYS */;
INSERT INTO `ims_sale_order` VALUES (1,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',0,'02142146','8888',0,0,2,'0',0,'',1,'','','',8888.00,0.00,0,1423873625),(2,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',0,'02146651','8888',3,0,2,'0',0,'',1,'','','',8888.00,0.00,0,1423873671),(3,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',0,'02140352','8898',0,0,2,'0',0,'',1,'','','',8888.00,10.00,1,1423874406),(4,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',0,'02141262','8898',0,0,2,'0',0,'',1,'','','',8888.00,10.00,1,1423874628),(5,2,1,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',0,'02159892','1',0,0,2,'0',0,'',1,'','','',1.00,0.00,2,1423990654);
/*!40000 ALTER TABLE `ims_sale_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_order_goods`
--

DROP TABLE IF EXISTS `ims_sale_order_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_order_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `goodsid` int(10) unsigned NOT NULL,
  `commission` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '该订单的推荐佣金',
  `commission2` decimal(10,2) unsigned DEFAULT '0.00',
  `commission3` decimal(10,2) unsigned DEFAULT '0.00',
  `applytime` int(10) unsigned DEFAULT NULL COMMENT '申请时间',
  `checktime` int(10) unsigned DEFAULT NULL COMMENT '审核时间',
  `status` tinyint(3) DEFAULT '0' COMMENT '申请状态，-2为标志删除，-1为审核无效，0为未申请，1为正在申请，2为审核通过',
  `content` text,
  `price` decimal(10,2) DEFAULT '0.00',
  `total` int(10) unsigned NOT NULL DEFAULT '1',
  `optionid` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `optionname` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_order_goods`
--

LOCK TABLES `ims_sale_order_goods` WRITE;
/*!40000 ALTER TABLE `ims_sale_order_goods` DISABLE KEYS */;
INSERT INTO `ims_sale_order_goods` VALUES (1,2,1,1,1777.60,266.64,26.66,NULL,NULL,0,NULL,8888.00,1,0,1423873625,NULL),(2,2,2,1,1777.60,266.64,26.66,NULL,NULL,0,NULL,8888.00,1,0,1423873671,NULL),(3,2,3,1,1777.60,266.64,26.66,NULL,NULL,0,NULL,8888.00,1,0,1423874406,NULL),(4,2,4,1,1777.60,266.64,26.66,NULL,NULL,0,NULL,8888.00,1,0,1423874628,NULL),(5,2,5,1,0.20,0.03,0.00,NULL,NULL,0,NULL,1.00,1,0,1423990654,NULL);
/*!40000 ALTER TABLE `ims_sale_order_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_product`
--

DROP TABLE IF EXISTS `ims_sale_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `productsn` varchar(50) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `marketprice` decimal(10,0) unsigned NOT NULL,
  `productprice` decimal(10,0) unsigned NOT NULL,
  `total` int(11) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `spec` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_goodsid` (`goodsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_product`
--

LOCK TABLES `ims_sale_product` WRITE;
/*!40000 ALTER TABLE `ims_sale_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_rule`
--

DROP TABLE IF EXISTS `ims_sale_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  `rule` text,
  `terms` text,
  `createtime` int(10) NOT NULL,
  `gzurl` varchar(255) NOT NULL,
  `teamfy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_rule`
--

LOCK TABLES `ims_sale_rule` WRITE;
/*!40000 ALTER TABLE `ims_sale_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_rules`
--

DROP TABLE IF EXISTS `ims_sale_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_rules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `rule` text,
  `terms` text,
  `createtime` int(10) NOT NULL,
  `commtime` int(5) NOT NULL DEFAULT '15' COMMENT '默认15天',
  `promotertimes` int(10) NOT NULL DEFAULT '1' COMMENT '默认成交一次才能成为推广员',
  `ischeck` tinyint(1) DEFAULT '1' COMMENT '0为未审核，1为审核',
  `clickcredit` int(10) NOT NULL DEFAULT '0' COMMENT '点击获取积分',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_rules`
--

LOCK TABLES `ims_sale_rules` WRITE;
/*!40000 ALTER TABLE `ims_sale_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_share_history`
--

DROP TABLE IF EXISTS `ims_sale_share_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_share_history` (
  `sharemid` int(11) DEFAULT NULL,
  `uniacid` int(11) DEFAULT NULL,
  `from_user` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_share_history`
--

LOCK TABLES `ims_sale_share_history` WRITE;
/*!40000 ALTER TABLE `ims_sale_share_history` DISABLE KEYS */;
INSERT INTO `ims_sale_share_history` VALUES (1,2,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU',1),(2,2,'oO0vot6yLu-hYLNW1CxL5i_sB6bg',2);
/*!40000 ALTER TABLE `ims_sale_share_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_spec`
--

DROP TABLE IF EXISTS `ims_sale_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_spec` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `displaytype` tinyint(3) unsigned NOT NULL,
  `content` text NOT NULL,
  `goodsid` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_spec`
--

LOCK TABLES `ims_sale_spec` WRITE;
/*!40000 ALTER TABLE `ims_sale_spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_sale_spec_item`
--

DROP TABLE IF EXISTS `ims_sale_spec_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_sale_spec_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `specid` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `show` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_specid` (`specid`),
  KEY `indx_show` (`show`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_sale_spec_item`
--

LOCK TABLES `ims_sale_spec_item` WRITE;
/*!40000 ALTER TABLE `ims_sale_spec_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_sale_spec_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_article`
--

DROP TABLE IF EXISTS `ims_site_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `iscommend` tinyint(1) NOT NULL,
  `ishot` tinyint(1) unsigned NOT NULL,
  `pcate` int(10) unsigned NOT NULL,
  `ccate` int(10) unsigned NOT NULL,
  `template` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `credit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_iscommend` (`iscommend`),
  KEY `idx_ishot` (`ishot`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_article`
--

LOCK TABLES `ims_site_article` WRITE;
/*!40000 ALTER TABLE `ims_site_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_category`
--

DROP TABLE IF EXISTS `ims_site_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `nid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `icon` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `template` varchar(300) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `templatefile` varchar(100) NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `ishomepage` tinyint(1) NOT NULL,
  `icontype` tinyint(1) unsigned NOT NULL,
  `css` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_category`
--

LOCK TABLES `ims_site_category` WRITE;
/*!40000 ALTER TABLE `ims_site_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_multi`
--

DROP TABLE IF EXISTS `ims_site_multi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_multi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `site_info` text NOT NULL,
  `quickmenu` varchar(2000) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_multi`
--

LOCK TABLES `ims_site_multi` WRITE;
/*!40000 ALTER TABLE `ims_site_multi` DISABLE KEYS */;
INSERT INTO `ims_site_multi` VALUES (2,2,'维客旺CRM',2,'','a:2:{s:8:\"template\";s:7:\"default\";s:12:\"enablemodule\";a:0:{}}',0);
/*!40000 ALTER TABLE `ims_site_multi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_nav`
--

DROP TABLE IF EXISTS `ims_site_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_nav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `section` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` smallint(5) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(500) NOT NULL,
  `css` varchar(1000) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `multiid` (`multiid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_nav`
--

LOCK TABLES `ims_site_nav` WRITE;
/*!40000 ALTER TABLE `ims_site_nav` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_slide`
--

DROP TABLE IF EXISTS `ims_site_slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `displayorder` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_slide`
--

LOCK TABLES `ims_site_slide` WRITE;
/*!40000 ALTER TABLE `ims_site_slide` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_styles`
--

DROP TABLE IF EXISTS `ims_site_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_styles`
--

LOCK TABLES `ims_site_styles` WRITE;
/*!40000 ALTER TABLE `ims_site_styles` DISABLE KEYS */;
INSERT INTO `ims_site_styles` VALUES (2,2,1,'微站默认模板_S8iH');
/*!40000 ALTER TABLE `ims_site_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_styles_vars`
--

DROP TABLE IF EXISTS `ims_site_styles_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_styles_vars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `variable` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_styles_vars`
--

LOCK TABLES `ims_site_styles_vars` WRITE;
/*!40000 ALTER TABLE `ims_site_styles_vars` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_styles_vars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_templates`
--

DROP TABLE IF EXISTS `ims_site_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `version` varchar(64) NOT NULL,
  `description` varchar(500) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sections` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_templates`
--

LOCK TABLES `ims_site_templates` WRITE;
/*!40000 ALTER TABLE `ims_site_templates` DISABLE KEYS */;
INSERT INTO `ims_site_templates` VALUES (1,'default','微站默认模板','','由微擎提供默认微站模板套系','微擎团队','http://we7.cc','1',0);
/*!40000 ALTER TABLE `ims_site_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_solution_acl`
--

DROP TABLE IF EXISTS `ims_solution_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_solution_acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `module` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `eid` int(10) unsigned NOT NULL,
  `do` varchar(255) NOT NULL,
  `state` varchar(1000) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_module` (`module`),
  KEY `idx_eid` (`eid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_solution_acl`
--

LOCK TABLES `ims_solution_acl` WRITE;
/*!40000 ALTER TABLE `ims_solution_acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_solution_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_keyword`
--

DROP TABLE IF EXISTS `ims_stat_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_keyword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` varchar(10) NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_keyword`
--

LOCK TABLES `ims_stat_keyword` WRITE;
/*!40000 ALTER TABLE `ims_stat_keyword` DISABLE KEYS */;
INSERT INTO `ims_stat_keyword` VALUES (1,2,'9',13,1,1423822610,1423756800),(2,2,'10',14,1,1423822652,1423756800);
/*!40000 ALTER TABLE `ims_stat_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_msg_history`
--

DROP TABLE IF EXISTS `ims_stat_msg_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_msg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `type` varchar(10) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_msg_history`
--

LOCK TABLES `ims_stat_msg_history` WRITE;
/*!40000 ALTER TABLE `ims_stat_msg_history` DISABLE KEYS */;
INSERT INTO `ims_stat_msg_history` VALUES (1,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.957525\";s:10:\"location_y\";s:10:\"121.431335\";s:9:\"precision\";s:11:\"1740.000000\";}','trace',1423822394),(2,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','','event',1423822582),(3,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";N;s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:9:\"subscribe\";}','text',1423822592),(4,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.957525\";s:10:\"location_y\";s:10:\"121.431335\";s:9:\"precision\";s:11:\"1740.000000\";}','trace',1423822594),(5,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.957525\";s:10:\"location_y\";s:10:\"121.431335\";s:9:\"precision\";s:11:\"1740.000000\";}','trace',1423822603),(6,2,9,13,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','cover','a:4:{s:7:\"content\";s:6:\"购物\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423822610),(7,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.957525\";s:10:\"location_y\";s:10:\"121.431335\";s:9:\"precision\";s:11:\"1740.000000\";}','trace',1423822632),(8,2,10,14,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','cover','a:4:{s:7:\"content\";s:5:\"daili\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423822652),(9,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.957525\";s:10:\"location_y\";s:10:\"121.431335\";s:9:\"precision\";s:11:\"1740.000000\";}','trace',1423822663),(10,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"30.963388\";s:10:\"location_y\";s:10:\"121.428795\";s:9:\"precision\";s:10:\"900.000000\";}','trace',1423825889),(11,2,0,0,'oO0vot3U8NO6NgG6-sirMG4U1A74','','','event',1423828860),(12,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.086664\";s:10:\"location_y\";s:10:\"121.385490\";s:9:\"precision\";s:10:\"900.000000\";}','trace',1423837267),(13,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.086664\";s:10:\"location_y\";s:10:\"121.385490\";s:9:\"precision\";s:10:\"900.000000\";}','trace',1423837290),(14,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.131887\";s:10:\"location_y\";s:10:\"121.394608\";s:9:\"precision\";s:11:\"1660.000000\";}','trace',1423838150),(15,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216557\";s:10:\"location_y\";s:10:\"121.523621\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423872696),(16,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216557\";s:10:\"location_y\";s:10:\"121.523621\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423872718),(17,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216530\";s:10:\"location_y\";s:10:\"121.523651\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423872729),(18,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216543\";s:10:\"location_y\";s:10:\"121.523651\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423872793),(19,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216516\";s:10:\"location_y\";s:10:\"121.523621\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423873555),(20,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216717\";s:10:\"location_y\";s:10:\"121.523735\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423873752),(21,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216677\";s:10:\"location_y\";s:10:\"121.523560\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423873943),(22,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216677\";s:10:\"location_y\";s:10:\"121.523560\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423873943),(23,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216599\";s:10:\"location_y\";s:10:\"121.523712\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874227),(24,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','','event',1423874230),(25,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";N;s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:9:\"subscribe\";}','text',1423874243),(26,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216599\";s:10:\"location_y\";s:10:\"121.523712\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874247),(27,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423874252),(28,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216599\";s:10:\"location_y\";s:10:\"121.523712\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874255),(29,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423874259),(30,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216599\";s:10:\"location_y\";s:10:\"121.523712\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874280),(31,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216484\";s:10:\"location_y\";s:10:\"121.523712\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423874395),(32,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423874395),(33,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216703\";s:10:\"location_y\";s:10:\"121.523804\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874609),(34,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423874619),(35,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216492\";s:10:\"location_y\";s:10:\"121.523537\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423874921),(36,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423874929),(37,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216492\";s:10:\"location_y\";s:10:\"121.523537\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423874936),(38,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216591\";s:10:\"location_y\";s:10:\"121.523613\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874942),(39,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216501\";s:10:\"location_y\";s:10:\"121.523567\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423874965),(40,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216463\";s:10:\"location_y\";s:10:\"121.523560\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423875177),(41,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:12:\"敬请关注\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:5:\"click\";}','text',1423875178),(42,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:69:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=fansindex&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875183),(43,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','default','a:3:{s:10:\"location_x\";s:9:\"31.333279\";s:10:\"location_y\";s:10:\"121.428223\";s:9:\"precision\";s:9:\"90.000000\";}','trace',1423875204),(44,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875209),(45,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','default','a:3:{s:10:\"location_x\";s:9:\"31.333279\";s:10:\"location_y\";s:10:\"121.428223\";s:9:\"precision\";s:9:\"90.000000\";}','trace',1423875217),(46,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','','a:4:{s:7:\"content\";s:63:\"http://121.41.118.242/app/index.php?i=2&c=entry&do=award&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875218),(47,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','default','a:3:{s:10:\"location_x\";s:9:\"31.333279\";s:10:\"location_y\";s:10:\"121.428223\";s:9:\"precision\";s:9:\"90.000000\";}','trace',1423875223),(48,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','','a:4:{s:7:\"content\";s:63:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=phb&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875224),(49,2,0,0,'oO0vot7Enl-kYHGMKdf3Nh5PEG9k','default','a:3:{s:10:\"location_x\";s:9:\"31.333279\";s:10:\"location_y\";s:10:\"121.428223\";s:9:\"precision\";s:9:\"90.000000\";}','trace',1423875228),(50,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216501\";s:10:\"location_y\";s:10:\"121.523567\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423875284),(51,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:69:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=fansindex&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875285),(52,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216501\";s:10:\"location_y\";s:10:\"121.523567\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423875331),(53,2,0,0,'oO0vot03OPAAzjfySGT6jUMVkMhM','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875568),(54,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.216450\";s:10:\"location_y\";s:10:\"121.523544\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423875608),(55,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875608),(56,2,0,0,'oO0votzG_RsUsDv52aEIhU4LLH6Y','default','a:3:{s:10:\"location_x\";s:9:\"30.957348\";s:10:\"location_y\";s:10:\"121.457062\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423875680),(57,2,0,0,'oO0votzG_RsUsDv52aEIhU4LLH6Y','default','a:3:{s:10:\"location_x\";s:9:\"30.957371\";s:10:\"location_y\";s:10:\"121.457069\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423875690),(58,2,0,0,'oO0votzG_RsUsDv52aEIhU4LLH6Y','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423875691),(59,2,0,0,'oO0vot7lM4ACySQFbZpDwfwWa5OU','','a:4:{s:7:\"content\";N;s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:9:\"subscribe\";}','text',1423878994),(60,2,0,0,'oO0vot7lM4ACySQFbZpDwfwWa5OU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423879001),(61,2,0,0,'oO0vot7lM4ACySQFbZpDwfwWa5OU','','a:4:{s:7:\"content\";s:12:\"敬请关注\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:5:\"click\";}','text',1423879015),(62,2,0,0,'oO0votylO7fzB3vrn-eYEToxV80U','','a:4:{s:7:\"content\";N;s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:9:\"subscribe\";}','text',1423879260),(63,2,0,0,'oO0votylO7fzB3vrn-eYEToxV80U','default','a:3:{s:10:\"location_x\";s:9:\"31.194733\";s:10:\"location_y\";s:10:\"121.398926\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423879307),(64,2,0,0,'oO0votylO7fzB3vrn-eYEToxV80U','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423879317),(65,2,0,0,'oO0votylO7fzB3vrn-eYEToxV80U','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423879321),(66,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";N;s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:9:\"subscribe\";}','text',1423888909),(67,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423888915),(68,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','default','a:3:{s:10:\"location_x\";s:9:\"31.164240\";s:10:\"location_y\";s:10:\"121.431168\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423889818),(69,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423889819),(70,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','default','a:3:{s:10:\"location_x\";s:9:\"31.164200\";s:10:\"location_y\";s:10:\"121.431190\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423890108),(71,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423890117),(72,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423890122),(73,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:12:\"敬请关注\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:5:\"click\";}','text',1423890124),(74,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423890129),(75,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:12:\"敬请关注\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:5:\"click\";}','text',1423914855),(76,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194052\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"70.000000\";}','trace',1423923442),(77,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423923443),(78,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194004\";s:10:\"location_y\";s:10:\"121.508995\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423923515),(79,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423923516),(80,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194021\";s:10:\"location_y\";s:10:\"121.508987\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924041),(81,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:28:\"o3Q2dt5Dg3hEO0rs-sPrn3UweQzo\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423924045),(82,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194021\";s:10:\"location_y\";s:10:\"121.508987\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924074),(83,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194012\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924464),(84,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194012\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924470),(85,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.508949\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423924485),(86,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194016\";s:10:\"location_y\";s:10:\"121.509003\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924544),(87,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','','a:4:{s:7:\"content\";s:28:\"o3Q2dt_ovmD4vIm5C74rCarL-CLw\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423924549),(88,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.508995\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924607),(89,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194012\";s:10:\"location_y\";s:10:\"121.508987\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924614),(90,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194012\";s:10:\"location_y\";s:10:\"121.508987\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924620),(91,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.509003\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423924680),(92,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.193998\";s:10:\"location_y\";s:10:\"121.509018\";s:9:\"precision\";s:9:\"70.000000\";}','trace',1423924682),(93,2,0,0,'oO0votxc_I61yuZUG5xiUoy-gn6U','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.509003\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423924689),(94,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.193991\";s:10:\"location_y\";s:10:\"121.508965\";s:9:\"precision\";s:9:\"70.000000\";}','trace',1423924708),(95,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194012\";s:10:\"location_y\";s:10:\"121.509010\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423924887),(96,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194004\";s:10:\"location_y\";s:10:\"121.508995\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423925144),(97,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194004\";s:10:\"location_y\";s:10:\"121.508995\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423925167),(98,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194014\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423925172),(99,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194014\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423925184),(100,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194008\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423925777),(101,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194008\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423925794),(102,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:28:\"https://we360.cn.nuskin.com/\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423925806),(103,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194008\";s:10:\"location_y\";s:10:\"121.508995\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423925828),(104,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194002\";s:10:\"location_y\";s:10:\"121.509003\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423926083),(105,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194002\";s:10:\"location_y\";s:10:\"121.509003\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423926089),(106,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.508881\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423926095),(107,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.194000\";s:10:\"location_y\";s:10:\"121.508797\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423926727),(108,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.193989\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423926854),(109,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:272:\"https://open.weixin.qq.com/connect/oauth2/authorize?appid=wx12219e70931143c1&redirect_uri=https://we360test.cn.nuskin.com/adminPanel/wechat/auth.nuskin%3Furl%3Dhttps%3A%2F%2Fwe360stage.cn.nuskin.com%2Findex.html&response_type=code&scope=snsapi_base&state=1#wechat_redirect\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1423926859),(110,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.193989\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423926867),(111,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.193989\";s:10:\"location_y\";s:10:\"121.508980\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423926873),(112,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','default','a:3:{s:10:\"location_x\";s:9:\"31.137800\";s:10:\"location_y\";s:10:\"121.448326\";s:9:\"precision\";s:9:\"65.000000\";}','trace',1423964980),(113,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:69:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=fansindex&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423964984),(114,2,0,0,'oO0vot6yLu-hYLNW1CxL5i_sB6bg','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423965064),(115,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.211912\";s:10:\"location_y\";s:10:\"121.525581\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423976049),(116,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.211996\";s:10:\"location_y\";s:10:\"121.525574\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984347),(117,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423984351),(118,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212067\";s:10:\"location_y\";s:10:\"121.525635\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984428),(119,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423984456),(120,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212120\";s:10:\"location_y\";s:10:\"121.525581\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984513),(121,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212025\";s:10:\"location_y\";s:10:\"121.525574\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984520),(122,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212025\";s:10:\"location_y\";s:10:\"121.525574\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984562),(123,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423984570),(124,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212082\";s:10:\"location_y\";s:10:\"121.525620\";s:9:\"precision\";s:9:\"60.000000\";}','trace',1423984577),(125,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212231\";s:10:\"location_y\";s:10:\"121.525612\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984644),(126,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423984646),(127,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212231\";s:10:\"location_y\";s:10:\"121.525612\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984688),(128,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:69:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=fansindex&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423984691),(129,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212065\";s:10:\"location_y\";s:10:\"121.525612\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984742),(130,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212065\";s:10:\"location_y\";s:10:\"121.525612\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423984797),(131,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.211918\";s:10:\"location_y\";s:10:\"121.525597\";s:9:\"precision\";s:9:\"30.000000\";}','trace',1423985174),(132,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','default','a:3:{s:10:\"location_x\";s:9:\"31.212011\";s:10:\"location_y\";s:10:\"121.525566\";s:9:\"precision\";s:9:\"40.000000\";}','trace',1423990553),(133,2,0,0,'oO0vot0Fp7hy9Blpi4NQwMZn3AJU','','a:4:{s:7:\"content\";s:64:\"http://wxpay.vikvon.com/app/index.php?i=2&c=entry&do=list&m=sale\";s:8:\"original\";N;s:11:\"redirection\";b:1;s:6:\"source\";s:4:\"VIEW\";}','text',1423990563);
/*!40000 ALTER TABLE `ims_stat_msg_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_rule`
--

DROP TABLE IF EXISTS `ims_stat_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=134 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_rule`
--

LOCK TABLES `ims_stat_rule` WRITE;
/*!40000 ALTER TABLE `ims_stat_rule` DISABLE KEYS */;
INSERT INTO `ims_stat_rule` VALUES (1,2,0,1,1423822394,1423756800),(2,2,0,1,1423822582,1423756800),(3,2,0,1,1423822592,1423756800),(4,2,0,1,1423822594,1423756800),(5,2,0,1,1423822603,1423756800),(6,2,9,1,1423822610,1423756800),(7,2,0,1,1423822632,1423756800),(8,2,10,1,1423822652,1423756800),(9,2,0,1,1423822663,1423756800),(10,2,0,1,1423825889,1423756800),(11,2,0,1,1423828860,1423843200),(12,2,0,1,1423837267,1423843200),(13,2,0,1,1423837290,1423843200),(14,2,0,1,1423838150,1423843200),(15,2,0,1,1423872696,1423843200),(16,2,0,1,1423872718,1423843200),(17,2,0,1,1423872729,1423843200),(18,2,0,1,1423872793,1423843200),(19,2,0,1,1423873555,1423843200),(20,2,0,1,1423873752,1423843200),(21,2,0,1,1423873943,1423843200),(22,2,0,1,1423873943,1423843200),(23,2,0,1,1423874227,1423843200),(24,2,0,1,1423874230,1423843200),(25,2,0,1,1423874243,1423843200),(26,2,0,1,1423874247,1423843200),(27,2,0,1,1423874252,1423843200),(28,2,0,1,1423874255,1423843200),(29,2,0,1,1423874259,1423843200),(30,2,0,1,1423874280,1423843200),(31,2,0,1,1423874395,1423843200),(32,2,0,1,1423874395,1423843200),(33,2,0,1,1423874609,1423843200),(34,2,0,1,1423874619,1423843200),(35,2,0,1,1423874921,1423843200),(36,2,0,1,1423874929,1423843200),(37,2,0,1,1423874936,1423843200),(38,2,0,1,1423874942,1423843200),(39,2,0,1,1423874965,1423843200),(40,2,0,1,1423875177,1423843200),(41,2,0,1,1423875178,1423843200),(42,2,0,1,1423875183,1423843200),(43,2,0,1,1423875204,1423843200),(44,2,0,1,1423875209,1423843200),(45,2,0,1,1423875217,1423843200),(46,2,0,1,1423875218,1423843200),(47,2,0,1,1423875223,1423843200),(48,2,0,1,1423875224,1423843200),(49,2,0,1,1423875228,1423843200),(50,2,0,1,1423875284,1423843200),(51,2,0,1,1423875285,1423843200),(52,2,0,1,1423875331,1423843200),(53,2,0,1,1423875568,1423843200),(54,2,0,1,1423875608,1423843200),(55,2,0,1,1423875608,1423843200),(56,2,0,1,1423875680,1423843200),(57,2,0,1,1423875690,1423843200),(58,2,0,1,1423875691,1423843200),(59,2,0,1,1423878994,1423843200),(60,2,0,1,1423879001,1423843200),(61,2,0,1,1423879015,1423843200),(62,2,0,1,1423879260,1423843200),(63,2,0,1,1423879307,1423843200),(64,2,0,1,1423879317,1423843200),(65,2,0,1,1423879321,1423843200),(66,2,0,1,1423888909,1423843200),(67,2,0,1,1423888915,1423843200),(68,2,0,1,1423889818,1423843200),(69,2,0,1,1423889819,1423843200),(70,2,0,1,1423890108,1423843200),(71,2,0,1,1423890117,1423843200),(72,2,0,1,1423890122,1423843200),(73,2,0,1,1423890124,1423843200),(74,2,0,1,1423890129,1423843200),(75,2,0,1,1423914855,1423843200),(76,2,0,1,1423923442,1423843200),(77,2,0,1,1423923443,1423843200),(78,2,0,1,1423923515,1423843200),(79,2,0,1,1423923516,1423843200),(80,2,0,1,1423924041,1423843200),(81,2,0,1,1423924045,1423843200),(82,2,0,1,1423924074,1423843200),(83,2,0,1,1423924464,1423843200),(84,2,0,1,1423924470,1423843200),(85,2,0,1,1423924485,1423843200),(86,2,0,1,1423924544,1423843200),(87,2,0,1,1423924549,1423843200),(88,2,0,1,1423924607,1423843200),(89,2,0,1,1423924614,1423843200),(90,2,0,1,1423924620,1423843200),(91,2,0,1,1423924680,1423843200),(92,2,0,1,1423924682,1423843200),(93,2,0,1,1423924689,1423843200),(94,2,0,1,1423924708,1423843200),(95,2,0,1,1423924887,1423843200),(96,2,0,1,1423925144,1423843200),(97,2,0,1,1423925167,1423843200),(98,2,0,1,1423925172,1423843200),(99,2,0,1,1423925184,1423843200),(100,2,0,1,1423925777,1423843200),(101,2,0,1,1423925794,1423843200),(102,2,0,1,1423925806,1423843200),(103,2,0,1,1423925828,1423843200),(104,2,0,1,1423926083,1423843200),(105,2,0,1,1423926089,1423843200),(106,2,0,1,1423926095,1423843200),(107,2,0,1,1423926727,1423843200),(108,2,0,1,1423926854,1423843200),(109,2,0,1,1423926859,1423843200),(110,2,0,1,1423926867,1423843200),(111,2,0,1,1423926873,1423843200),(112,2,0,1,1423964980,1423929600),(113,2,0,1,1423964984,1423929600),(114,2,0,1,1423965064,1423929600),(115,2,0,1,1423976049,1423929600),(116,2,0,1,1423984347,1423929600),(117,2,0,1,1423984351,1423929600),(118,2,0,1,1423984428,1423929600),(119,2,0,1,1423984456,1423929600),(120,2,0,1,1423984513,1423929600),(121,2,0,1,1423984520,1423929600),(122,2,0,1,1423984562,1423929600),(123,2,0,1,1423984570,1423929600),(124,2,0,1,1423984577,1423929600),(125,2,0,1,1423984644,1423929600),(126,2,0,1,1423984646,1423929600),(127,2,0,1,1423984688,1423929600),(128,2,0,1,1423984691,1423929600),(129,2,0,1,1423984742,1423929600),(130,2,0,1,1423984797,1423929600),(131,2,0,1,1423985174,1423929600),(132,2,0,1,1423990553,1423929600),(133,2,0,1,1423990563,1423929600);
/*!40000 ALTER TABLE `ims_stat_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account`
--

DROP TABLE IF EXISTS `ims_uni_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account` (
  `uniacid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account`
--

LOCK TABLES `ims_uni_account` WRITE;
/*!40000 ALTER TABLE `ims_uni_account` DISABLE KEYS */;
INSERT INTO `ims_uni_account` VALUES (2,-1,'维客旺CRM','');
/*!40000 ALTER TABLE `ims_uni_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account_modules`
--

DROP TABLE IF EXISTS `ims_uni_account_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account_modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_module` (`module`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account_modules`
--

LOCK TABLES `ims_uni_account_modules` WRITE;
/*!40000 ALTER TABLE `ims_uni_account_modules` DISABLE KEYS */;
INSERT INTO `ims_uni_account_modules` VALUES (12,2,'basic',1,''),(13,2,'news',1,''),(14,2,'music',1,''),(15,2,'userapi',1,''),(16,2,'recharge',1,''),(17,2,'custom',1,''),(18,2,'images',1,''),(19,2,'video',1,''),(20,2,'voice',1,''),(21,2,'chats',1,''),(22,2,'sale',1,'a:15:{s:11:\"noticeemail\";N;s:8:\"shopname\";N;s:15:\"zhifuCommission\";s:0:\"\";s:16:\"globalCommission\";s:0:\"\";s:17:\"globalCommission2\";s:0:\"\";s:17:\"globalCommission3\";s:0:\"\";s:7:\"indexss\";i:0;s:4:\"ydyy\";s:0:\"\";s:16:\"paymsgTemplateid\";s:0:\"\";s:7:\"address\";N;s:5:\"phone\";N;s:5:\"appid\";s:18:\"wxff1e4034f5f9f549\";s:6:\"secret\";s:32:\"83276e5bcb1026951d6d5db655956b70\";s:11:\"officialweb\";N;s:11:\"description\";s:0:\"\";}');
/*!40000 ALTER TABLE `ims_uni_account_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account_users`
--

DROP TABLE IF EXISTS `ims_uni_account_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_memberid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account_users`
--

LOCK TABLES `ims_uni_account_users` WRITE;
/*!40000 ALTER TABLE `ims_uni_account_users` DISABLE KEYS */;
INSERT INTO `ims_uni_account_users` VALUES (2,2,1,'manager');
/*!40000 ALTER TABLE `ims_uni_account_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_group`
--

DROP TABLE IF EXISTS `ims_uni_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `modules` varchar(5000) NOT NULL,
  `templates` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_group`
--

LOCK TABLES `ims_uni_group` WRITE;
/*!40000 ALTER TABLE `ims_uni_group` DISABLE KEYS */;
INSERT INTO `ims_uni_group` VALUES (1,'体验套餐服务','N;','N;');
/*!40000 ALTER TABLE `ims_uni_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_settings`
--

DROP TABLE IF EXISTS `ims_uni_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_settings` (
  `uniacid` int(10) unsigned NOT NULL,
  `passport` varchar(200) NOT NULL,
  `oauth` varchar(100) NOT NULL,
  `jsauth_acid` int(10) unsigned NOT NULL,
  `uc` varchar(500) NOT NULL,
  `notify` varchar(2000) NOT NULL,
  `creditnames` varchar(500) NOT NULL,
  `creditbehaviors` varchar(500) NOT NULL,
  `welcome` varchar(60) NOT NULL,
  `default` varchar(60) NOT NULL,
  `default_message` varchar(1000) NOT NULL,
  `shortcuts` varchar(5000) NOT NULL,
  `payment` varchar(2000) NOT NULL,
  `groupdata` varchar(100) NOT NULL,
  `stat` varchar(300) NOT NULL,
  `menuset` text NOT NULL,
  `default_site` int(10) unsigned DEFAULT NULL,
  `sync` varchar(100) NOT NULL,
  PRIMARY KEY (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_settings`
--

LOCK TABLES `ims_uni_settings` WRITE;
/*!40000 ALTER TABLE `ims_uni_settings` DISABLE KEYS */;
INSERT INTO `ims_uni_settings` VALUES (2,'','a:2:{s:6:\"status\";i:1;s:7:\"account\";i:2;}',0,'','a:1:{s:3:\"sms\";a:2:{s:7:\"balance\";i:0;s:9:\"signature\";s:0:\"\";}}','a:2:{s:7:\"credit1\";a:2:{s:5:\"title\";s:6:\"积分\";s:7:\"enabled\";i:1;}s:7:\"credit2\";a:2:{s:5:\"title\";s:6:\"余额\";s:7:\"enabled\";i:1;}}','a:2:{s:8:\"activity\";s:7:\"credit1\";s:8:\"currency\";s:7:\"credit2\";}','','','','','a:4:{s:6:\"credit\";a:1:{s:6:\"switch\";b:1;}s:6:\"alipay\";a:4:{s:6:\"switch\";b:1;s:7:\"account\";s:18:\"vikvon@outlook.com\";s:7:\"partner\";s:16:\"2088011385574785\";s:6:\"secret\";s:32:\"7e34o044ardsbxh7yfzt0zonnzy3h2x6\";}s:6:\"wechat\";a:8:{s:6:\"switch\";b:1;s:7:\"account\";s:1:\"2\";s:7:\"signkey\";s:32:\"91abffb439e10989483d61344338d264\";s:7:\"partner\";s:0:\"\";s:3:\"key\";s:0:\"\";s:7:\"version\";s:1:\"2\";s:5:\"mchid\";s:8:\"10021946\";s:6:\"apikey\";s:32:\"91abffb439e10989483d61344338d264\";}s:8:\"delivery\";a:1:{s:6:\"switch\";b:1;}}','a:3:{s:8:\"isexpire\";i:0;s:10:\"oldgroupid\";i:0;s:7:\"endtime\";i:1423821045;}','','YTo0OntpOjA7YTo1OntzOjU6InRpdGxlIjtzOjEyOiLotK3niankuK3lv4MiO3M6NDoidHlwZSI7czozOiJ1cmwiO3M6MzoidXJsIjtzOjY0OiJodHRwOi8vd3hwYXkudmlrdm9uLmNvbS9hcHAvaW5kZXgucGhwP2k9MiZjPWVudHJ5JmRvPWxpc3QmbT1zYWxlIjtzOjc6ImZvcndhcmQiO3M6MDoiIjtzOjg6InN1Yk1lbnVzIjthOjA6e319aToxO2E6NTp7czo1OiJ0aXRsZSI7czoxMjoi5oiR6KaB5YiG6ZSAIjtzOjQ6InR5cGUiO3M6MzoidXJsIjtzOjM6InVybCI7czowOiIiO3M6NzoiZm9yd2FyZCI7czowOiIiO3M6ODoic3ViTWVudXMiO2E6Mzp7aTowO2E6NDp7czo1OiJ0aXRsZSI7czoxNToi5Y+Y6Lqr57uP6ZSA5ZWGIjtzOjQ6InR5cGUiO3M6MzoidXJsIjtzOjM6InVybCI7czo2OToiaHR0cDovL3d4cGF5LnZpa3Zvbi5jb20vYXBwL2luZGV4LnBocD9pPTImYz1lbnRyeSZkbz1mYW5zaW5kZXgmbT1zYWxlIjtzOjc6ImZvcndhcmQiO3M6MDoiIjt9aToxO2E6NDp7czo1OiJ0aXRsZSI7czoxNToi5pS25YWl5o6S6KGM5qacIjtzOjQ6InR5cGUiO3M6MzoidXJsIjtzOjM6InVybCI7czo2MzoiaHR0cDovL3d4cGF5LnZpa3Zvbi5jb20vYXBwL2luZGV4LnBocD9pPTImYz1lbnRyeSZkbz1waGImbT1zYWxlIjtzOjc6ImZvcndhcmQiO3M6MDoiIjt9aToyO2E6NDp7czo1OiJ0aXRsZSI7czoxMjoi5Liq5Lq65Lit5b+DIjtzOjQ6InR5cGUiO3M6MzoidXJsIjtzOjM6InVybCI7czo2MzoiaHR0cDovLzEyMS40MS4xMTguMjQyL2FwcC9pbmRleC5waHA/aT0yJmM9ZW50cnkmZG89YXdhcmQmbT1zYWxlIjtzOjc6ImZvcndhcmQiO3M6MDoiIjt9fX1pOjI7YTo1OntzOjU6InRpdGxlIjtzOjEyOiLllK7lkI7mnI3liqEiO3M6NDoidHlwZSI7czo1OiJjbGljayI7czozOiJ1cmwiO3M6NzoiaHR0cDovLyI7czo3OiJmb3J3YXJkIjtzOjEyOiLmlazor7flhbPms6giO3M6ODoic3ViTWVudXMiO2E6MDp7fX1pOjM7YToxOntzOjEwOiJjcmVhdGV0aW1lIjtpOjE0MjM4NzQyMTE7fX0=',2,'a:2:{s:6:\"switch\";i:0;s:4:\"acid\";s:0:\"\";}');
/*!40000 ALTER TABLE `ims_uni_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_verifycode`
--

DROP TABLE IF EXISTS `ims_uni_verifycode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_verifycode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `verifycode` varchar(6) NOT NULL,
  `total` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_verifycode`
--

LOCK TABLES `ims_uni_verifycode` WRITE;
/*!40000 ALTER TABLE `ims_uni_verifycode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_uni_verifycode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_userapi_cache`
--

DROP TABLE IF EXISTS `ims_userapi_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_userapi_cache` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `content` text NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_userapi_cache`
--

LOCK TABLES `ims_userapi_cache` WRITE;
/*!40000 ALTER TABLE `ims_userapi_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_userapi_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_userapi_reply`
--

DROP TABLE IF EXISTS `ims_userapi_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_userapi_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `description` varchar(300) NOT NULL,
  `apiurl` varchar(300) NOT NULL,
  `token` varchar(32) NOT NULL,
  `default_text` varchar(100) NOT NULL,
  `cachetime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_userapi_reply`
--

LOCK TABLES `ims_userapi_reply` WRITE;
/*!40000 ALTER TABLE `ims_userapi_reply` DISABLE KEYS */;
INSERT INTO `ims_userapi_reply` VALUES (1,1,'\"城市名+天气\", 如: \"北京天气\"','weather.php','','',0),(2,2,'\"百科+查询内容\" 或 \"定义+查询内容\", 如: \"百科姚明\", \"定义自行车\"','baike.php','','',0),(3,3,'\"@查询内容(中文或英文)\"','translate.php','','',0),(4,4,'\"日历\", \"万年历\", \"黄历\"或\"几号\"','calendar.php','','',0),(5,5,'\"新闻\"','news.php','','',0),(6,6,'\"快递+单号\", 如: \"申通1200041125\"','express.php','','',0);
/*!40000 ALTER TABLE `ims_userapi_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users`
--

DROP TABLE IF EXISTS `ims_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` int(10) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `joindate` int(10) unsigned NOT NULL,
  `joinip` varchar(15) NOT NULL,
  `lastvisit` int(10) unsigned NOT NULL,
  `lastip` varchar(15) NOT NULL,
  `remark` varchar(500) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users`
--

LOCK TABLES `ims_users` WRITE;
/*!40000 ALTER TABLE `ims_users` DISABLE KEYS */;
INSERT INTO `ims_users` VALUES (1,0,'admin','c9206c0277cc23ba7f6a9fd63130f26c1654c12b','3291ba89',0,1423820438,'',1423990198,'211.161.247.38','');
/*!40000 ALTER TABLE `ims_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_group`
--

DROP TABLE IF EXISTS `ims_users_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `package` varchar(5000) NOT NULL,
  `maxaccount` int(10) unsigned NOT NULL,
  `maxsubaccount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_group`
--

LOCK TABLES `ims_users_group` WRITE;
/*!40000 ALTER TABLE `ims_users_group` DISABLE KEYS */;
INSERT INTO `ims_users_group` VALUES (1,'体验用户组','a:1:{i:0;i:1;}',1,1),(2,'白金用户组','a:1:{i:0;i:1;}',2,2),(3,'黄金用户组','a:1:{i:0;i:1;}',3,3);
/*!40000 ALTER TABLE `ims_users_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_invitation`
--

DROP TABLE IF EXISTS `ims_users_invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_invitation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `fromuid` int(10) unsigned NOT NULL,
  `inviteuid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_invitation`
--

LOCK TABLES `ims_users_invitation` WRITE;
/*!40000 ALTER TABLE `ims_users_invitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_users_invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_permission`
--

DROP TABLE IF EXISTS `ims_users_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_permission`
--

LOCK TABLES `ims_users_permission` WRITE;
/*!40000 ALTER TABLE `ims_users_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_users_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_profile`
--

DROP TABLE IF EXISTS `ims_users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `fakeid` varchar(30) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_profile`
--

LOCK TABLES `ims_users_profile` WRITE;
/*!40000 ALTER TABLE `ims_users_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_video_reply`
--

DROP TABLE IF EXISTS `ims_video_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_video_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_video_reply`
--

LOCK TABLES `ims_video_reply` WRITE;
/*!40000 ALTER TABLE `ims_video_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_video_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_voice_reply`
--

DROP TABLE IF EXISTS `ims_voice_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_voice_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_voice_reply`
--

LOCK TABLES `ims_voice_reply` WRITE;
/*!40000 ALTER TABLE `ims_voice_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_voice_reply` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-15 17:09:30
