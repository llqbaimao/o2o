<?php

$url = "https://api.mch.weixin.qq.com/mmpaymkttransfers/sendredpack";
$mch_id = '10021946';
$appid = 'wxff1e4034f5f9f549';
$fans_open_id = 'o3Q2dt5Dg3hEO0rs-sPrn3UweQzo';
$mch_billno = $mch_id . '201502160000000003';
$nick_name = '维客旺CRM';
$send_name = '知平软件';
$total_amount = 100;
$min_value = 100;
$max_value = 100;
$total_num = 1;
$wishing = '恭喜发财';
$client_ip = '121.41.118.242';
$act_name = '红包测试';
$remark = '猜越多得越多，快来抢！';
$logo_imgurl = 'http://121.41.118.242:81/0723logo-you-weibo.jpg';
$share_content = '快来参加猜灯谜活动';
$share_url = 'http://121.41.118.242:81/';
$share_imgurl = 'http://121.41.118.242:81/0723logo-you-weibo.jpg';
$nonce_str = '50780e0cca98c8c8e814883e5caa672e';
$search_str = 'act_name=' . $act_name .
              '&client_ip=' . $client_ip . 
              '&logo_imgurl=' . $logo_imgurl .
              '&max_value=' . $max_value .
              '&mch_billno=' . $mch_billno .
              '&mch_id='. $mch_id .
              '&min_value='. $min_value .
              '&nick_name=' . $nick_name .
              '&nonce_str=' . $nonce_str .
              '&re_openid=' . $fans_open_id .  
              '&remark='. $remark . 
              '&send_name=' . $send_name .
              '&share_content=' . $share_content .
              '&share_imgurl=' . $share_imgurl .
              '&share_url=' . $share_url .
              '&total_amount=' . $total_amount .
              '&total_num=' . $total_num .
              '&wishing=' . $wishing .
              '&wxappid=' . $appid;

$sign_str = $search_str . '&key=wDImIzMekjhdAfO3HJixsrNl3kawT1H7';
echo $sign_str . '\n';
$sign = strtoupper(MD5($sign_str));
$params = '<xml> '.
 '<sign>'. $sign .'</sign> ' .
 '<mch_billno>' . $mch_billno . '</mch_billno> ' .
 '<mch_id>' . $mch_id . '</mch_id> '.
 '<wxappid>' . $appid . '</wxappid> '.
 '<nick_name>'. $nick_name . '</nick_name> '.
 '<send_name>'. $send_name . '</send_name> '.
 '<re_openid>'. $fans_open_id . '</re_openid> '.
 '<total_amount>'. $total_amount . '</total_amount> '.
 '<min_value>'. $min_value . '</min_value> '.
 '<max_value>'. $max_value . '</max_value> '.
 '<total_num>'. $total_num . '</total_num> '.
 '<wishing>'. $wishing .'</wishing> '.
 '<client_ip>'. $client_ip .'</client_ip> '.
 '<act_name>'. $act_name . '</act_name> '.
 '<remark>'. $remark . '</remark> '.
 '<logo_imgurl>'. $logo_imgurl .'</logo_imgurl> '.
 '<share_content>'. $share_content .'</share_content> '.
 '<share_url>'. $share_url .'</share_url> '.
 '<share_imgurl>'. $share_imgurl .'</share_imgurl> '. 
 '<nonce_str>'. $nonce_str .'</nonce_str> '.
 '</xml>';

echo '\r\n';
echo $params;
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
curl_setopt($ch,CURLOPT_SSLCERTTYPE,'PEM');
curl_setopt($ch,CURLOPT_SSLCERT,getcwd().'/apiclient_cert.pem');
curl_setopt($ch,CURLOPT_SSLKEYTYPE,'PEM');
curl_setopt($ch,CURLOPT_SSLKEY,getcwd().'/apiclient_key.pem');
$output = curl_exec($ch);
curl_close($ch);
print_r($output);

?>

