<?php
/**
 * Created by PhpStorm.
 * User: Shedom
 * Date: 15-3-24
 * Time: 下午3:55
 */

class Order {
    public $id = '0';
    public $from_user;
    public $ordersn;
    public $price;
    public $status;
    public $addreid;
    public $createtime;
    public $goods;
    public $waybills = "";
    public $logisfee = "0";
    public $tax;
    public $goodscount;
    public $goodsweight;

    public $paymentsn;
}

class FromUser {
    public $id;
    public $fromOpenid;
    public $fromEmail;
    public $mobile;
    public $papertype = "";
    public $papernumber = "";
}

class Address {
    public $id;
    public $openid;
    public $username;
    public $mobileno;
    public $province;
    public $city;
    public $area;
    public $address;
    public $zipcode;
}

function status($type){
    if($type == 0){
        return "未支付";
    }elseif($type == 1){
        return "已支付";
    }elseif($type == 2){
        return "已发货";
    }elseif($type == 3){
        return "已收货";
    }
    return $type;
}