﻿<?php
/**
 * Created by PhpStorm.
 * User: Shedom
 * Date: 15-3-24
 * Time: 下午12:21
 */
error_reporting(E_ALL);
ini_set('display_errors', true);
require_once "Order.php";
require_once "Goods.php";
require_once "db.base.php";

$type = $_GET["type"];
$array = array();
$orders = $pdo->query("select * from ims_sale_order where status = '" . $type . "'");
while ($row = $orders->fetch()) {
    $o = new Order();
    $o->id = $row["id"];
    $o->logisfee = $row['dispatchprice'];
    $add = $pdo->query("select * from ims_sale_address where id=".$row["addressid"]);
    $add = $add->fetch();

    $address = new Address();
    $address->id = $add["id"];
    $address->openid=$add["openid"];
    $address->mobileno=$add["mobile"];
    $address->username=$add["realname"];
    $address->province = $add["province"];
    $address->city = $add["city"];
    $address->area = $add["area"];
    $address->address=$add["address"];
    $address->zipcode=$add['zipcode'];


    $o->addreid = $address;
    $usr = $pdo->query("select * from ims_mc_members where uid = ".$row['uid'])->fetch();
    $u = new FromUser();
    $u->id=$row['uid'];
    $u->fromOpenid=$row['from_user'];
    $u->fromEmail=$usr['email'];
    $u->mobile=$usr['mobile'];
    $o->from_user = $u;

    $o->createtime = $row["createtime"];
    $o->ordersn = $row["ordersn"];
    $o->price = $row["price"];
    $o->status = status($type);

    $goods = $pdo->query("select * from ims_sale_order_goods where orderid = ".$row['id']);
    $modils = array();
    $o->goodscount = 0;
    $o->goodsweight = 0;
    while($r = $goods->fetch()){
        $g = new Good();
        $g->quantity=$r['total'];
        $o->goodscount =+ intval($g->quantity);

        $r2 = $pdo->query("select * from ims_sale_goods where id = ".$r['goodsid'])->fetch();
        $g->id = $r2["id"];
        $g->goodSN = $r2["goodssn"];
        $g->productSN = $r2["productsn"];
        $g->name=$r2['title'];
        $g->marketPrice=$r2['marketprice'];
        $g->weight=$r2['weight'];
        $o->goodsweight += intval($r['total'])*floatval($r2['weight']);

        $g->country="304";
        if(strpos($g->name,"奥地利") === 0){
            $g->country = "315";
        }
        $modils[] = $g;
    }
    $o->goods = $modils;
    $o->tax = floatval($o->price)/10;
    $paylog = $pdo->query("select * from ims_core_paylog where tid = ".$row['id'])->fetch();
    $o->paymentsn = $paylog["paymentsn"];
    $array[] = $o;
}

$json = json_encode($array);
echo $json;
