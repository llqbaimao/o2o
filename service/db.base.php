<?php
/**
 * Created by PhpStorm.
 * User: Shedom
 * Date: 15-3-27
 * Time: 下午1:32
 */
$config = array();
$config['db']['host'] = 'localhost';
$config['db']['username'] = 'root';
$config['db']['password'] = '123456';
//$config['db']['port'] = '3306';
$config['db']['database'] = 'we7';
$config['db']['charset'] = "SET NAMES 'UTF8';";
//$config['db']['pconnect'] = 0;
//$config['db']['tablepre'] = 'ims_';

$dns = "mysql:dbname=" . $config['db']['database'] . ";host=" . $config['db']['host'];
$opt = array(PDO::ATTR_PERSISTENT => true,PDO::MYSQL_ATTR_INIT_COMMAND => $config['db']['charset']);
$pdo = new PDO($dns, $config['db']['username'], $config['db']['password'], $opt);
