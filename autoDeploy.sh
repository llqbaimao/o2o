#!/usr/bin/expect
set timeout 30000
spawn ssh root@121.40.113.130
expect {
        "passphrase"
        {
            send "Llqwql41430\n";
        }
        "password"
        {
              send "Llqwql41430\n";
        }
        "yes/no"
        {
              send "yes\n";
              exp_continue;
        }
}
expect "#"
send "cd ~/repository/o2o"
expect "#"
send "git pull";
expect "#";
send "git checkout `git tag | head -n 1`"
expect "#"
send "cp -rf * /var/www/html/wq/"
expect "#"
send "tar -cvf wq_`date "+%Y%m%d%H%M"`.tar *"
expect "#"
send "mv wq_`date "+%Y%m%d%H%M"`.tar ~/repository/code_backup/"
expect "#"
send "rm -rf /var/www/html/wq/data/tpl/app/default/eso_sale/"
expect eof

