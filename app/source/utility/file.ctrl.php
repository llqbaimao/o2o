<?php
/**
 * [WeEngine System] Copyright (c) 2014 WE7.CC
 * WeEngine is NOT a free software, it under the license terms, visited http://www.we7.cc/ for more details.
 */
defined('IN_IA') or exit('Access Denied');

$do   = in_array($_GPC['do'], array('upload')) ? $_GPC['do'] : 'upload';
$type = in_array($_GPC['type'], array('image','audio')) ? $_GPC['type'] : 'image';

$result = array('error' => 1, 'message' => '');

$option = array();
if(isset($_GPC['options'])){
	$option = @base64_decode($_GPC['options']);
	$option = @iunserializer($option);
}

$dest_dir = trim($_GPC['dest_dir']);
if(!empty($dest_dir)){
	$dest_dir = trim($dest_dir, '/');
}

if ($do == 'upload') {
	if($type == 'image'){
		$result = array(
			'jsonrpc' => '2.0',
			'id' => 'id',
			'error' => array('code' => 1, 'message'=>''),
		);
		if (!empty($option['global']) && empty($_W['isfounder'])) {
			$result['error']['message'] = '没有向 global 文件夹上传图片的权限.';
			die(json_encode($result));
		}
		
		$thumb = 0; 		$width = 0; 		
		load()->model('setting');
		$uploadsetting = setting_load('upload');
		$uploadsetting = $uploadsetting['upload'];
		
		if(!empty($uploadsetting) && is_array($uploadsetting)){
			$thumb = empty($uploadsetting['image']['thumb']) ? 0 : 1;
			$width = intval($uploadsetting['image']['width']);
		}
		if(isset($option['thumb'])){
			$thumb = empty($option['thumb']) ? 0 : 1;
		}
		if (isset($option['width'])) {
			$width = intval($option['width']);
		}
		
		load()->func('file');
		
		if (!empty($_FILES['file']['name'])) {
			if ($_FILES['file']['error'] != 0) {
				$result['error']['message'] = '上传失败，请重试！';
				die(json_encode($result));
			}
			if (empty($_GPC['mediatype'])) {
				$_W['uploadsetting'] = array();
				$_W['uploadsetting']['image']['folder'] = 'images/' . $_W['uniacid'];
				$_W['uploadsetting']['image']['extentions'] = $_W['config']['upload']['image']['extentions'];
				$_W['uploadsetting']['image']['limit'] = $_W['config']['upload']['image']['limit'];
				if (isset($option['global']) && !empty($option['global'])) {
					$_W['uploadsetting']['image']['folder'] = 'images/global';
				}
			} else {
				$_W['uploadsetting'] = array();
				$_W['uploadsetting']['image']['folder'] = 'images/' . $_W['uniacid'];
				$_W['uploadsetting']['image']['extentions'] = array('jpg');
				if($_GPC['mediatype'] == 'image'){
					$_W['uploadsetting']['image']['limit'] = 1024;
				} elseif($_GPC['mediatype'] == 'thumb') {
					$_W['uploadsetting']['image']['limit'] = 64;
				} else {
					$result['error']['message'] = '媒体类型不正确。';
					die(json_encode($result));
				}
			}
			
			if(empty($dest_dir)){
				$file = file_upload($_FILES['file']);
			} else {
				$extention = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$dir = $_W['uploadsetting']['image']['folder'] .'/'.$dest_dir.'/';
				$fname = file_random_name($dir, $extension);
				$file = file_upload($_FILES['file'], 'image', $dir . $fname);
			}
			if (is_error($file)) {
				$result['error']['message'] = $file['message'];
				die(json_encode($result));
			}
			
			if ($thumb == 1 && $width > 0) {
				$srcfile = ATTACHMENT_ROOT . '/' . $file['path'];
				$extention = pathinfo($srcfile, PATHINFO_EXTENSION);
				do {
					if(!empty($option['global'])){
						$filename = "{$_W['uploadsetting']['image']['folder']}/" . random(30) . ".{$extention}";
					} else {
						if(empty($dest_dir)){
							$filename = "{$_W['uploadsetting']['image']['folder']}/" . date('Y/m/'). random(30) . ".{$extention}";
						} else {
							$filename = "{$_W['uploadsetting']['image']['folder']}/" . $dest_dir .'/' . random(30) . ".{$extention}";
						}
					}
				} while(file_exists(ATTACHMENT_ROOT . '/' . $filename));
				$r = file_image_thumb($srcfile, ATTACHMENT_ROOT . '/' . $filename, $option['width']);
				
				@unlink($srcfile);
				if (is_error($r)) {
					unset($result['result']);
					$result['error']['message'] = $r['message'];
					die(json_encode($result));
				} else {
					$result['result'] = $r;
					$result['id'] = $file['path'];
				}
			} else {
				$result['result'] = $file['path'];
				$result['id'] = $file['path'];
			}
			pdo_insert('core_attachment', array(
				'uniacid' => $_W['uniacid'],
				'uid' => $_W['uid'],
				'filename' => $_FILES['file']['name'],
				'attachment' => $result['filename'],
				'type' => 1,
				'createtime' => TIMESTAMP,
			));
			unset($result['error']);
			die(json_encode($result));
		} else {
			$result['error']['message'] = '请选择要上传的图片！';
			die(json_encode($result));
		}
			}
} 
if ($do == 'delete') {
	if (empty($_GPC['file'])) {
		$result['message'] = '请选择要删除的图片！';
		exit(json_encode($result));
	}
	if (empty($_W['isfounder']) && !empty($option['global'])) {
		$result['message'] = '没有删除 global 文件夹中图片的权限.';
		exit(json_encode($result));
	}
	$attachment = $_GPC['file'];
	load()->func('file');
	file_delete($attachment);
	if(empty($option['global'])){
		pdo_delete('core_attachment', array('uniacid'=>$_W['uniacid'], 'attachment'=>$attachment));
	}else{
		pdo_delete('core_attachment', array('attachment'=>$attachment));
	}
	exit('success');
}