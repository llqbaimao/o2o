-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: we7
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ims_account`
--

DROP TABLE IF EXISTS `ims_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account` (
  `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `hash` varchar(8) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `isconnect` tinyint(4) NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account`
--

LOCK TABLES `ims_account` WRITE;
/*!40000 ALTER TABLE `ims_account` DISABLE KEYS */;
INSERT INTO `ims_account` VALUES (10,9,'iUL93baS',1,1);
/*!40000 ALTER TABLE `ims_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_account_wechats`
--

DROP TABLE IF EXISTS `ims_account_wechats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account_wechats` (
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `encodingaeskey` varchar(255) NOT NULL,
  `access_token` varchar(1000) NOT NULL,
  `jsapi_ticket` varchar(1000) NOT NULL,
  `level` tinyint(4) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `account` varchar(30) NOT NULL,
  `original` varchar(50) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `country` varchar(10) NOT NULL,
  `province` varchar(3) NOT NULL,
  `city` varchar(15) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `subscribeurl` varchar(120) NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account_wechats`
--

LOCK TABLES `ims_account_wechats` WRITE;
/*!40000 ALTER TABLE `ims_account_wechats` DISABLE KEYS */;
INSERT INTO `ims_account_wechats` VALUES (10,9,'p1e1qneexn1Eh81HR1J1HE191UHnH1qJ','AdTC4T9eNckCCzEKCEyT9BCc11QccH8Q9NNw4T1NeMU','a:2:{s:5:\"token\";s:107:\"_9vK-CcxvBHk0Nl3IDJA3JzbQBt8Jp007TfR2d2ynL0JbHcMCqWxq0cbyT6wA2NB_gJ-Uf76ocpHFyjWwVRr_6gbhSljdfBtv_oSwyfMow4\";s:6:\"expire\";i:1433162199;}','a:2:{s:6:\"ticket\";s:86:\"sM4AOVdWfPE4DxkXGEs8VJPpB_2yd9wBQiMM_73Qz1n3XA4TGhLwnVhZvBFUA0ASY76Ibp31DGAVslV54N9YDQ\";s:6:\"expire\";i:1433162199;}',1,'芝码客','z-markworld','gh_c32f3210c4c4','z-mark（芝码客）为线下实体店提供海外商品跨境保税展示及相关的供应链服务，消费者在线下合作门店购买跨境商品后，可以通过此系统查询订单物流信息及海关申报记录。','','','','3124432177@qq.com','eef71b9bf4cfaa5ace26c9be5283bf78',1430897349,'wx634ea809d3e6f439','80426dd69b52e4b781bf882936c8859e',0,'');
/*!40000 ALTER TABLE `ims_account_wechats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_account_yixin`
--

DROP TABLE IF EXISTS `ims_account_yixin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_account_yixin` (
  `acid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `token` varchar(32) NOT NULL,
  `access_token` varchar(1000) NOT NULL,
  `level` tinyint(4) unsigned NOT NULL,
  `name` varchar(30) NOT NULL,
  `account` varchar(30) NOT NULL,
  `signature` varchar(100) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(32) NOT NULL,
  `key` varchar(50) NOT NULL,
  `secret` varchar(50) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`acid`),
  KEY `idx_key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_account_yixin`
--

LOCK TABLES `ims_account_yixin` WRITE;
/*!40000 ALTER TABLE `ims_account_yixin` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_account_yixin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon`
--

DROP TABLE IF EXISTS `ims_activity_coupon`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon` (
  `couponid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `type` tinyint(4) NOT NULL,
  `title` varchar(30) NOT NULL,
  `couponsn` varchar(50) NOT NULL,
  `description` text,
  `discount` decimal(10,2) NOT NULL,
  `condition` decimal(10,2) NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) unsigned NOT NULL,
  `limit` int(11) NOT NULL,
  `dosage` int(11) unsigned NOT NULL,
  `amount` int(11) unsigned NOT NULL,
  `thumb` varchar(500) NOT NULL,
  `credit` int(10) unsigned NOT NULL,
  `credittype` varchar(20) NOT NULL,
  PRIMARY KEY (`couponid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon`
--

LOCK TABLES `ims_activity_coupon` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_allocation`
--

DROP TABLE IF EXISTS `ims_activity_coupon_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_allocation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `couponid` int(10) unsigned NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`couponid`,`groupid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_allocation`
--

LOCK TABLES `ims_activity_coupon_allocation` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_password`
--

DROP TABLE IF EXISTS `ims_activity_coupon_password`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_password` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_password`
--

LOCK TABLES `ims_activity_coupon_password` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_password` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_password` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_coupon_record`
--

DROP TABLE IF EXISTS `ims_activity_coupon_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_coupon_record` (
  `recid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `grantmodule` varchar(50) NOT NULL,
  `granttime` int(10) unsigned NOT NULL,
  `usemodule` varchar(50) NOT NULL,
  `usetime` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `operator` varchar(30) NOT NULL,
  `remark` varchar(300) NOT NULL,
  `couponid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`recid`),
  KEY `couponid` (`uid`,`grantmodule`,`usemodule`,`status`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_coupon_record`
--

LOCK TABLES `ims_activity_coupon_record` WRITE;
/*!40000 ALTER TABLE `ims_activity_coupon_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_coupon_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange`
--

DROP TABLE IF EXISTS `ims_activity_exchange`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `couponid` int(10) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `thumb` varchar(500) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `extra` varchar(3000) NOT NULL,
  `credit` int(10) unsigned NOT NULL,
  `credittype` varchar(10) NOT NULL,
  `pretotal` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  `starttime` int(10) unsigned NOT NULL,
  `endtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange`
--

LOCK TABLES `ims_activity_exchange` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange_trades`
--

DROP TABLE IF EXISTS `ims_activity_exchange_trades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange_trades` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `uniacid` (`uniacid`,`uid`,`exid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange_trades`
--

LOCK TABLES `ims_activity_exchange_trades` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange_trades` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange_trades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_exchange_trades_shipping`
--

DROP TABLE IF EXISTS `ims_activity_exchange_trades_shipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_exchange_trades_shipping` (
  `tid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `district` varchar(30) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(6) NOT NULL,
  `mobile` varchar(30) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`tid`),
  KEY `uniacid` (`uniacid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_exchange_trades_shipping`
--

LOCK TABLES `ims_activity_exchange_trades_shipping` WRITE;
/*!40000 ALTER TABLE `ims_activity_exchange_trades_shipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_exchange_trades_shipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_modules`
--

DROP TABLE IF EXISTS `ims_activity_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_modules` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `exid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `available` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `uniacid` (`uniacid`),
  KEY `module` (`module`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_modules`
--

LOCK TABLES `ims_activity_modules` WRITE;
/*!40000 ALTER TABLE `ims_activity_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_activity_modules_record`
--

DROP TABLE IF EXISTS `ims_activity_modules_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_activity_modules_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(10) unsigned NOT NULL,
  `num` tinyint(3) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_activity_modules_record`
--

LOCK TABLES `ims_activity_modules_record` WRITE;
/*!40000 ALTER TABLE `ims_activity_modules_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_activity_modules_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_article_reply`
--

DROP TABLE IF EXISTS `ims_article_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_article_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `articleid` int(11) NOT NULL,
  `isfill` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_article_reply`
--

LOCK TABLES `ims_article_reply` WRITE;
/*!40000 ALTER TABLE `ims_article_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_article_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_basic_reply`
--

DROP TABLE IF EXISTS `ims_basic_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_basic_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `content` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_basic_reply`
--

LOCK TABLES `ims_basic_reply` WRITE;
/*!40000 ALTER TABLE `ims_basic_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_basic_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_business`
--

DROP TABLE IF EXISTS `ims_business`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_business` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `weid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` varchar(1000) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `province` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `dist` varchar(50) NOT NULL,
  `address` varchar(500) NOT NULL,
  `lng` varchar(10) NOT NULL,
  `lat` varchar(10) NOT NULL,
  `industry1` varchar(10) NOT NULL,
  `industry2` varchar(10) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_lat_lng` (`lng`,`lat`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_business`
--

LOCK TABLES `ims_business` WRITE;
/*!40000 ALTER TABLE `ims_business` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_business` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_attachment`
--

DROP TABLE IF EXISTS `ims_core_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_attachment`
--

LOCK TABLES `ims_core_attachment` WRITE;
/*!40000 ALTER TABLE `ims_core_attachment` DISABLE KEYS */;
INSERT INTO `ims_core_attachment` VALUES (89,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432200494),(88,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432200439),(87,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432200085),(86,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432200003),(82,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1431493938),(83,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432112141),(84,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432199506),(85,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1432199531),(81,9,0,'TB1RhdeGpXXXXaiXVXXXXXXXXXX_!!0-item_pic.jpg','',1,1431493874),(80,9,0,'网站备案核验单.jpg','',1,1430906052);
/*!40000 ALTER TABLE `ims_core_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_cache`
--

DROP TABLE IF EXISTS `ims_core_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_cache` (
  `key` varchar(50) NOT NULL,
  `value` mediumtext NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_cache`
--

LOCK TABLES `ims_core_cache` WRITE;
/*!40000 ALTER TABLE `ims_core_cache` DISABLE KEYS */;
INSERT INTO `ims_core_cache` VALUES ('setting','a:3:{s:8:\"authmode\";i:1;s:5:\"close\";a:2:{s:6:\"status\";s:1:\"0\";s:6:\"reason\";s:0:\"\";}s:8:\"register\";a:4:{s:4:\"open\";i:1;s:6:\"verify\";i:1;s:4:\"code\";i:1;s:7:\"groupid\";i:1;}}'),('menus:platform','a:0:{}'),('menus:site','a:0:{}'),('modules','a:12:{s:5:\"basic\";a:16:{s:3:\"mid\";s:1:\"1\";s:4:\"name\";s:5:\"basic\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本文字回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:24:\"和您进行简单对话\";s:11:\"description\";s:201:\"一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:4:\"news\";a:16:{s:3:\"mid\";s:1:\"2\";s:4:\"name\";s:4:\"news\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:24:\"基本混合图文回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:33:\"为你提供生动的图文资讯\";s:11:\"description\";s:272:\"一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"music\";a:16:{s:3:\"mid\";s:1:\"3\";s:4:\"name\";s:5:\"music\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本音乐回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:39:\"提供语音、音乐等音频类回复\";s:11:\"description\";s:183:\"在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:7:\"userapi\";a:16:{s:3:\"mid\";s:1:\"4\";s:4:\"name\";s:7:\"userapi\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:21:\"自定义接口回复\";s:7:\"version\";s:3:\"1.1\";s:7:\"ability\";s:33:\"更方便的第三方接口设置\";s:11:\"description\";s:141:\"自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:8:\"recharge\";a:16:{s:3:\"mid\";s:1:\"5\";s:4:\"name\";s:8:\"recharge\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:24:\"会员中心充值模块\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:24:\"提供会员充值功能\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:6:\"custom\";a:16:{s:3:\"mid\";s:1:\"6\";s:4:\"name\";s:6:\"custom\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:15:\"多客服转接\";s:7:\"version\";s:5:\"1.0.0\";s:7:\"ability\";s:36:\"用来接入腾讯的多客服系统\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:17:\"http://bbs.we7.cc\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";a:0:{}s:7:\"handles\";a:6:{i:0;s:5:\"image\";i:1;s:5:\"voice\";i:2;s:5:\"video\";i:3;s:8:\"location\";i:4;s:4:\"link\";i:5;s:4:\"text\";}s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:6:\"images\";a:16:{s:3:\"mid\";s:1:\"7\";s:4:\"name\";s:6:\"images\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本图片回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供图片回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"video\";a:16:{s:3:\"mid\";s:1:\"8\";s:4:\"name\";s:5:\"video\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本视频回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供图片回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"voice\";a:16:{s:3:\"mid\";s:1:\"9\";s:4:\"name\";s:5:\"voice\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"基本语音回复\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:18:\"提供语音回复\";s:11:\"description\";s:132:\"在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:5:\"chats\";a:16:{s:3:\"mid\";s:2:\"10\";s:4:\"name\";s:5:\"chats\";s:4:\"type\";s:6:\"system\";s:5:\"title\";s:18:\"发送客服消息\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:77:\"公众号可以在粉丝最后发送消息的48小时内无限制发送消息\";s:11:\"description\";s:0:\"\";s:6:\"author\";s:13:\"WeEngine Team\";s:3:\"url\";s:18:\"http://www.we7.cc/\";s:8:\"settings\";s:1:\"0\";s:10:\"subscribes\";s:0:\"\";s:7:\"handles\";s:0:\"\";s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"1\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:8:\"we7_demo\";a:16:{s:3:\"mid\";s:2:\"13\";s:4:\"name\";s:8:\"we7_demo\";s:4:\"type\";s:5:\"other\";s:5:\"title\";s:12:\"官方示例\";s:7:\"version\";s:3:\"1.0\";s:7:\"ability\";s:36:\"此模块提供基本的功能展示\";s:11:\"description\";s:36:\"此模块提供基本的功能展示\";s:6:\"author\";s:12:\"微擎团队\";s:3:\"url\";s:18:\"http://bbs.we7.cc/\";s:8:\"settings\";s:1:\"1\";s:10:\"subscribes\";a:13:{i:0;s:4:\"text\";i:1;s:5:\"image\";i:2;s:5:\"voice\";i:3;s:5:\"video\";i:4;s:8:\"location\";i:5;s:4:\"link\";i:6;s:9:\"subscribe\";i:7;s:11:\"unsubscribe\";i:8;s:2:\"qr\";i:9;s:5:\"trace\";i:10;s:5:\"click\";i:11;s:4:\"view\";i:12;s:5:\"enter\";}s:7:\"handles\";a:2:{i:0;s:8:\"location\";i:1;s:4:\"text\";}s:12:\"isrulefields\";s:1:\"1\";s:8:\"issystem\";s:1:\"0\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}s:8:\"eso_sale\";a:16:{s:3:\"mid\";s:2:\"20\";s:4:\"name\";s:8:\"eso_sale\";s:4:\"type\";s:8:\"business\";s:5:\"title\";s:12:\"分销系统\";s:7:\"version\";s:3:\"2.0\";s:7:\"ability\";s:33:\"分销系统，全民分佣啦！\";s:11:\"description\";s:33:\"分销系统，全民分佣啦！\";s:6:\"author\";s:9:\"陈凌俊\";s:3:\"url\";s:0:\"\";s:8:\"settings\";s:1:\"1\";s:10:\"subscribes\";a:0:{}s:7:\"handles\";a:1:{i:0;s:4:\"text\";}s:12:\"isrulefields\";s:1:\"0\";s:8:\"issystem\";s:1:\"0\";s:10:\"issolution\";s:1:\"0\";s:6:\"target\";s:1:\"0\";}}'),('usersfields','a:51:{i:0;s:3:\"uid\";i:1;s:7:\"uniacid\";i:2;s:6:\"mobile\";i:3;s:5:\"email\";i:4;s:8:\"password\";i:5;s:4:\"salt\";i:6;s:7:\"groupid\";i:7;s:7:\"credit1\";i:8;s:7:\"credit2\";i:9;s:7:\"credit3\";i:10;s:7:\"credit4\";i:11;s:7:\"credit5\";i:12;s:10:\"createtime\";i:13;s:8:\"realname\";i:14;s:8:\"nickname\";i:15;s:6:\"avatar\";i:16;s:2:\"qq\";i:17;s:3:\"vip\";i:18;s:6:\"gender\";i:19;s:9:\"birthyear\";i:20;s:10:\"birthmonth\";i:21;s:8:\"birthday\";i:22;s:13:\"constellation\";i:23;s:6:\"zodiac\";i:24;s:9:\"telephone\";i:25;s:6:\"idcard\";i:26;s:9:\"studentid\";i:27;s:5:\"grade\";i:28;s:7:\"address\";i:29;s:7:\"zipcode\";i:30;s:11:\"nationality\";i:31;s:14:\"resideprovince\";i:32;s:10:\"residecity\";i:33;s:10:\"residedist\";i:34;s:14:\"graduateschool\";i:35;s:7:\"company\";i:36;s:9:\"education\";i:37;s:10:\"occupation\";i:38;s:8:\"position\";i:39;s:7:\"revenue\";i:40;s:15:\"affectivestatus\";i:41;s:10:\"lookingfor\";i:42;s:9:\"bloodtype\";i:43;s:6:\"height\";i:44;s:6:\"weight\";i:45;s:6:\"alipay\";i:46;s:3:\"msn\";i:47;s:6:\"taobao\";i:48;s:4:\"site\";i:49;s:3:\"bio\";i:50;s:8:\"interest\";}'),('wxauth:jovi_lin@hotmail.com:token','s:9:\"823933154\";'),('wxauth:jovi_lin@hotmail.com:cookie','s:441:\"data_bizuin=3001073591; Path=/; Secure; HttpOnly; data_ticket=AgW4SGpZORHtHV2gIhBWPCCoAwEUTfO/2jM5Q/CM24E=; Path=/; Secure; HttpOnly; slave_user=gh_5dd3ab99c2d8; Path=/; Secure; HttpOnly; slave_sid=REtwU0FIbFZTZXgwbHN5bmhZdHZCMjBIdHZST2FCTGVVakhQMGZkcWJ2SURVMXBrdERqTnRWdlN4eUZLQk5xeE9xR3VJQkhpUm9JOFRkWjdSNWhfc3o0d2doNDdia2ViaGxCTFBiazZjYjJ4eGJsUklaUHlodGlmSll1ZVMvQkU=; Path=/; Secure; HttpOnly; bizuin=3001073591; Path=/; Secure; HttpOnly\";'),('wxauth:503226060@qq.com:token','s:9:\"510830663\";'),('wxauth:503226060@qq.com:cookie','s:441:\"data_bizuin=3015115963; Path=/; Secure; HttpOnly; data_ticket=AgU/mweJVsO7q6Gv0hbWdOX1AwGVNTYGcwonSJiOLkM=; Path=/; Secure; HttpOnly; slave_user=gh_ff8db263bffd; Path=/; Secure; HttpOnly; slave_sid=R2d3NGlqR1dDWHJuVVBiZEZpdEFfMm1PVEFVcWROYUI4M2t4b18zZm1VQmhOam1jWU1YWEM3NnhybnJiQURpcEVwR0E5MTIyZ1MwbFMzT3hOeFhqRHJSMzFXZ2lJOTFOTGdrU05PUEpoZTR3UnhoRWp0aGJQNGVNd2VGeUhPSmg=; Path=/; Secure; HttpOnly; bizuin=3015115963; Path=/; Secure; HttpOnly\";'),('wxauth:2507714662@qq.com:token','s:10:\"1055215138\";'),('wxauth:2507714662@qq.com:cookie','s:441:\"data_bizuin=2395036696; Path=/; Secure; HttpOnly; data_ticket=AgWwHq1ZxOcbfp1F5Ear9jQZAwFXoWWpU44xRPVEr50=; Path=/; Secure; HttpOnly; slave_user=gh_1f8c245ce3f7; Path=/; Secure; HttpOnly; slave_sid=UEd2Q2ZtSjRwMUNidXdCVXpWY0F6VHJmY1UyWHM4VU9EQThmSVBqVExoNVk2MUk3MGtPR0RUWlNlZnVEZFp0UkR1VHJTMUVxUFhrcUZNM3YycGp3VGVwQktoRXFjWGtvdW9VR0dVdG9rR3ZxTUtFMVljNTcrZ3d5cXo1ZmdDYkE=; Path=/; Secure; HttpOnly; bizuin=2395036696; Path=/; Secure; HttpOnly\";'),('wxauth:3124432177@qq.com:token','s:9:\"598899614\";'),('wxauth:3124432177@qq.com:cookie','s:441:\"data_bizuin=3087598499; Path=/; Secure; HttpOnly; data_ticket=AgVXgEPRBg2L52AX6/xl+LeZAwGL7ckiTDXQizrkHck=; Path=/; Secure; HttpOnly; slave_user=gh_c32f3210c4c4; Path=/; Secure; HttpOnly; slave_sid=TGdPbGJwX3RHUG92OWoxMmlkRDU3SUo3dURNdHFmem9UNXhYbWNvYUhxZnBjNVpQbUVBVmphZmc5ano0c3lqcHFNem5VWllIeXFldnJwN1EzZGM3djFua3dBWXQwUndaUHp1Q1l1REdPV051U0dXc2hPQitmUzRxM01QMHhER1Q=; Path=/; Secure; HttpOnly; bizuin=3087598499; Path=/; Secure; HttpOnly\";');
/*!40000 ALTER TABLE `ims_core_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_paylog`
--

DROP TABLE IF EXISTS `ims_core_paylog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_paylog` (
  `plid` bigint(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `uniacid` int(11) NOT NULL,
  `openid` varchar(40) NOT NULL,
  `tid` varchar(64) NOT NULL,
  `fee` decimal(10,2) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `tag` varchar(2000) NOT NULL,
  `paymentsn` varchar(145) DEFAULT NULL COMMENT '支付单号',
  PRIMARY KEY (`plid`),
  KEY `idx_openid` (`openid`),
  KEY `idx_tid` (`tid`),
  KEY `idx_uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=2015021436 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_paylog`
--

LOCK TABLES `ims_core_paylog` WRITE;
/*!40000 ALTER TABLE `ims_core_paylog` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_paylog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_performance`
--

DROP TABLE IF EXISTS `ims_core_performance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_performance` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL,
  `runtime` varchar(10) NOT NULL,
  `runurl` varchar(512) NOT NULL,
  `runsql` varchar(512) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_performance`
--

LOCK TABLES `ims_core_performance` WRITE;
/*!40000 ALTER TABLE `ims_core_performance` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_performance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_queue`
--

DROP TABLE IF EXISTS `ims_core_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_queue` (
  `qid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `message` varchar(2000) NOT NULL,
  `params` varchar(1000) NOT NULL,
  `keyword` varchar(1000) NOT NULL,
  `response` varchar(2000) NOT NULL,
  `module` varchar(50) NOT NULL,
  `dateline` int(10) unsigned NOT NULL,
  PRIMARY KEY (`qid`),
  KEY `uniacid` (`uniacid`,`acid`)
) ENGINE=MyISAM AUTO_INCREMENT=240 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_queue`
--

LOCK TABLES `ims_core_queue` WRITE;
/*!40000 ALTER TABLE `ims_core_queue` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_resource`
--

DROP TABLE IF EXISTS `ims_core_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_resource` (
  `mid` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `media_id` varchar(100) NOT NULL,
  `trunk` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `dateline` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `acid` (`uniacid`),
  KEY `type` (`type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_resource`
--

LOCK TABLES `ims_core_resource` WRITE;
/*!40000 ALTER TABLE `ims_core_resource` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_sessions`
--

DROP TABLE IF EXISTS `ims_core_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_sessions` (
  `sid` char(32) NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `data` varchar(5000) NOT NULL,
  `expiretime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_sessions`
--

LOCK TABLES `ims_core_sessions` WRITE;
/*!40000 ALTER TABLE `ims_core_sessions` DISABLE KEYS */;
INSERT INTO `ims_core_sessions` VALUES ('e20jvichqcl2cjeskl9t8jh4j1',9,'APP','uid|s:3:\"113\";',1433257266),('lebcvlvog9tvim2n3g12u0epg4',9,'APP','',1433252410),('jnd5qkjn0bnl287120kd6vo7n6',9,'APP','',1433251950),('v04bbq2ibo34lf53m03rashj01',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1433249427),('lqvqg1q5i25qc39s87peq0be70',9,'APP','',1433248926),('d4r81f7nk58k6ggkf1v74jh0i1',9,'APP','uid|s:3:\"112\";',1433232890),('98kscd7p9sm6dd0tc8h3i6pr27',9,'APP','uid|s:3:\"112\";',1433232314),('o2e8067osi8at9cvr9j32tk834',9,'APP','',1433232209),('nttrpm46rtskna3v4sjmkeq817',9,'APP','',1433232173),('fmfhrc844jnuvj2jvoadgrqnf0',9,'APP','',1433232161),('vkfvpe08q5r23rhav3mqthe9s3',9,'APP','',1433232014),('plmdk0kjq3qruasnukrg9vde30',9,'APP','',1433231776),('n4t5v493hmuui2rhihp3smq6h2',9,'APP','',1433231721),('ettsb7igqcm9m4qv49n1a2vak5',9,'APP','',1433231609),('psjn4ioha86lgltm6836rbo1r0',9,'APP','',1433231595),('l1cokntv9idjlq6ddcl45cv567',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1433232150),('im79cie0a2i21gin9dk6jsnet1',9,'APP','',1433231541),('0if9u8ihupraukigge5o3e8cm7',9,'APP','',1433231530),('ge6lj4hnca8277999nq3os90e1',9,'APP','',1433231479),('vi4rqih81lm2lp1dcs2n5c0ki3',9,'APP','',1433231475),('cbvq23e9gaaf8aunbcb411aol7',9,'APP','',1433231341),('r1a39n8db1td8bg2u6b4fnnmp1',9,'APP','',1433227616),('jo4nbt5ar0fl9qci6nkftljog5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1433230085),('en69dkgkd8c9ctbtlqv4pkafv1',9,'APP','',1433226750),('flhb8h1s6g2gdfl5f8nc9162f5',9,'APP','',1433223314),('v77ip98mt84krf9ttnjm9ougf6',9,'APP','',1433231114),('3078tbkrn8vpqifal4jatn60d1',9,'APP','',1433167950),('15v49o4rrs1iriafd8f7c34ub7',9,'APP','',1433159807),('4jugefm9dcb7kvprpf3f6h6gc7',9,'APP','uid|s:3:\"110\";',1433159195),('8n0g60jmk5a9hk3fkl7m63nvs6',9,'APP','',1433158199),('ii1pe0cnalm144ecnngufi5m44',9,'APP','',1433153517),('3a9g9753bar9a2i3qj2jn4cdd7',9,'APP','',1433153095),('pke0m7lsk00ootsr5qsb7d4ta5',9,'APP','',1433153086),('lm98f7djtess0r57gnm79nhi07',9,'APP','',1433153070),('miqjpq4rsaon8tkjqnct1m6rf4',9,'APP','uid|s:3:\"109\";',1433156155),('vatl4oo1vcl0feu48862btlc06',9,'APP','',1433152294),('0lqlfh60ijfc17ojclfllr8ge0',9,'APP','',1433152270),('i7o67d1tar08u43ou680nn7d25',9,'APP','',1433151813),('3igfosqhlnsdvn72ubc392fo85',9,'APP','',1433151795),('ercfs7glthjbajmprm5iikbmi5',9,'APP','',1433151710),('1t758r10cp8pse5fg0plkag227',9,'APP','',1433151480),('6pj43qcpl5m0omtdcffqt7jrk6',9,'APP','',1433151312),('j7mk5afn0gadek46a6pro2mu73',9,'APP','',1433151175),('506qbl9viselrkbc75mo99elf5',9,'APP','vipid|s:2:\"12\";vipname|s:15:\"合肥经销商\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1433152952),('afg5f88p2ikenmrcuoamm0oe67',9,'APP','',1433151161),('hmmg1878eum5jfsprpalo3bvb4',9,'APP','',1433151086),('d9blsb12mot0fdcpdl9kmalq50',9,'APP','',1433151049),('t9h38lgst0rpr6hgrqvih2ip65',9,'APP','',1433150873),('vvp7pbp4kp16hg2cdeqrmhggg0',9,'APP','',1433150867),('f2gs3aslgp9l6rneoom7eg4tp1',9,'APP','',1433160956),('u51jpe16vs04hsg6tcs0qllb97',9,'APP','uid|s:3:\"111\";',1433231776),('r22n4omp4kmhb9rel2e30ibbf5',9,'APP','',1433150729),('37p572l44jm2hjk0l942c5bu55',9,'APP','',1433150674),('irh0n8ifftcqhch1jg0hd2o627',9,'APP','',1433150590),('8ei109m8or4f1a4ant58aj1f33',9,'APP','',1433145943),('f8e3n71imvc99toeajasd03o71',9,'APP','uid|s:3:\"103\";',1433174139),('5sfeuir5hb52igi68jovhda8i7',9,'APP','uid|s:3:\"103\";',1433144516),('tg573l3t30hrg3si9plpll5om7',9,'APP','uid|s:3:\"103\";',1433142537),('jd1n540bkfa2q5gqi8kuenca70',9,'APP','',1433162883),('kuagju2934dlcemnacm7v4rtj7',9,'APP','vipid|s:1:\"5\";vipname|s:15:\"合肥经销商\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1433255064),('5hp8jr3aifjbc9nmceq5i60ad5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1433129651),('foeks2bvprkvrahk0sd6h3mdd5',9,'APP','',1433127692),('2er0vq282losi66h9es2ktu6l7',9,'APP','',1433124904),('vcd1moptndeagf7qp1h17381m5',9,'APP','',1433081698),('4t6dcniqbj0fh076os932blq26',9,'APP','',1433081667),('vkqeuthsli87oubn18e254b875',9,'APP','',1433081596),('nnp610ftf2r6245q2qbfjcr9l6',9,'APP','',1432948527),('mpk8aans7n0mp15o4m31jmihn2',9,'APP','',1432915235),('c7e716so360g86ma95l8a0t1u4',9,'APP','uid|s:3:\"103\";',1432899877),('ahqhmsdm1nqdpc969p1l8anfs7',9,'APP','',1432879517),('qfjrvu05l2q2sokkn7h5a6i4f6',9,'APP','',1432818008),('4gukumc6ivcvhqj6mme9269k85',9,'APP','uid|s:3:\"103\";',1432817600),('3cnu57a8t2o5p4mk9n5vtv2b74',9,'APP','uid|s:3:\"103\";',1432871839),('28ja3jd617ur7eag13ce5g2cm0',9,'APP','',1432817092),('e3uhrqrohbjsuo5qe7opvp91b0',9,'APP','uid|s:3:\"103\";',1432800907),('sh6s8d3jo8h723lcsps2gen4b0',9,'APP','',1432798411),('gfmvp7suaa76jdrohrklaja6m0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432797605),('fc59koamfnlt4kuuvlqhlagve2',9,'APP','',1432796376),('esc3i1tq6dghkj4vlchctefea1',9,'APP','',1432795333),('54529phsl4jcveklkgk0u935i2',9,'APP','dsh|s:1:\"0\";',1432795321),('jm9amludantm9i4n46v3b58ef0',9,'APP','',1432783933),('53a6lvt1ud8n4q2iqt4f2fmcq3',9,'APP','',1432750977),('9j9o7r9jq0rj6dp177k7frb3f1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432642794),('qjlek6njian1f827iuhmu0l212',9,'APP','uid|s:3:\"103\";',1432626080),('95541ghdgfu1i0kkkueb04trp4',9,'APP','',1432613785),('ig1bfj8q4gd6k6jgi4kaqi9qt7',9,'APP','uid|s:3:\"103\";',1432553135),('8nqthklifdberkdes4fpegb0a5',9,'APP','',1432550626),('b74sif1udreimaja3dkd232j22',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432538724),('5pa5ic52bnintr7epc7orn8p50',9,'APP','',1432538690),('3mns0nndbouuu5i94ictsat8j1',9,'APP','uid|s:3:\"103\";',1432542931),('069ef21j60fos4muk8ctd43kb4',9,'APP','uid|s:3:\"103\";',1432538115),('eiqh3vv4nkjbg85n5ourcatdg3',9,'APP','uid|s:3:\"103\";',1432455558),('0eaudtnso72m2a8iei5obtri11',9,'APP','uid|s:3:\"103\";',1432455306),('9cfm6lqgmapookg93qugoii483',9,'APP','',1432436296),('ggvn55vofsokdmp3cksnr0a742',9,'APP','dsh|s:1:\"0\";vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1432436462),('gk6ovf8g13ukmg6s89tptvaur6',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432436522),('0e261qvfnv3opvn1j6q694es30',9,'APP','',1432354003),('bv0u564vdvltlljp63686gpse2',9,'APP','',1432310946),('kem6gmma3ihskt2684uq2ulsg5',9,'APP','',1432310770),('d0rkuudu02dsbnqapghmjl98g1',9,'APP','uid|s:3:\"103\";',1432316766),('2fhocjst44ajq6jje21h8gor25',9,'APP','',1432309972),('642ap35mdeg0eme7mgelb9h6u6',9,'APP','',1432303757),('r6k8nb6dp3r88f9u76ejifdtv2',9,'APP','',1432303762),('n6a6nt7cgbkesfumphj7egf5e3',9,'APP','',1432302961),('7ct1usgv2k4hfiqchhmsfinqv3',9,'APP','',1432302933),('0qdobmd5jmapnubg9kqd0tnig3',9,'APP','',1432302919),('as67haffiffrrhfbme88ufmdn0',9,'APP','',1432302793),('hs7j3bob1q0hulupuv62bfcou5',9,'APP','',1432302767),('fdl6faoogpk5om3uu23pki7236',9,'APP','uid|s:3:\"103\";',1432719461),('fk4t5r6483so8p5danedavk2q6',9,'APP','',1432869549),('i1pm118eep1lqd3vo04j67gl62',9,'APP','',1432298252),('bbr1q6dfign96ogdh7s1gg46r7',9,'APP','',1432298254),('h19i6prk8rpmo89ilb6130urc5',9,'APP','',1432291707),('kuujmfik573el7ve8bb3slrtr6',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432291707),('i3949oskrbvtc96u9ap55s2ic4',9,'APP','',1432276833),('770b4foo5kqma25r3ufuqh01v6',9,'APP','uid|s:3:\"103\";',1432292497),('nq0ee21ot75bu9g70ci3tmpdm7',9,'APP','',1432291227),('rv2qga094laq4d0mn4ucfk8v53',9,'APP','vipid|s:1:\"7\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1433081649),('6g8cv3as2i1jet8b82j2nalkl0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432261927),('2cma1gfg2ukv1bp1a63vaelqo2',9,'APP','vipid|s:0:\"\";vipname|s:0:\"\";vip|s:0:\"\";flag|s:0:\"\";dsh|s:1:\"0\";',1432260550),('an0e9iusgt7vustl657u73no25',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432224214),('d110bf0slgraofcj65qghrjjs6',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432223952),('aqkuh5qn9cnpf67ijbrf7ndaa1',9,'APP','',1432223794),('lpbuou5fs8sgi73mr1u6ivbic1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432225161),('vk2gu8fmhlba80rt726djufud3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"1\";',1432212821),('ia4b79kkrl6ece334kf7gs19o7',9,'APP','',1432211094),('kv8korkegnhd9l1ddvo6f4ofu1',9,'APP','uid|s:3:\"108\";',1432210267),('hpoiv30tvr37jodioo872ivc50',9,'APP','vipid|s:1:\"2\";vipname|s:1:\"A\";vip|s:1:\"2\";flag|s:1:\"0\";dsh|s:1:\"1\";',1432211041),('oj6p8lgh9ckukjlh9dlk2v0sc1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432200899),('qj1396glrk4lq0jotbei9sd385',9,'APP','',1432196005),('92tuf6epufhkrife9q91qlej97',9,'APP','',1432191896),('hlu674vmdd93dqlo3heufhkvn2',9,'APP','',1432191085),('c4i831v71a7q1200otqphrj547',9,'APP','',1432189565),('n0clto60cvi3p7ro17gnq3jfj1',9,'APP','uid|s:3:\"106\";',1432197223),('o859r8subvuo8vdg0bn6knpqa0',9,'APP','',1432187688),('8vajdqja48qojccsa2rl5vthf4',9,'APP','',1432187511),('acab26pigtpmhlu3ongg1q3182',9,'APP','',1432187506),('am04mpnfde02tfg4pm8ds1br37',9,'APP','',1432179933),('luku4q946tnq6dsijg7f6s2n33',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432175326),('ojpou0485p1rlg35kehaad4b05',9,'APP','',1432175111),('2v635vg9k5nboutffh1afce6a6',9,'APP','',1432141877),('dpimc57j43rkcklkmnr8d43745',9,'APP','',1432141766),('uj0v6utanmjnmkqhuo5aqejdr5',9,'APP','',1432141759),('f7ni5n4cib01dvd99an4bf6kq2',9,'APP','',1432141746),('7p1ggoi5ij0h7460lu68f85tr3',9,'APP','',1432141733),('3nv1ocncsp5odm9h6pr038hmn1',9,'APP','',1432141717),('n988940drlvsib6j4cm76lt173',9,'APP','',1432187477),('pfkd0j6ulb2r9gg1pl8spg7797',9,'APP','',1433158139),('3452rm4juoag6j5t24du2etnq0',9,'APP','',1433158143),('lrfe9id62f8lk9o1de5b32r4o5',9,'APP','',1433150495),('j5i0idss0t34p4ohtdd6ebtsv7',9,'APP','vipid|s:1:\"4\";vipname|s:9:\"程筠曦\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1433232156),('un5hhant3tdg8354oiq6lqknv6',9,'APP','',1432138912),('l9pciv8h8aj0hb9q9gvr0mm8q4',9,'APP','',1432138798),('m010eue6phtpgssvtda8g4al12',9,'APP','',1432138212),('7kkk8apef69c8e662ra44ljdb0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"1\";',1432211477),('fl96sjn9vn2jtl0lfr0lg1cat0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432136717),('q7bmv2nl8iv9vln33a9pvcpp07',9,'APP','dsh|s:1:\"0\";vipid|s:2:\"27\";vipname|s:1:\"B\";vip|s:1:\"3\";flag|s:1:\"1\";',1432135484),('5k4apkr65ndknu2tv3bv0jmke7',9,'APP','vipid|s:2:\"28\";vipname|s:15:\"丹阳分公司\";vip|s:1:\"2\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432140583),('ot47l2nk0a35794k3rh87qnhe6',9,'APP','',1432139896),('pbfkjvfbi1nhm1af22i9uvkn33',9,'APP','dsh|s:1:\"0\";',1432115013),('o1eaphui47h07eap9qngae46i0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432114944),('rimf5de3eu2fmvarj7ub37jtt4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432025569),('3dqnpa9lr3thkm81ppb9rduee6',9,'APP','',1432018409),('gpl2postj8h15haauvgl2ub317',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432023921),('in8k25j7tvuk0v8j34td92mmb1',9,'APP','vipid|s:2:\"21\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1432037665),('tu5cec665aod25dk96l9cgr800',9,'APP','',1432025688),('6vdqkbfs14oqhf06kmlgqpgqa2',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431954738),('dcvpjbpfctisatuislcp59dfv1',9,'APP','vipid|s:2:\"21\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1431956343),('84s0bj41h7afur1qdtkhi3pvu6',9,'APP','',1431934283),('vpes6s11tautjecekcecjp6jk4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431933811),('6fnp9s2vbd62tp8jrcs7980at3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431932642),('5nuj53ksus6oh0gidn2a6jert7',9,'APP','',1431932443),('vmhp355o6s0brggqb3j9cp08o1',9,'APP','',1431932348),('j4aebgk3f7e8earcb67cfj5s82',9,'APP','',1432141650),('7jb53lperg5cvhjv1kr1798n41',9,'APP','',1432187663),('em0prd5u58vuseprvgln9eopi0',9,'APP','uid|s:3:\"106\";',1431931685),('4kugcdu9n4tqnn0letc61rina1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431885689),('8lc9a3dhq5ksurdc0obqdujfp0',9,'APP','',1431879096),('tvlgjmrehki1q3570ocqmif9q2',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431879227),('9jkufful1vj5r6oupup7ctja46',9,'APP','dsh|s:1:\"0\";vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1431878379),('tpv8qbgdluvohr7qc6iaalcvm4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431871803),('fjsf0b9ifduhddtrvcq5r2ujl3',9,'APP','vipid|s:2:\"27\";vipname|s:1:\"B\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1432117462),('8foolcrgphpi3qoi7gklu8l6m5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432036275),('05hkmpgkpt86vsu0eatmsbnjq6',9,'APP','dsh|s:1:\"0\";vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1432142705),('qb3v1fv0eon4mrk5lmkolfhij4',9,'APP','',1431870262),('jcqubdtlepp7o228aprs9en5i2',9,'APP','dsh|s:1:\"0\";',1431870117),('esscbp6gr794ppohi7vifrlna0',9,'APP','uid|s:3:\"105\";vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431870834),('5pko9n85lejncuouo7aknj5qm1',9,'APP','',1431869420),('0mei6a56nlffq4qv8pqd7p3f03',9,'APP','',1431869321),('m5mo50m3j0tirut1cbc1te8cq5',9,'APP','',1431869243),('q01a1tftjumotect92s0h44543',9,'APP','',1431866253),('i5tol78ro5hd8clo054p8g11l7',9,'APP','',1431865065),('vqgf30rtjfrrqa7ibnvpmpsq07',9,'APP','',1431864768),('i8lhnn980pidj7jh2ftmf4js26',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431864609),('cj6c90v7hh94uliso82avnhho4',9,'APP','',1431864407),('q0ag4r5u7a8k5i52u7lktkf372',9,'APP','',1431864151),('8gull4f890dth2el3t22p83do0',9,'APP','',1431864133),('hrvvmprgqno78affo9nmtt5ro4',9,'APP','',1431862959),('ainomic3gelqgtqadj7m3g6iq4',9,'APP','',1431863072),('sr1179v3174duoaeh3der4jvp0',9,'APP','',1431862918),('qbtnuv7tubvmigqlj6mt68rnu4',9,'APP','',1431862948),('2herhmjbfre38ms266b2ilaiu7',9,'APP','',1431862635),('tkv0fdqtj97s6l4ari4fimd4h3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";uid|s:3:\"105\";dsh|s:1:\"0\";',1431868753),('8tlogqd1u19bcg59n47n1tvis5',9,'APP','',1431716087),('5slqm6702unl5ht9ua6cr8d934',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1432042175),('e8vhsrno46t5ap2qtq8od5h6d7',9,'APP','',1431859906),('iiklbsrk1721pv6ql02vkdnig1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431718164),('ec53sli5tunjl5u434cpqs3ml4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431718065),('srbnvv03deo8g37ga2q792erp2',9,'APP','uid|s:3:\"101\";',1431718733),('2731suung7at7tc4mvpqdv3sf7',9,'APP','',1431770827),('l1b8jd421hgkj986ph9168s7a1',9,'APP','vipid|s:2:\"21\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"3\";',1431755690),('l6qc8mgv9kit9dtmnu6ceiqeq7',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431754975),('h3uu44h0cc863nuqo5bk91fap3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431777264),('c21und80ggtltpg37muno0h461',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431777469),('qp677ki4voinoobp17ogm2mtj0',9,'APP','',1431938419),('6pjurdr8sh1i14kjcuila8ftg7',9,'APP','uid|s:3:\"103\";',1431805872),('qprjq11af49hcmv5n79oqucsn6',9,'APP','',1431847033),('56p7ldcd8j556fjsdcvu63utl6',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431848191),('dqthrk6fudnnv2u2df4eu4rka7',9,'APP','',1431847155),('ns8si40qmjpuno2e16gq5qoot1',9,'APP','',1433231721),('r3pgmu41o2i22ia732g8aoqte5',9,'APP','uid|s:3:\"103\";',1431959525),('tl4g11ggigt6hl8tgqfl52cso0',9,'APP','',1431845471),('3p9bg9gee02nb3up8rdbqfcrg2',9,'APP','vipid|s:2:\"21\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1431848323),('3iraf4vs1t774l41vqa8sbq4v3',9,'APP','',1431845483),('ms6jlang9rcmmv6ht3dp6nhti0',9,'APP','',1431845701),('fgbu9arp766b2a0o4oenq8le81',9,'APP','',1431847165),('60p7hpnue7gvea75gjt8ccd7u6',9,'APP','',1431847205),('uli9a1tbtdsmnjdirf8fh9u555',9,'APP','',1431848304),('i91suhcltspu34s1psm5v72525',9,'APP','',1431848329),('qf3q6kset0umdkbu949rnc8eh2',9,'APP','',1431848336),('tpmj91pbqj4ggihpa6hfo6v4h4',9,'APP','',1431848338),('5s34db6qf23h2trk3qf258lch4',9,'APP','',1431848340),('esbcja1ppdsanar9b9pqhsv125',9,'APP','',1431851731),('nnj93ahi3k7qab80hg9gch9md5',9,'APP','',1431852729),('f8ap9sreu1mmmg5s83o0rvu7j2',9,'APP','',1431852732),('qolfml0b05mi7or2d4k60mbf53',9,'APP','',1431859906),('3c35rgrfr5gfaba017m5h3qmd7',9,'APP','',1431864192),('a9gdg7qfmi43lvtnon3e4milm5',9,'APP','',1431860832),('1bnmj5hkv7b1t0g66tvm3mad95',9,'APP','',1431860837),('tpeirl6vv6pe5fha8br2tla770',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431861694),('063cn94n3b55qfbgkio01e3d90',9,'APP','',1431861654),('t36hj19kv0rud365lomh2vlrk1',9,'APP','',1431861788),('0ktt0en4gt1oma6pslksrc5pc6',9,'APP','',1431862285),('tuujvvrncc6beglfi0uo4fqc44',9,'APP','',1431862293),('c9ovjglnc3t7unstdknnj54c46',9,'APP','',1431862293),('r0ajj9e7i0i4feulrpvtjqqf41',9,'APP','',1431862555),('lidjm2d9vbpb0ialkip3slmuo1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431718133),('50ifobm5tfb0kifj768g0d48g2',9,'APP','vipid|s:2:\"21\";vipname|s:2:\"BB\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"3\";',1431719708),('4qgett5s0d51h72jgptv0k2pa0',9,'APP','',1431710479),('3bu6t89gbdrf56evh1v8f31fh5',9,'APP','',1431708208),('k4bqmb6b4h0biav0af2g264vj0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431710410),('83h3v8ou2vfceajn72rluprj61',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431712149),('b1aeud1uoq24rvodeq9aae03d1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"3\";',1431708492),('fgf31or6pg6lt4tpjbpft7n427',9,'APP','',1431713974),('bvprn9q29bllfpjq159833nkr3',9,'APP','',1431707693),('4ggnfsdii16trg98mq6965h2t1',9,'APP','',1431664972),('fbc9uutfk5tv9fm3inqti54v66',9,'APP','',1431590976),('oiabngg4ugjd651ov5ag6c1jd5',9,'APP','uid|s:3:\"100\";',1431507245),('787j5694pred94mhlmdhrnc2p1',9,'APP','',1431590143),('p37v0la9hlj2l6cfjn99sqa5u2',9,'APP','',1431590196),('5vc5gmdnso9qd471jbkpj5e6a1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1431490598),('n5h6mrosvqajbikhubbqh0gfj7',9,'APP','',1431490612),('rroj55i1h4c4uohlgdsbn0kr50',9,'APP','',1431490646),('klp6711g1tiidcnep03fadrt01',9,'APP','',1431490829),('ql1drbvsadurqf6gb9qmdmrf83',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431507386),('fhojjttqpljejunr78ov29rie0',9,'APP','vipid|s:2:\"18\";vipname|s:1:\"A\";vip|s:1:\"2\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431506971),('lnsos0rq253opvem5tsao7agb0',9,'APP','',1431428537),('plh2bs0rc95jor6q4tcsineng3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1431489358),('o9p7j18eb2segesu8gf0179gb5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431590533),('sttmil8asamtqcngq6m7r0fe35',9,'APP','',1431427882),('la0dl038vqidsm6bedf8rns5b0',9,'APP','',1431427837),('kkn42anbn6nenn6cr49fgerka5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431427823),('6l66m6to8i9nnc4p8sph58g8u0',9,'APP','',1431427807),('q8p5p05l039kin9e78u3bghvi3',9,'APP','',1431427726),('qf6hmo6k1aaqpl52cl61746243',9,'APP','',1431425806),('pnroramkfnnuoq136s6n0d7lj6',9,'APP','vipid|s:2:\"17\";vipname|s:4:\"jovi\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1431425780),('3mqcrmj9fkpq1j8lfi0inifo17',9,'APP','',1431487562),('ov9700i25huoh16pnhg6k02727',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1431489341),('nmlcc6ctgs5o6ni7nr9s42m8l0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431150878),('pmlje6cdscn886rms7i43c8ar0',9,'APP','',1431187397),('p023u8jpuge9dbnn7dghgn7o30',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431406177),('06ve5su8b7qrpr9s4142q9cvr6',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431347701),('56fcs0u02r3s5v95m1dvvpvqa5',9,'APP','',1431347087),('4gp3eeegis00mcfhn93kfu1gl2',9,'APP','',1431347125),('c51kqng1gigkupp8a2gdd6g3v5',9,'APP','vipid|s:2:\"17\";vipname|s:2:\"gs\";vip|s:1:\"3\";flag|s:1:\"1\";dsh|s:1:\"0\";',1431348240),('nsu34ctjs5tig72cdeteis9p00',9,'APP','vipid|s:2:\"15\";vipname|s:1:\"A\";vip|s:1:\"2\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431073782),('h61ro9r8vgjp6nr133vrcl6h70',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431073588),('931pl7avrka5krmcn8frska9o2',9,'APP','',1431066131),('dvn35ticm9rusois9trvvdn465',9,'APP','',1431059842),('r8td72u1plj704agji4u3khv42',9,'APP','vipid|s:2:\"12\";vipname|s:6:\"苏州\";vip|s:1:\"2\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431061896),('p94ioj8psam6gh9gp2mqvm98e1',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431072457),('knltbe4jdd5ikkltv8mtec5ms4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431059867),('hqo3n6d5lkdm1is3pe88g40j94',9,'APP','',1431069457),('ukqf0st6npcckal9a5ktq0viq5',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431059691),('78topl8k0bb9iopef68irmlfb1',9,'APP','',1431059602),('0bgbgtv6smd70o1s6dbd68seh4',9,'APP','',1431059601),('f43au36v09c9ksmdilucjj5le0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431016489),('831q625afatluand2m4nkf85j6',9,'APP','',1431013180),('4qkgafj6kr8o567qgcvg9h5m53',9,'APP','',1431013422),('iqkvpb8kn18egql94tfbf3f9r2',9,'APP','',1430901603),('p37i2rg9tlpd0vqdio3r1i9rt4',9,'APP','',1430901374),('hms9jv0e653j8gjgb4nmcnba01',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1430903034),('a1pm21ctgi0vcrn0higd9g3943',9,'APP','',1430901327),('bobh237d5orovep65sde2av8h0',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1430901374),('rn8kjjhagestq26dmmrs900gr3',9,'APP','',1430901320),('d2qp5jsheiaj2ivhm8tpeb9bq4',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1430901382),('f4377c773fb190dc804f966b0bea57c3',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";',1430910427),('7per6bt23uffvas6tv9d50ib54',9,'APP','vipid|s:1:\"1\";vipname|s:9:\"总公司\";vip|s:1:\"1\";flag|s:1:\"0\";dsh|s:1:\"0\";',1431079104);
/*!40000 ALTER TABLE `ims_core_sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_settings`
--

DROP TABLE IF EXISTS `ims_core_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_settings` (
  `key` varchar(200) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_settings`
--

LOCK TABLES `ims_core_settings` WRITE;
/*!40000 ALTER TABLE `ims_core_settings` DISABLE KEYS */;
INSERT INTO `ims_core_settings` VALUES ('authmode','i:1;'),('close','a:2:{s:6:\"status\";s:1:\"0\";s:6:\"reason\";s:0:\"\";}'),('register','a:4:{s:4:\"open\";i:1;s:6:\"verify\";i:1;s:4:\"code\";i:1;s:7:\"groupid\";i:1;}');
/*!40000 ALTER TABLE `ims_core_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_core_wechats_attachment`
--

DROP TABLE IF EXISTS `ims_core_wechats_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_core_wechats_attachment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `filename` varchar(255) NOT NULL,
  `attachment` varchar(255) NOT NULL,
  `media_id` varchar(255) NOT NULL,
  `type` varchar(15) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `media_id` (`media_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_core_wechats_attachment`
--

LOCK TABLES `ims_core_wechats_attachment` WRITE;
/*!40000 ALTER TABLE `ims_core_wechats_attachment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_core_wechats_attachment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_cover_reply`
--

DROP TABLE IF EXISTS `ims_cover_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_cover_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `module` varchar(30) NOT NULL,
  `do` varchar(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_cover_reply`
--

LOCK TABLES `ims_cover_reply` WRITE;
/*!40000 ALTER TABLE `ims_cover_reply` DISABLE KEYS */;
INSERT INTO `ims_cover_reply` VALUES (9,9,0,19,'eso_sale','fansindex','代理入口','','','./index.php?i=9&c=entry&do=fansindex&m=eso_sale');
/*!40000 ALTER TABLE `ims_cover_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_custom_reply`
--

DROP TABLE IF EXISTS `ims_custom_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_custom_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `start1` int(10) NOT NULL,
  `end1` int(10) NOT NULL,
  `start2` int(10) NOT NULL,
  `end2` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_custom_reply`
--

LOCK TABLES `ims_custom_reply` WRITE;
/*!40000 ALTER TABLE `ims_custom_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_custom_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_address`
--

DROP TABLE IF EXISTS `ims_eso_sale_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `province` varchar(30) NOT NULL,
  `city` varchar(30) NOT NULL,
  `area` varchar(30) NOT NULL,
  `address` varchar(300) NOT NULL,
  `isdefault` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_address`
--

LOCK TABLES `ims_eso_sale_address` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_address` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_address` VALUES (1,9,'111','程祺','13916933369','上海市','上海辖区','虹口区','上海虹口区株洲路399弄9号503',1,0),(2,9,'112','邹雅静','18068061759','江苏省','苏州市','昆山市','北后街7号403',1,0),(3,9,'113','李罗琦','18621816275','安徽省','合肥市','庐阳区','长江路大众巷7栋608',1,0);
/*!40000 ALTER TABLE `ims_eso_sale_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_adv`
--

DROP TABLE IF EXISTS `ims_eso_sale_adv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_adv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `advname` varchar(50) DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `enabled` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_enabled` (`enabled`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_adv`
--

LOCK TABLES `ims_eso_sale_adv` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_adv` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_adv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_bb`
--

DROP TABLE IF EXISTS `ims_eso_sale_bb`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_bb` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `bbh` varchar(250) DEFAULT NULL,
  `nr` text,
  `time` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_bb`
--

LOCK TABLES `ims_eso_sale_bb` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_bb` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_bb` VALUES (1,'4.2版','4.2','4.0完整安装包',NULL),(2,'ims_eso_sale_goods_dls_lsj','4.3','ims_eso_sale_goods_dls_lsj','1433225299'),(3,'ims_eso_sale_goods_dls_lsj','4.4','ims_eso_sale_goods_dls_lsj','1433225413');
/*!40000 ALTER TABLE `ims_eso_sale_bb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_business_no_history`
--

DROP TABLE IF EXISTS `ims_eso_sale_business_no_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_business_no_history` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `businessNo` varchar(256) NOT NULL,
  `createTime` varchar(64) DEFAULT NULL,
  `updateTime` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=565 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_business_no_history`
--

LOCK TABLES `ims_eso_sale_business_no_history` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_business_no_history` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_business_no_history` VALUES (554,'JKF_XGYX201506011453328457_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601145352_649','2015-06-01 14:53:53','2015-06-01 14:53:53'),(555,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601165100_773','2015-06-01 16:51:00','2015-06-01 16:51:00'),(556,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601171339_740','2015-06-01 17:13:40','2015-06-01 17:13:40'),(557,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601175227_951','2015-06-01 17:52:28','2015-06-01 17:52:28'),(558,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_799','2015-06-01 18:44:43','2015-06-01 18:44:43'),(559,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_249','2015-06-01 18:44:43','2015-06-01 18:44:43'),(560,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_777','2015-06-01 18:46:31','2015-06-01 18:46:31'),(561,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_735','2015-06-01 18:46:31','2015-06-01 18:46:31'),(562,'JKF_XGYX201506011932097964_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601193236_949','2015-06-01 19:32:36','2015-06-01 19:32:36'),(563,'JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602205609_540','2015-06-02 20:56:10','2015-06-02 20:56:10'),(564,'JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602210950_126','2015-06-02 21:09:50','2015-06-02 21:09:50');
/*!40000 ALTER TABLE `ims_eso_sale_business_no_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_cart`
--

DROP TABLE IF EXISTS `ims_eso_sale_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_cart` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `goodsid` int(11) NOT NULL,
  `goodstype` tinyint(1) NOT NULL DEFAULT '1',
  `from_user` varchar(50) NOT NULL,
  `total` int(10) unsigned NOT NULL,
  `optionid` int(10) DEFAULT '0',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `dlsgid` varchar(20) DEFAULT NULL,
  `lid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_openid` (`from_user`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_cart`
--

LOCK TABLES `ims_eso_sale_cart` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_cart` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_cart` VALUES (2,9,5,0,'111',1,0,0.00,'9','38');
/*!40000 ALTER TABLE `ims_eso_sale_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_category`
--

DROP TABLE IF EXISTS `ims_eso_sale_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属帐号',
  `commission` int(10) unsigned DEFAULT '0' COMMENT '推荐该类商品所能获得的佣金',
  `name` varchar(50) NOT NULL COMMENT '分类名称',
  `thumb` varchar(255) NOT NULL COMMENT '分类图片',
  `parentid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级分类ID,0为第一级',
  `isrecommand` int(10) DEFAULT '0',
  `description` varchar(500) NOT NULL COMMENT '分类介绍',
  `displayorder` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '是否开启',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1001 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_category`
--

LOCK TABLES `ims_eso_sale_category` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_category` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_category` VALUES (1,9,0,'食品、饮料','',0,0,'',0,1),(2,9,0,'酒','',0,0,'',0,1),(3,9,0,'烟草','',0,0,'',0,1),(4,9,0,'纺织品及其制成品','',0,0,'',0,1),(5,9,0,'皮革服装及配饰','',0,0,'',0,1),(6,9,0,'箱包及鞋靴','',0,0,'',0,1),(7,9,0,'表、钟及其配件、附件','',0,0,'',0,1),(8,9,0,'表、钟及其配件、附件','',0,0,'',0,1),(9,9,0,'金、银、珠宝及其制品、艺术品、收藏品','',0,0,'',0,1),(10,9,0,'化妆品','',0,0,'',0,1),(11,9,0,'家用医疗、保健及美容器材','',0,0,'',0,1),(12,9,0,'厨卫用具及小家电','',0,0,'',0,1),(13,9,0,'厨卫用具及小家电','',0,0,'',0,1),(14,9,0,'家具','',0,0,'',0,1),(15,9,0,'空调及其配件、附件','',0,0,'',0,1),(16,9,0,'电冰箱及其配件、附件','',0,0,'',0,1),(17,9,0,'洗衣设备及其配件、附件','',0,0,'',0,1),(18,9,0,'电视机及其配件、附件','',0,0,'',0,1),(19,9,0,'摄影（像）设备及其配件、附件','',0,0,'',0,1),(20,9,0,'摄影（像）设备及其配件、附件','',0,0,'',0,1),(21,9,0,'影音设备及其配件、附件','',0,0,'',0,1),(22,9,0,'计算机及其外围设备','',0,0,'',0,1),(23,9,0,'书报、刊物及其他各类印刷品','',0,0,'',0,1),(24,9,0,'教育专用的电影片、幻灯片、原版录音带、录像带','',0,0,'',0,1),(25,9,0,'文具用品及玩具','',0,0,'',0,1),(26,9,0,'邮票','',0,0,'',0,1),(27,9,0,'乐器','',0,0,'',0,1),(28,9,0,'体育用品','',0,0,'',0,1),(29,9,0,'体育用品','',0,0,'',0,1),(30,9,0,'自行车、三轮车、童车及其配件、附件','',0,0,'',0,1),(31,9,0,'其他物品','',0,0,'',0,1),(1000,9,0,'奶粉','',1,0,'',0,1);
/*!40000 ALTER TABLE `ims_eso_sale_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_commission`
--

DROP TABLE IF EXISTS `ims_eso_sale_commission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_commission` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `mid` int(10) unsigned NOT NULL COMMENT '粉丝ID',
  `ogid` int(10) unsigned DEFAULT NULL COMMENT '订单商品ID',
  `commission` decimal(10,2) unsigned NOT NULL COMMENT '佣金',
  `content` text,
  `flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为账户充值记录，1为提现记录',
  `isout` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0为未导出，1为已导出',
  `isshare` int(11) DEFAULT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_commission`
--

LOCK TABLES `ims_eso_sale_commission` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_commission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_commission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_country_code`
--

DROP TABLE IF EXISTS `ims_eso_sale_country_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_country_code` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=245 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_country_code`
--

LOCK TABLES `ims_eso_sale_country_code` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_country_code` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_country_code` VALUES (1,'100','亚洲'),(2,'101','阿富汗'),(3,'102','巴林'),(4,'103','孟加拉国'),(5,'104','不丹'),(6,'105','文莱'),(7,'106','缅甸'),(8,'107','柬埔寨'),(9,'108','塞浦路斯'),(10,'109','朝鲜'),(11,'110','中国香港'),(12,'111','印度'),(13,'112','印度尼西亚'),(14,'113','伊朗'),(15,'114','伊拉克'),(16,'115','以色列'),(17,'116','日本'),(18,'117','约旦'),(19,'118','科威特'),(20,'119','老挝'),(21,'120','黎巴嫩'),(22,'121','中国澳门'),(23,'122','马来西亚'),(24,'123','马尔代夫'),(25,'124','蒙古'),(26,'125','尼泊尔'),(27,'126','阿曼'),(28,'127','巴基斯坦'),(29,'128','巴勒斯坦'),(30,'129','菲律宾'),(31,'130','卡塔尔'),(32,'131','沙特阿拉伯'),(33,'132','新加坡'),(34,'133','韩国'),(35,'134','斯里兰卡'),(36,'135','叙利亚'),(37,'136','泰国'),(38,'137','土耳其'),(39,'138','阿联酋'),(40,'139','也门共和国'),(41,'141','越南'),(42,'142','中国'),(43,'143','台澎金马关税区'),(44,'144','东帝汶'),(45,'145','哈萨克斯坦'),(46,'146','吉尔吉斯斯坦'),(47,'147','塔吉克斯坦'),(48,'148','土库曼斯坦'),(49,'149','乌兹别克斯坦'),(50,'199','亚洲其他国家(地区)'),(51,'200','非洲'),(52,'201','阿尔及利亚'),(53,'202','安哥拉'),(54,'203','贝宁'),(55,'204','博茨瓦那'),(56,'205','布隆迪'),(57,'206','喀麦隆'),(58,'207','加那利群岛'),(59,'208','佛得角'),(60,'209','中非共和国'),(61,'210','塞卜泰'),(62,'211','乍得'),(63,'212','科摩罗'),(64,'213','刚果'),(65,'214','吉布提'),(66,'215','埃及'),(67,'216','赤道几内亚'),(68,'217','埃塞俄比亚'),(69,'218','加蓬'),(70,'219','冈比亚'),(71,'220','加纳'),(72,'221','几内亚'),(73,'222','几内亚(比绍)'),(74,'223','科特迪瓦'),(75,'224','肯尼亚'),(76,'225','利比里亚'),(77,'226','利比亚'),(78,'227','马达加斯加'),(79,'228','马拉维'),(80,'229','马里'),(81,'230','毛里塔尼亚'),(82,'231','毛里求斯'),(83,'232','摩洛哥'),(84,'233','莫桑比克'),(85,'234','纳米比亚'),(86,'235','尼日尔'),(87,'236','尼日利亚'),(88,'237','留尼汪'),(89,'238','卢旺达'),(90,'239','圣多美和普林西比'),(91,'240','塞内加尔'),(92,'241','塞舌尔'),(93,'242','塞拉利昂'),(94,'243','索马里'),(95,'244','南非'),(96,'245','西撒哈拉'),(97,'246','苏丹'),(98,'247','坦桑尼亚'),(99,'248','多哥'),(100,'249','突尼斯'),(101,'250','乌干达'),(102,'251','布基纳法索'),(103,'252','民主刚果'),(104,'253','赞比亚'),(105,'254','津巴布韦'),(106,'255','莱索托'),(107,'256','梅利利亚'),(108,'257','斯威士兰'),(109,'258','厄立特里亚'),(110,'259','马约特岛'),(111,'299','非洲其他国家(地区)'),(112,'300','欧洲'),(113,'301','比利时'),(114,'302','丹麦'),(115,'303','英国'),(116,'304','德国'),(117,'305','法国'),(118,'306','爱尔兰'),(119,'307','意大利'),(120,'308','卢森堡'),(121,'309','荷兰'),(122,'310','希腊'),(123,'311','葡萄牙'),(124,'312','西班牙'),(125,'313','阿尔巴尼亚'),(126,'314','安道尔'),(127,'315','奥地利'),(128,'316','保加利亚'),(129,'318','芬兰'),(130,'320','直布罗陀'),(131,'321','匈牙利'),(132,'322','冰岛'),(133,'323','列支敦士登'),(134,'324','马耳他'),(135,'325','摩纳哥'),(136,'326','挪威'),(137,'327','波兰'),(138,'328','罗马尼亚'),(139,'329','圣马力诺'),(140,'330','瑞典'),(141,'331','瑞士'),(142,'334','爱沙尼亚'),(143,'335','拉脱维亚'),(144,'336','立陶宛'),(145,'337','格鲁吉亚'),(146,'338','亚美尼亚'),(147,'339','阿塞拜疆'),(148,'340','白俄罗斯'),(149,'343','摩尔多瓦'),(150,'344','俄罗斯联邦'),(151,'347','乌克兰'),(152,'349','塞尔维亚和黑山'),(153,'350','斯洛文尼亚'),(154,'351','克罗地亚'),(155,'352','捷克共和国'),(156,'353','斯洛伐克'),(157,'354','马其顿'),(158,'355','波斯尼亚-黑塞哥维那共和'),(159,'356','梵蒂冈城国'),(160,'399','欧洲其他国家(地区)'),(161,'400','拉丁美洲'),(162,'401','安提瓜和巴布达'),(163,'402','阿根廷'),(164,'403','阿鲁巴岛'),(165,'404','巴哈马'),(166,'405','巴巴多斯'),(167,'406','伯利兹'),(168,'408','玻利维亚'),(169,'409','博内尔'),(170,'410','巴西'),(171,'411','开曼群岛'),(172,'412','智利'),(173,'413','哥伦比亚'),(174,'414','多米尼亚共和国'),(175,'415','哥斯达黎加'),(176,'416','古巴'),(177,'417','库腊索岛'),(178,'418','多米尼加共和国'),(179,'419','厄瓜多尔'),(180,'420','法属圭亚那'),(181,'421','格林纳达'),(182,'422','瓜德罗普'),(183,'423','危地马拉'),(184,'424','圭亚那'),(185,'425','海地'),(186,'426','洪都拉斯'),(187,'427','牙买加'),(188,'428','马提尼克'),(189,'429','墨西哥'),(190,'430','蒙特塞拉特'),(191,'431','尼加拉瓜'),(192,'432','巴拿马'),(193,'433','巴拉圭'),(194,'434','秘鲁'),(195,'435','波多黎各'),(196,'436','萨巴'),(197,'437','圣卢西亚'),(198,'438','圣马丁岛'),(199,'439','圣文森特和格林纳丁斯'),(200,'440','萨尔瓦多'),(201,'441','苏里南'),(202,'442','特立尼达和多巴哥'),(203,'443','特克斯和凯科斯群岛'),(204,'444','乌拉圭'),(205,'445','委内瑞拉'),(206,'446','英属维尔京群岛'),(207,'447','圣其茨-尼维斯'),(208,'448','圣皮埃尔和密克隆'),(209,'449','荷属安地列斯群岛'),(210,'499','拉丁美洲其他国家(地区)'),(211,'500','北美洲'),(212,'501','加拿大'),(213,'502','美国'),(214,'503','格陵兰'),(215,'504','百慕大'),(216,'599','北美洲其他国家(地区)'),(217,'600','大洋洲'),(218,'601','澳大利亚'),(219,'602','库克群岛'),(220,'603','斐济'),(221,'604','盖比群岛'),(222,'605','马克萨斯群岛'),(223,'606','瑙鲁'),(224,'607','新喀里多尼亚'),(225,'608','瓦努阿图'),(226,'609','新西兰'),(227,'610','诺福克岛'),(228,'611','巴布亚新几内亚'),(229,'612','社会群岛'),(230,'613','所罗门群岛'),(231,'614','汤加'),(232,'615','土阿莫土群岛'),(233,'616','土布艾群岛'),(234,'617','萨摩亚'),(235,'618','基里巴斯'),(236,'619','图瓦卢'),(237,'620','密克罗尼西亚联邦'),(238,'621','马绍尔群岛'),(239,'622','帕劳共和国'),(240,'623','法属波利尼西亚'),(241,'625','瓦利斯和浮图纳'),(242,'699','大洋洲其他国家(地区)'),(243,'701','国(地)别不详的'),(244,'702','联合国及机构和国际组织');
/*!40000 ALTER TABLE `ims_eso_sale_country_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_credit_award`
--

DROP TABLE IF EXISTS `ims_eso_sale_credit_award`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_credit_award` (
  `award_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `deadline` datetime NOT NULL,
  `credit_cost` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '100',
  `content` text NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`award_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_credit_award`
--

LOCK TABLES `ims_eso_sale_credit_award` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_credit_award` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_credit_award` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_credit_request`
--

DROP TABLE IF EXISTS `ims_eso_sale_credit_request`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_credit_request` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `award_id` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_credit_request`
--

LOCK TABLES `ims_eso_sale_credit_request` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_credit_request` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_credit_request` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_currency`
--

DROP TABLE IF EXISTS `ims_eso_sale_currency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_currency` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_currency`
--

LOCK TABLES `ims_eso_sale_currency` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_currency` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_currency` VALUES (1,'110','港元'),(2,'113','伊朗里亚尔'),(3,'116','日本元'),(4,'118','科威特第纳尔'),(5,'121','澳门元'),(6,'122','马来西亚林吉特'),(7,'127','巴基斯坦卢比'),(8,'129','菲律宾比索'),(9,'132','新加坡元'),(10,'136','泰国铢'),(11,'142','人民币'),(12,'143','台湾元'),(13,'300','欧元'),(14,'302','丹麦克朗'),(15,'303','英镑'),(16,'326','挪威克朗'),(17,'330','瑞典克朗'),(18,'331','瑞士法郎'),(19,'398','记帐瑞士法郎'),(20,'501','加拿大元'),(21,'502','美元'),(22,'601','澳大利亚元'),(23,'609','新西兰元');
/*!40000 ALTER TABLE `ims_eso_sale_currency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_dispatch`
--

DROP TABLE IF EXISTS `ims_eso_sale_dispatch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_dispatch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `dispatchname` varchar(50) DEFAULT '',
  `dispatchtype` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  `firstprice` decimal(10,2) DEFAULT '0.00',
  `secondprice` decimal(10,2) DEFAULT '0.00',
  `firstweight` int(11) DEFAULT '0',
  `secondweight` int(11) DEFAULT '0',
  `express` int(11) DEFAULT '0',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_dispatch`
--

LOCK TABLES `ims_eso_sale_dispatch` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_dispatch` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_dispatch` VALUES (1,9,'中国外运股份有限公司（EMS）快递（1-2工作日到获）',0,0,0.00,0.00,0,0,0,NULL);
/*!40000 ALTER TABLE `ims_eso_sale_dispatch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_express`
--

DROP TABLE IF EXISTS `ims_eso_sale_express`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `express_name` varchar(50) DEFAULT '',
  `displayorder` int(11) DEFAULT '0',
  `express_price` varchar(10) DEFAULT '',
  `express_area` varchar(100) DEFAULT '',
  `express_url` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_express`
--

LOCK TABLES `ims_eso_sale_express` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_express` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_express` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_feedback`
--

DROP TABLE IF EXISTS `ims_eso_sale_feedback`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_feedback` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1为维权，2为告擎',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态0未解决，1用户同意，2用户拒绝',
  `feedbackid` varchar(30) NOT NULL COMMENT '投诉单号',
  `transid` varchar(30) NOT NULL COMMENT '订单号',
  `reason` varchar(1000) NOT NULL COMMENT '理由',
  `solution` varchar(1000) NOT NULL COMMENT '期待解决方案',
  `remark` varchar(1000) NOT NULL COMMENT '备注',
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_uniacid` (`uniacid`),
  KEY `idx_feedbackid` (`feedbackid`),
  KEY `idx_createtime` (`createtime`),
  KEY `idx_transid` (`transid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_feedback`
--

LOCK TABLES `ims_eso_sale_feedback` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_feedback` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_feedback` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `pcate` int(10) unsigned NOT NULL DEFAULT '0',
  `ccate` int(10) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1为实体，2为虚拟',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `displayorder` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(100) NOT NULL DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `xsthumb` varchar(255) DEFAULT '',
  `unit` varchar(5) NOT NULL DEFAULT '',
  `description` varchar(1000) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `goodssn` varchar(50) NOT NULL DEFAULT '',
  `productsn` varchar(50) NOT NULL DEFAULT '',
  `marketprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `productprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `costprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` int(10) NOT NULL DEFAULT '0',
  `totalcnf` int(11) DEFAULT '0' COMMENT '0 拍下减库存 1 付款减库存 2 永久不减',
  `sales` int(10) unsigned NOT NULL DEFAULT '0',
  `spec` varchar(5000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `weight` decimal(10,2) NOT NULL DEFAULT '0.00',
  `credit` int(11) DEFAULT '0',
  `maxbuy` int(11) DEFAULT '0',
  `hasoption` int(11) DEFAULT '0',
  `dispatch` int(11) DEFAULT '0',
  `thumb_url` text,
  `isnew` int(11) DEFAULT '0',
  `ishot` int(11) DEFAULT '0',
  `isdiscount` int(11) DEFAULT '0',
  `isrecommand` int(11) DEFAULT '0',
  `istime` int(11) DEFAULT '0',
  `timestart` int(11) DEFAULT '0',
  `timeend` int(11) DEFAULT '0',
  `viewcount` int(11) DEFAULT '0',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `commission2` int(3) DEFAULT NULL,
  `commission3` int(3) DEFAULT NULL,
  `commission` int(3) NOT NULL,
  `bz` varchar(20) DEFAULT NULL,
  `shuilv` varchar(20) DEFAULT NULL,
  `dw` varchar(20) DEFAULT NULL,
  `guojia` varchar(200) DEFAULT NULL,
  `ieFlag` varchar(20) DEFAULT NULL,
  `unitPrice` varchar(20) DEFAULT NULL,
  `pic` text,
  `parcelTaxCode` varchar(255) DEFAULT NULL,
  `goodItemNo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods`
--

LOCK TABLES `ims_eso_sale_goods` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_goods` VALUES (1,9,1,1000,0,1,0,'奥地利爱他美pre段800G','/app/kd/attached/image/20150602/20150602120452_76613.jpeg','','','奥地利爱他美pre段800G','&lt;table border=&quot;0&quot; cellpadding=&quot;0&quot; cellspacing=&quot;0&quot; width=&quot;183&quot; style=&quot;width:137pt;&quot; class=&quot;ke-zeroborder&quot;&gt;\r\n	&lt;tbody&gt;\r\n		&lt;tr&gt;\r\n			&lt;td height=&quot;45&quot; class=&quot;xl65&quot; width=&quot;183&quot;&gt;\r\n				奥地利爱他美pre段800G\r\n			&lt;/td&gt;\r\n		&lt;/tr&gt;\r\n	&lt;/tbody&gt;\r\n&lt;/table&gt;','XGYX201506020001','',0.00,0.00,0.00,2000,1,0,'',1433217946,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,1,0,0,0,'0','1','96','127',NULL,'90',NULL,'01010700','0085-00001'),(2,9,1,1000,0,1,0,'奥地利爱他美pre段800G','/app/kd/attached/image/20150602/20150602124012_63018.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602001','',0.00,0.00,0.00,6,1,0,'',1433220487,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','127',NULL,'200',NULL,'01010700','0085-00001'),(3,9,1,1000,0,1,0,'奥地利爱他美1段800G','/app/kd/attached/image/20150602/20150602125552_56228.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602002','',0.00,0.00,0.00,4,1,2,'',1433221151,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','127',NULL,'200',NULL,'01010700','0085-00002'),(4,9,1,1000,0,1,0,'奥地利爱他美2段800G','/app/kd/attached/image/20150602/20150602130140_16347.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602003','',0.00,0.00,0.00,28,1,0,'',1433221425,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','127',NULL,'200',NULL,'01010700','0085-00003'),(5,9,1,1000,0,1,0,'奥地利爱他美3段800G','/app/kd/attached/image/20150602/20150602130532_37800.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602004','',0.00,0.00,0.00,26,1,4,'',1433221606,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','127',NULL,'200',NULL,'01010700','0085-00004'),(6,9,1,1000,0,1,0,'奥地利爱他美1+段800G','/app/kd/attached/image/20150602/20150602130755_74379.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602005','',0.00,0.00,0.00,116,1,0,'',1433221757,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'0','1','96','127',NULL,'200',NULL,'01010700','0085-00005'),(7,9,1,1000,0,1,0,'奥地利爱他美2+段800G','/app/kd/attached/image/20150602/20150602131018_84054.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区','','XGYX20150602006','',0.00,0.00,0.00,60,1,0,'',1433221921,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','0',NULL,'200',NULL,'01010700','0085-00006'),(8,9,1,1000,0,1,0,'奥地利爱他美3+段800G','/app/kd/attached/image/20150602/20150602131253_43084.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602007','',0.00,0.00,0.00,18,1,0,'',1433222058,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','127',NULL,'200',NULL,'01010700','0085-00007'),(9,9,1,1000,0,1,0,'德国爱他美2段800G','/app/kd/attached/image/20150602/20150602131530_82166.jpg','','','商品发自中（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602008','',0.00,0.00,0.00,78,1,0,'',1433222236,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','116',NULL,'200',NULL,'01010700','0085-00012'),(10,9,1,1000,0,1,0,'德国爱他美3段800G','/app/kd/attached/image/20150602/20150602131802_32358.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGXY20150602009','',0.00,0.00,0.00,72,1,0,'',1433222431,800.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','96','116',NULL,'200',NULL,'01010700','0085-00013'),(11,9,1,1000,0,1,0,'德国爱他美1+段600G','/app/kd/attached/image/20150602/20150602132112_77971.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区','','XGYX20150602010','',0.00,0.00,0.00,285,1,0,'',1433222551,600.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','112','116',NULL,'200',NULL,'01010700','0085-00008'),(12,9,1,1000,0,1,0,'德国爱他美2+段600G','/app/kd/attached/image/20150602/20150602132304_92439.jpg','','','商品发自中国（杭州）跨境电子商务综合试验区Z-MARK保税仓库','','XGYX20150602011','',0.00,0.00,0.00,100,1,0,'',1433222640,600.00,0,0,0,0,'a:0:{}',0,0,0,0,0,0,0,0,0,0,0,0,'3','1','112','116',NULL,'200',NULL,'01010700','0085-00009');
/*!40000 ALTER TABLE `ims_eso_sale_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_bz`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_bz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_bz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_bz`
--

LOCK TABLES `ims_eso_sale_goods_bz` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_bz` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_goods_bz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_dls`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_dls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_dls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `mid` int(11) DEFAULT NULL,
  `dl` int(11) DEFAULT '0',
  `addtime` varchar(255) DEFAULT '',
  `dlj` varchar(50) DEFAULT NULL,
  `fgsdj` varchar(50) DEFAULT NULL,
  `lsj` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_dls`
--

LOCK TABLES `ims_eso_sale_goods_dls` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_dls` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_goods_dls` VALUES (2,9,12,4,2,'1433222783',NULL,NULL,NULL),(3,9,11,4,2,'1433222783',NULL,NULL,NULL),(4,9,10,4,2,'1433222783',NULL,NULL,NULL),(5,9,9,4,2,'1433222783',NULL,NULL,NULL),(6,9,8,4,2,'1433222783',NULL,NULL,NULL),(7,9,7,4,2,'1433222783',NULL,NULL,NULL),(8,9,6,4,2,'1433222783',NULL,NULL,NULL),(9,9,5,4,2,'1433222783',NULL,NULL,NULL),(10,9,4,4,2,'1433222783',NULL,NULL,NULL),(11,9,3,4,2,'1433222783',NULL,NULL,NULL),(12,9,2,4,2,'1433222783',NULL,NULL,NULL),(24,9,3,5,2,'1433249282',NULL,NULL,NULL);
/*!40000 ALTER TABLE `ims_eso_sale_goods_dls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_dls_lsj`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_dls_lsj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_dls_lsj` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bid` varchar(250) DEFAULT NULL,
  `fgsj` varchar(250) DEFAULT NULL,
  `jxsj` varchar(250) DEFAULT NULL,
  `lsj` varchar(250) DEFAULT NULL,
  `fz` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_dls_lsj`
--

LOCK TABLES `ims_eso_sale_goods_dls_lsj` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_dls_lsj` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_goods_dls_lsj` VALUES (17,'2','150','150',NULL,'23'),(18,'2','450','450',NULL,'24'),(19,'3','150','150',NULL,'21'),(20,'3','450','450',NULL,'22'),(21,'4','185','185',NULL,'19'),(22,'4','350','350',NULL,'20'),(23,'5','180','180',NULL,'17'),(24,'5','350','350',NULL,'18'),(25,'6','185','185',NULL,'15'),(26,'6','175','175',NULL,'16'),(27,'7','185','185',NULL,'13'),(28,'7','175','175',NULL,'14'),(29,'8','185','185',NULL,'11'),(30,'8','175','175',NULL,'12'),(31,'12','200','200','200','3'),(32,'12','380','380','380','4'),(33,'11','190','190',NULL,'5'),(34,'11','360','360',NULL,'6'),(35,'10','190','190',NULL,'7'),(36,'10','360','360',NULL,'8'),(37,'9','190','190','190','9'),(38,'9','360','360','380','10'),(39,'24','190','210','210','5'),(40,'24','360','380','380','6');
/*!40000 ALTER TABLE `ims_eso_sale_goods_dls_lsj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_dw`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_dw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_dw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_dw`
--

LOCK TABLES `ims_eso_sale_goods_dw` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_dw` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_goods_dw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_fz`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_fz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_fz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gid` varchar(250) DEFAULT NULL,
  `n` varchar(250) DEFAULT NULL,
  `je` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_fz`
--

LOCK TABLES `ims_eso_sale_goods_fz` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_fz` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_goods_fz` VALUES (1,'1','1','90'),(2,'1','2','160'),(3,'2','1','200'),(4,'2','2','380'),(5,'3','1','190'),(6,'3','2','360'),(7,'4','1','190'),(8,'4','2','360'),(9,'5','1','190'),(10,'5','2','360'),(11,'6','1','185'),(12,'6','2','350'),(13,'7','1','185'),(14,'7','2','350'),(15,'8','1','185'),(16,'8','2','350'),(17,'9','1','180'),(18,'9','2','350'),(19,'10','1','185'),(20,'10','2','350'),(21,'11','1','165'),(22,'11','3','450'),(23,'12','1','165'),(24,'12','3','450');
/*!40000 ALTER TABLE `ims_eso_sale_goods_fz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_option`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_option`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_option` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(10) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `thumb` varchar(60) DEFAULT '',
  `productprice` decimal(10,2) DEFAULT '0.00',
  `marketprice` decimal(10,2) DEFAULT '0.00',
  `costprice` decimal(10,2) DEFAULT '0.00',
  `stock` int(11) DEFAULT '0',
  `weight` decimal(10,2) DEFAULT '0.00',
  `displayorder` int(11) DEFAULT '0',
  `specs` text,
  PRIMARY KEY (`id`),
  KEY `indx_goodsid` (`goodsid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_option`
--

LOCK TABLES `ims_eso_sale_goods_option` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_option` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_goods_option` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_goods_param`
--

DROP TABLE IF EXISTS `ims_eso_sale_goods_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_goods_param` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goodsid` int(10) DEFAULT '0',
  `title` varchar(50) DEFAULT '',
  `value` text,
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_goodsid` (`goodsid`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_goods_param`
--

LOCK TABLES `ims_eso_sale_goods_param` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_goods_param` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_goods_param` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_member`
--

DROP TABLE IF EXISTS `ims_eso_sale_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `shareid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `from_user` varchar(50) NOT NULL,
  `realname` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `pwd` varchar(255) NOT NULL,
  `bankcard` varchar(20) DEFAULT NULL,
  `banktype` varchar(20) DEFAULT NULL,
  `alipay` varchar(100) DEFAULT NULL,
  `wxhao` varchar(100) DEFAULT NULL,
  `commission` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '已结佣佣金',
  `zhifu` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '已打款佣金',
  `content` text,
  `createtime` int(10) NOT NULL,
  `flagtime` int(10) DEFAULT NULL COMMENT '为成推广人的时间',
  `status` tinyint(1) DEFAULT '1' COMMENT '0为禁用，1为可用',
  `flag` tinyint(1) DEFAULT '0' COMMENT '0为会推广人，1为推广人',
  `clickcount` int(11) NOT NULL DEFAULT '0' COMMENT '点击次数',
  `vip` varchar(2) DEFAULT '0',
  `username` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `oid` varchar(11) DEFAULT NULL,
  `px` varchar(20) DEFAULT NULL,
  `nid` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_member`
--

LOCK TABLES `ims_eso_sale_member` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_member` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_member` VALUES (1,0,NULL,NULL,'','','','202cb962ac59075b964b07152d234b70',NULL,NULL,NULL,NULL,0.00,0.00,NULL,0,NULL,1,0,0,'1','gs','总公司',NULL,NULL,'1',NULL),(2,9,NULL,NULL,'','李罗琦','18621816275','202cb962ac59075b964b07152d234b70','',NULL,'llqbaimao@live.com',NULL,0.00,0.00,'',1433218002,NULL,1,0,0,'2','hfgs','合肥分公司','合肥市蜀山区肥西路汇金大厦 ','1','','1'),(5,9,NULL,NULL,'','李罗琦','18621816275','202cb962ac59075b964b07152d234b70','',NULL,'llqbaimao@gmail.com',NULL,0.00,0.00,'',1433249231,NULL,1,1,0,'3','baimao','合肥经销商','合肥市蜀山区肥西路汇金大厦 ','2','','3'),(4,9,NULL,NULL,'','程祺','13916933369','a201bad585055d1e1f22db5e34617ec8','',NULL,'358136290@qq.com',NULL,0.00,0.00,'',1433222748,NULL,1,1,0,'3','chengqi86','程筠曦','上海市虹口区株洲路399弄','1','','2');
/*!40000 ALTER TABLE `ims_eso_sale_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_mid`
--

DROP TABLE IF EXISTS `ims_eso_sale_mid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_mid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `num` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_mid`
--

LOCK TABLES `ims_eso_sale_mid` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_mid` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_mid` VALUES (1,'vip1','1'),(2,'vip3','1'),(3,'vip3','3');
/*!40000 ALTER TABLE `ims_eso_sale_mid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_order`
--

DROP TABLE IF EXISTS `ims_eso_sale_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `shareid` int(10) unsigned DEFAULT '0' COMMENT '推荐人ID',
  `ordersn` varchar(255) NOT NULL,
  `price` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '-1取消状态，0普通状态，1为已付款，2为已发货，3为成功',
  `sendtype` tinyint(1) unsigned NOT NULL COMMENT '1为快递，2为自提',
  `paytype` tinyint(1) unsigned NOT NULL COMMENT '1为余额，2为在线，3为到付',
  `transid` varchar(30) NOT NULL DEFAULT '0' COMMENT '微信支付单号',
  `goodstype` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `remark` varchar(1000) NOT NULL DEFAULT '',
  `addressid` int(10) unsigned NOT NULL,
  `expresscom` varchar(30) NOT NULL DEFAULT '',
  `expresssn` varchar(50) NOT NULL DEFAULT '',
  `express` varchar(200) NOT NULL DEFAULT '',
  `goodsprice` decimal(10,2) DEFAULT '0.00',
  `dispatchprice` decimal(10,2) DEFAULT '0.00',
  `dispatch` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `yj` varchar(50) DEFAULT NULL,
  `xys` varchar(50) DEFAULT NULL,
  `jyh` varchar(255) DEFAULT NULL,
  `ddh` varchar(255) DEFAULT NULL,
  `logisBillNo` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_order`
--

LOCK TABLES `ims_eso_sale_order` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_order` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_order` VALUES (1,9,111,'111',0,'XGYX201506021452072748','380',0,0,2,'0',0,'',1,'','','',380.00,0.00,1,1433227927,'380','0',NULL,NULL,NULL),(2,9,112,'112',0,'XGYX201506021513163339','380',1,0,2,'0',0,'',2,'','','',380.00,0.00,1,1433229196,'380','0','2015060200001000090052756941','XGYX201506021513163339',NULL),(3,9,113,'113',0,'XGYX201506022053594048','380',2,0,2,'0',0,'',3,'','','',380.00,0.00,1,1433249639,'380','0','2015060200001000690054718201','XGYX201506022053594048','1103122468305');
/*!40000 ALTER TABLE `ims_eso_sale_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_order_goods`
--

DROP TABLE IF EXISTS `ims_eso_sale_order_goods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_order_goods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `orderid` int(10) unsigned NOT NULL,
  `goodsid` int(10) unsigned NOT NULL,
  `commission` decimal(10,2) unsigned DEFAULT '0.00' COMMENT '该订单的推荐佣金',
  `commission2` decimal(10,2) unsigned DEFAULT '0.00',
  `commission3` decimal(10,2) unsigned DEFAULT '0.00',
  `applytime` int(10) unsigned DEFAULT NULL COMMENT '申请时间',
  `checktime` int(10) unsigned DEFAULT NULL COMMENT '审核时间',
  `status` tinyint(3) DEFAULT '0' COMMENT '申请状态，-2为标志删除，-1为审核无效，0为未申请，1为正在申请，2为审核通过',
  `content` text,
  `price` decimal(10,2) DEFAULT '0.00',
  `total` int(10) unsigned NOT NULL DEFAULT '1',
  `optionid` int(10) DEFAULT '0',
  `createtime` int(10) unsigned NOT NULL,
  `optionname` text,
  `dlsgid` varchar(20) DEFAULT NULL,
  `mid` varchar(20) DEFAULT NULL,
  `js` varchar(20) DEFAULT NULL,
  `js2` varchar(20) DEFAULT NULL,
  `jstime` varchar(20) DEFAULT NULL,
  `js2time` varchar(20) DEFAULT NULL,
  `lid` varchar(20) DEFAULT NULL,
  `gn` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_order_goods`
--

LOCK TABLES `ims_eso_sale_order_goods` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_order_goods` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_order_goods` VALUES (1,9,1,5,0.00,0.00,0.00,NULL,NULL,0,NULL,0.00,1,0,1433227927,NULL,'9','4',NULL,NULL,NULL,NULL,'38','2'),(2,9,2,5,0.00,0.00,0.00,NULL,NULL,0,NULL,0.00,1,0,1433229196,NULL,'9','4',NULL,NULL,NULL,NULL,'38','2'),(3,9,3,3,0.00,0.00,0.00,NULL,NULL,0,NULL,190.00,1,0,1433249639,NULL,'24','5',NULL,NULL,NULL,NULL,'40','2');
/*!40000 ALTER TABLE `ims_eso_sale_order_goods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_order_pz`
--

DROP TABLE IF EXISTS `ims_eso_sale_order_pz`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_order_pz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyName` varchar(250) DEFAULT NULL,
  `companyCode` varchar(250) DEFAULT NULL,
  `payCompanyCode` varchar(250) DEFAULT NULL,
  `eCommerceCode` varchar(250) DEFAULT NULL,
  `eCommerceName` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_order_pz`
--

LOCK TABLES `ims_eso_sale_order_pz` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_order_pz` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_order_pz` VALUES (1,'香港筠希电子商务科技有限公司','PT15021201','ZF14021901','DS15051101','香港筠希电子商务科技有限公司');
/*!40000 ALTER TABLE `ims_eso_sale_order_pz` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_order_sent_history`
--

DROP TABLE IF EXISTS `ims_eso_sale_order_sent_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_order_sent_history` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `orderNo` varchar(64) NOT NULL,
  `businessNo` varchar(256) NOT NULL,
  `createTime` varchar(64) DEFAULT NULL,
  `updateTime` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=600 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_order_sent_history`
--

LOCK TABLES `ims_eso_sale_order_sent_history` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_order_sent_history` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_order_sent_history` VALUES (589,'XGYX201506011453328457','JKF_XGYX201506011453328457_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601145352_649','2015-06-01 14:53:52','2015-06-01 14:53:52'),(590,'XGYX201506011650382192','JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601165100_773','2015-06-01 16:51:00','2015-06-01 16:51:00'),(591,'XGYX201506011650382192','JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601171339_740','2015-06-01 17:13:40','2015-06-01 17:13:40'),(592,'XGYX201506011712224486','JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601175227_951','2015-06-01 17:52:28','2015-06-01 17:52:28'),(593,'XGYX201506011843265524','JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_799','2015-06-01 18:44:43','2015-06-01 18:44:43'),(594,'XGYX201506011843265524','JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_249','2015-06-01 18:44:43','2015-06-01 18:44:43'),(595,'XGYX201506011843265524','JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_777','2015-06-01 18:46:31','2015-06-01 18:46:31'),(596,'XGYX201506011843265524','JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_735','2015-06-01 18:46:31','2015-06-01 18:46:31'),(597,'XGYX201506011932097964','JKF_XGYX201506011932097964_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601193236_949','2015-06-01 19:32:36','2015-06-01 19:32:36'),(598,'XGYX201506022053594048','JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602205609_540','2015-06-02 20:56:10','2015-06-02 20:56:10'),(599,'XGYX201506022053594048','JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602210950_126','2015-06-02 21:09:50','2015-06-02 21:09:50');
/*!40000 ALTER TABLE `ims_eso_sale_order_sent_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_order_wl`
--

DROP TABLE IF EXISTS `ims_eso_sale_order_wl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_order_wl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gb` varchar(250) DEFAULT NULL,
  `name` varchar(250) DEFAULT NULL,
  `logisCompanyName` varchar(250) DEFAULT NULL,
  `logisCompanyCode` varchar(250) DEFAULT NULL,
  `bonded` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_order_wl`
--

LOCK TABLES `ims_eso_sale_order_wl` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_order_wl` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_order_wl` VALUES (1,'中国','香港筠希电子商务科技有限公司','杭州中外运电子商务有限公司','3301510001','1');
/*!40000 ALTER TABLE `ims_eso_sale_order_wl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_pay`
--

DROP TABLE IF EXISTS `ims_eso_sale_pay`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `num` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_pay`
--

LOCK TABLES `ims_eso_sale_pay` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_pay` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_pay` VALUES (1,'1000');
/*!40000 ALTER TABLE `ims_eso_sale_pay` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_product`
--

DROP TABLE IF EXISTS `ims_eso_sale_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_product` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `goodsid` int(11) NOT NULL,
  `productsn` varchar(50) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `marketprice` decimal(10,0) unsigned NOT NULL,
  `productprice` decimal(10,0) unsigned NOT NULL,
  `total` int(11) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `spec` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_goodsid` (`goodsid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_product`
--

LOCK TABLES `ims_eso_sale_product` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_rule`
--

DROP TABLE IF EXISTS `ims_eso_sale_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT '',
  `rule` text,
  `terms` text,
  `createtime` int(10) NOT NULL,
  `gzurl` varchar(255) NOT NULL,
  `teamfy` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_rule`
--

LOCK TABLES `ims_eso_sale_rule` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_rules`
--

DROP TABLE IF EXISTS `ims_eso_sale_rules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_rules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `rule` text,
  `terms` text,
  `createtime` int(10) NOT NULL,
  `commtime` int(5) NOT NULL DEFAULT '15' COMMENT '默认15天',
  `promotertimes` int(10) NOT NULL DEFAULT '1' COMMENT '默认成交一次才能成为推广员',
  `ischeck` tinyint(1) DEFAULT '1' COMMENT '0为未审核，1为审核',
  `clickcredit` int(10) NOT NULL DEFAULT '0' COMMENT '点击获取积分',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_rules`
--

LOCK TABLES `ims_eso_sale_rules` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_rules` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_rules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_share_history`
--

DROP TABLE IF EXISTS `ims_eso_sale_share_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_share_history` (
  `sharemid` int(11) DEFAULT NULL,
  `uniacid` int(11) DEFAULT NULL,
  `from_user` varchar(50) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_share_history`
--

LOCK TABLES `ims_eso_sale_share_history` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_share_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_share_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_sino_result_record`
--

DROP TABLE IF EXISTS `ims_eso_sale_sino_result_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_sino_result_record` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `business_no` varchar(1024) DEFAULT NULL,
  `chk_mark` varchar(32) DEFAULT NULL,
  `notice_time` varchar(128) DEFAULT NULL,
  `notice_content` varchar(1024) DEFAULT NULL,
  `business_type` varchar(128) DEFAULT NULL,
  `way_bills` varchar(256) DEFAULT NULL,
  `create_time` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_sino_result_record`
--

LOCK TABLES `ims_eso_sale_sino_result_record` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_sino_result_record` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_sino_result_record` VALUES (51,'JKF_XGYX201506011453328457_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601145352_649','1','2015-06-01 14:53:52','处理成功','ORDER','1103120543205','2015-06-01 14:53:53'),(52,'JKF_XGYX201506011453328457_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601145352_649','1','2015-06-01 14:53:00','处理成功;','IMPORTORDER','','2015-06-01 14:54:40'),(53,'JKF_XGYX201506011453328457_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601145352_649','2','2015-06-01 14:55:00','22003:行邮税号 1901101000 未备案!;','IMPORTORDER','','2015-06-01 14:54:40'),(54,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601165100_773','1','2015-06-01 16:51:00','处理成功','ORDER','1103120544605','2015-06-01 16:51:00'),(55,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601165100_773','1','2015-06-01 16:52:00','处理成功;','IMPORTORDER','','2015-06-01 16:51:43'),(56,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601171339_740','1','2015-06-01 17:13:40','处理成功','ORDER','1103120545005','2015-06-01 17:13:40'),(57,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601171339_740','1','2015-06-01 17:12:00','处理成功;','IMPORTORDER','','2015-06-01 17:14:04'),(58,'JKF_XGYX201506011650382192_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601171339_740','2','2015-06-01 17:12:00','22007:订单号 (orderNo) XGYX201506011650382192 不允许重复申报;','IMPORTORDER','','2015-06-01 17:14:04'),(59,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601175227_951','1','2015-06-01 17:52:28','处理成功','ORDER','1103120546305','2015-06-01 17:52:28'),(60,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601175227_951','1','2015-06-01 17:53:00','处理成功;','IMPORTORDER','','2015-06-01 17:53:06'),(61,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_799','1','2015-06-01 18:44:43','处理成功','ORDER','1103120547705','2015-06-01 18:44:43'),(62,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_249','1','2015-06-01 18:44:43','处理成功','ORDER','1103120548505','2015-06-01 18:44:43'),(63,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_799','1','2015-06-01 18:46:00','处理成功;','IMPORTORDER','','2015-06-01 18:45:26'),(64,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184443_249','1','2015-06-01 18:46:00','处理成功;','IMPORTORDER','','2015-06-01 18:45:26'),(65,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_777','1','2015-06-01 18:46:31','处理成功','ORDER','1103120549405','2015-06-01 18:46:31'),(66,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_735','1','2015-06-01 18:46:31','处理成功','ORDER','1103120550305','2015-06-01 18:46:31'),(67,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_777','1','2015-06-01 18:46:00','处理成功;','IMPORTORDER','','2015-06-01 18:47:26'),(68,'JKF_XGYX201506011843265524_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601184631_735','1','2015-06-01 18:46:00','处理成功;','IMPORTORDER','','2015-06-01 18:47:27'),(69,'JKF_XGYX201506011932097964_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601193236_949','1','2015-06-01 19:32:36','处理成功','ORDER','1103120551705','2015-06-01 19:32:36'),(70,'JKF_XGYX201506011932097964_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150601193236_949','1','2015-06-01 19:33:00','处理成功;','IMPORTORDER','','2015-06-01 19:33:08'),(71,'JKF_1_IMPORTORDER_1_20150602155941_065','1','2015-06-02 15:59:00','处理成功;',NULL,'','2015-06-02 16:00:21'),(72,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602171037_473','1','2015-06-02 17:09:00','处理成功;','IMPORTORDER','','2015-06-02 17:10:43'),(73,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602171050_433','1','2015-06-02 17:12:00','处理成功;','IMPORTORDER','','2015-06-02 17:11:23'),(74,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602171357_655','1','2015-06-02 17:14:00','处理成功;','IMPORTORDER','','2015-06-02 17:14:43'),(75,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602171529_522','1','2015-06-02 17:15:00','处理成功;','IMPORTORDER','','2015-06-02 17:16:43'),(76,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602171732_243','1','2015-06-02 17:18:00','处理成功;','IMPORTORDER','','2015-06-02 17:17:43'),(77,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602172020_252','1','2015-06-02 17:24:00','处理成功;','IMPORTORDER','','2015-06-02 17:25:23'),(78,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602173449_753','1','2015-06-02 17:34:00','处理成功;','IMPORTORDER','','2015-06-02 17:35:24'),(79,'JKF_XGYX201506011712224486_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602174437_021','1','2015-06-02 17:43:00','处理成功;','IMPORTORDER','','2015-06-02 17:45:04'),(80,'JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602205609_540','2','2015-06-02 20:56:10','处理失败:企业备案名称(companyName)不能为空企业备案编号(companyCode)不能为空支付公司编码(payCompanyCode)不能为空电商企业编码(eCommerceCode)不能为空电商企业名称(eCommerceName)不能为空发件人姓名(senderName)不能为空','ORDER','','2015-06-02 20:56:10'),(81,'JKF_XGYX201506022053594048_9bccec6c-1ccc-41ed-bb82-bc67ebda0079_20150602210950_126','1','2015-06-02 21:09:00','处理成功;','IMPORTORDER','','2015-06-02 21:10:30');
/*!40000 ALTER TABLE `ims_eso_sale_sino_result_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_spec`
--

DROP TABLE IF EXISTS `ims_eso_sale_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_spec` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `displaytype` tinyint(3) unsigned NOT NULL,
  `content` text NOT NULL,
  `goodsid` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_spec`
--

LOCK TABLES `ims_eso_sale_spec` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_spec` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_spec` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_spec_item`
--

DROP TABLE IF EXISTS `ims_eso_sale_spec_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_spec_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) DEFAULT '0',
  `specid` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT '',
  `thumb` varchar(255) DEFAULT '',
  `show` int(11) DEFAULT '0',
  `displayorder` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `indx_uniacid` (`uniacid`),
  KEY `indx_specid` (`specid`),
  KEY `indx_show` (`show`),
  KEY `indx_displayorder` (`displayorder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_spec_item`
--

LOCK TABLES `ims_eso_sale_spec_item` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_spec_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_eso_sale_spec_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_tax_code`
--

DROP TABLE IF EXISTS `ims_eso_sale_tax_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_tax_code` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `taxCode` varchar(32) NOT NULL,
  `taxRate` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_tax_code`
--

LOCK TABLES `ims_eso_sale_tax_code` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_tax_code` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_tax_code` VALUES (1,'1000000','0.1'),(2,'2000000','0.5'),(3,'3000000','0.5'),(4,'4000000','0.2'),(5,'5000000','0.1'),(6,'6000000','0.1'),(7,'7000000','0.3'),(8,'7000001','0.2'),(9,'8000000','0.1'),(10,'9000000','0.5'),(11,'10000000','0.1'),(12,'11000000','0.1'),(13,'11000001','0.2'),(14,'12000000','0.1'),(15,'13000000','0.2'),(16,'14000000','0.2'),(17,'15000000','0.2'),(18,'16000000','0.2'),(19,'17000000','0.1'),(20,'17000001 ','0.2'),(21,'18000000','0.2'),(22,'19000000','0.1'),(23,'20000000','0.1'),(24,'21000000','0.1'),(25,'22000000','0.1'),(26,'23000000','0.1'),(27,'24000000','0.1'),(28,'25000000','0.3'),(29,'25000001','0.1'),(30,'26000000','0.2'),(31,'27000000','0.1');
/*!40000 ALTER TABLE `ims_eso_sale_tax_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_unit`
--

DROP TABLE IF EXISTS `ims_eso_sale_unit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_unit` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `code` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=122 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_unit`
--

LOCK TABLES `ims_eso_sale_unit` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_unit` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_unit` VALUES (1,'001','台'),(2,'002','座'),(3,'003','辆'),(4,'004','艘'),(5,'005','架'),(6,'006','套'),(7,'007','个'),(8,'008','只'),(9,'009','头'),(10,'010','张'),(11,'011','件'),(12,'012','支'),(13,'013','枝'),(14,'014','根'),(15,'015','条'),(16,'016','把'),(17,'017','块'),(18,'018','卷'),(19,'019','副'),(20,'020','片'),(21,'021','组'),(22,'022','份'),(23,'023','幅'),(24,'025','双'),(25,'026','对'),(26,'027','棵'),(27,'028','株'),(28,'029','井'),(29,'030','米'),(30,'031','盘'),(31,'032','平方米'),(32,'033','立方米'),(33,'034','筒'),(34,'035','千克'),(35,'036','克'),(36,'037','盆'),(37,'038','万个'),(38,'039','具'),(39,'040','百副'),(40,'041','百支'),(41,'042','百把'),(42,'043','百个'),(43,'044','百片'),(44,'045','刀'),(45,'046','疋'),(46,'047','公担'),(47,'048','扇'),(48,'049','百枝'),(49,'050','千只'),(50,'051','千块'),(51,'052','千盒'),(52,'053','千枝'),(53,'054','千个'),(54,'055','亿支'),(55,'056','亿个'),(56,'057','万套'),(57,'058','千张'),(58,'059','万张'),(59,'060','千伏安'),(60,'061','千瓦'),(61,'062','千瓦时'),(62,'063','千升'),(63,'067','英尺'),(64,'070','吨'),(65,'071','长吨'),(66,'072','短吨'),(67,'073','司马担'),(68,'074','司马斤'),(69,'075','斤'),(70,'076','磅'),(71,'077','担'),(72,'078','英担'),(73,'079','短担'),(74,'080','两'),(75,'081','市担'),(76,'083','盎司'),(77,'084','克拉'),(78,'085','市尺'),(79,'086','码'),(80,'088','英寸'),(81,'089','寸'),(82,'095','升'),(83,'096','毫升'),(84,'097','英加仑'),(85,'098','美加仑'),(86,'099','立方英尺'),(87,'101','立方尺'),(88,'110','平方码'),(89,'111','平方英尺'),(90,'112','平方尺'),(91,'115','英制马力'),(92,'116','公制马力'),(93,'118','令'),(94,'120','箱'),(95,'121','批'),(96,'122','罐'),(97,'123','桶'),(98,'124','扎'),(99,'125','包'),(100,'126','箩'),(101,'127','打'),(102,'128','筐'),(103,'129','罗'),(104,'130','匹'),(105,'131','册'),(106,'132','本'),(107,'133','发'),(108,'134','枚'),(109,'135','捆'),(110,'136','袋'),(111,'139','粒'),(112,'140','盒'),(113,'141','合'),(114,'142','瓶'),(115,'143','千支'),(116,'144','万双'),(117,'145','万粒'),(118,'146','千粒'),(119,'147','千米'),(120,'148','千英尺'),(121,'163','部');
/*!40000 ALTER TABLE `ims_eso_sale_unit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_eso_sale_zip_code`
--

DROP TABLE IF EXISTS `ims_eso_sale_zip_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_eso_sale_zip_code` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `city` varchar(128) DEFAULT NULL,
  `province` varchar(128) DEFAULT NULL,
  `zipCode` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=512 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_eso_sale_zip_code`
--

LOCK TABLES `ims_eso_sale_zip_code` WRITE;
/*!40000 ALTER TABLE `ims_eso_sale_zip_code` DISABLE KEYS */;
INSERT INTO `ims_eso_sale_zip_code` VALUES (1,'广州市','广东','510000'),(2,'汕尾市','广东','516600'),(3,'阳江市','广东','529500'),(4,'揭阳市 ','广东','522000'),(5,'茂名市','广东','525000'),(6,'江门市','广东','529000'),(7,'韶关市','广东','512000'),(8,'惠州市','广东','516000'),(9,'梅州市','广东','514000'),(10,'汕头市','广东','515000'),(11,'深圳市','广东','518000'),(12,'珠海市','广东','519000'),(13,'佛山市','广东','528000'),(14,'肇庆市','广东','526000'),(15,'湛江市','广东','524000'),(16,'中山市','广东','528400'),(17,'河源市','广东','517000'),(18,'清远市 ','广东','511500'),(19,'云浮市','广东','527000'),(20,'潮州市','广东','521000'),(21,'东莞市','广东','523000'),(22,'福州市','福建','350000'),(23,'厦门市','福建','361000'),(24,'宁德市','福建','352100'),(25,'莆田市','福建','351100'),(26,'泉州市','福建','362000'),(27,'漳州市','福建','363000'),(28,'龙岩市','福建','364000'),(29,'三明市','福建','365000'),(30,'南平市','福建','353000'),(31,'南宁市','广西','530000'),(32,'崇左市','广西','532000'),(33,'来宾市','广西','546100'),(34,'柳州市','广西','545000'),(35,'桂林市','广西','541000'),(36,'梧州市','广西','543000'),(37,'贺州市','广西','542800'),(38,'贵港市','广西','537100'),(39,'玉林市','广西','537000'),(40,'百色市','广西','533000'),(41,'钦州市','广西','535000'),(42,'河池市','广西','547000'),(43,'北海市','广西','536000'),(44,'防城港市','广西','535700'),(45,'海南三沙市','海南','572000'),(46,'海南海口市','海南','571000'),(47,'海南三亚市','海南','572000'),(48,'长沙市','湖南','410011'),(49,'株洲市','湖南','412000'),(50,'湘潭市','湖南','411100'),(51,'衡阳市','湖南','421001'),(52,'邵阳市','湖南','422000'),(53,'岳阳市','湖南','414000'),(54,'张家界市','湖南','427000'),(55,'益阳市','湖南','413000'),(56,'常德市','湖南','415000'),(57,'娄底市','湖南','417000'),(58,'郴州市','湖南','423000'),(59,'永州市','湖南','425000'),(60,'怀化市','湖南','418000'),(61,'湘西州','湖南','416000'),(62,'贵阳市','贵州','550000'),(63,'六盘水市','贵州','553000'),(64,'遵义市','贵州','563000'),(65,'铜仁市','贵州','554300'),(66,'毕节市','贵州','551700'),(67,'安顺市','贵州','561000'),(68,'南昌市','江西','330000'),(69,'赣州市','江西','341000'),(70,'宜春市','江西','336000'),(71,'吉安市','江西','343000'),(72,'上饶市','江西','334000'),(73,'抚州市','江西','344000'),(74,'九江市','江西','332000'),(75,'景德镇市','江西','333000'),(76,'萍乡市','江西','337000'),(77,'新余市','江西','338000'),(78,'鹰潭市','江西','335000'),(79,'成都市','四川','610000'),(80,'绵阳市','四川','621000'),(81,'自贡市','四川','643000'),(82,'攀枝花市','四川','617000'),(83,'泸州市','四川','646000'),(84,'德阳市','四川','618000'),(85,'广元市','四川','628000'),(86,'遂宁市','四川','629000'),(87,'内江市','四川','641000'),(88,'乐山市','四川','614000'),(89,'资阳市','四川','641300'),(90,'宜宾市','四川','644000'),(91,'南充市','四川','637000'),(92,'达州市','四川','635000'),(93,'雅安市','四川','625000'),(94,'广安市','四川','638500'),(95,'巴中市','四川','636600'),(96,'眉山市','四川','620000'),(97,'阿坝州','四川','624000'),(98,'甘孜州','四川','626700'),(99,'凉山州','四川','615000'),(100,'武汉市','湖北','430000'),(101,'黄石市','湖北','435000'),(102,'十堰市','湖北','442000'),(103,'荆州市','湖北','434000'),(104,'宜昌市','湖北','443000'),(105,'襄阳市','湖北','441000'),(106,'鄂州市','湖北','436000'),(107,'荆门市','湖北','448000'),(108,'黄冈市','湖北','438000'),(109,'孝感市','湖北','432000'),(110,'咸宁市','湖北','437000'),(111,'仙桃市','湖北','433000'),(112,'潜江市','湖北','433100'),(113,'随州市','湖北','441300'),(114,'恩施州','湖北','445000'),(115,'神农架林区','湖北','442400'),(116,'天门市','湖北','431700'),(117,'昆明市','云南','650000'),(118,'曲靖市','云南','655000'),(119,'玉溪市','云南','653100'),(120,'普洱市','云南','665000'),(121,'保山市','云南','678000'),(122,'大理州','云南','671000'),(123,'怒江州','云南','673200'),(124,'昭通市','云南','650000'),(125,'丽江市','云南','674100'),(126,'红河州','云南','661400'),(127,'西双版纳州','云南','666100'),(128,'楚雄州','云南','600000'),(129,'德宏州','云南','600000'),(130,'临沧市','云南','677000'),(131,'文山州','云南','660000'),(132,'迪庆州','云南','674400'),(133,'杭州市','浙江','310000'),(134,'宁波市','浙江','315000'),(135,'温州市','浙江','325000'),(136,'嘉兴市','浙江','314000'),(137,'湖州市','浙江','313000'),(138,'绍兴市','浙江','312000'),(139,'金华市','浙江','321000'),(140,'衢州市','浙江','324000'),(141,'舟山市','浙江','316000'),(142,'台州市','浙江','318000'),(143,'丽水市','浙江','323000'),(144,'南京市','江苏','210000'),(145,'无锡市','江苏','214000'),(146,'徐州市','江苏','221000'),(147,'常州市','江苏','213000'),(148,'苏州市','江苏','215000'),(149,'南通市','江苏','216000'),(150,'连云港市','江苏','222000'),(151,'淮安市','江苏','223000'),(152,'盐城市','江苏','224000'),(153,'扬州市','江苏','225000'),(154,'镇江市','江苏','212000'),(155,'泰州市','江苏','225300'),(156,'宿迁市','江苏','223800'),(157,'济南市','山东','250000'),(158,'青岛市','山东','266000'),(159,'淄博市','山东','255000'),(160,'枣庄市','山东','277000'),(161,'东营市','山东','257000'),(162,'烟台市','山东','264000'),(163,'潍坊市','山东','261000'),(164,'济宁市','山东','272000'),(165,'泰安市','山东','271000'),(166,'威海市','山东','264200'),(167,'日照市','山东','276800'),(168,'莱芜市','山东','271100'),(169,'临沂市','山东','276000'),(170,'德州市','山东','253000'),(171,'聊城市','山东','252000'),(172,'滨州市','山东','256600'),(173,'菏泽市','山东','274000'),(174,'太原市','山西','030000'),(175,'大同市','山西','037000'),(176,'阳泉市','山西','045000'),(177,'长治市','山西','046000'),(178,'晋城市','山西','048000'),(179,'朔州市','山西','036000'),(180,'晋中市','山西','030600'),(181,'运城市','山西','044000'),(182,'忻州市','山西','034000'),(183,'临汾市','山西','041000'),(184,'吕梁市','山西','033000'),(185,'郑州市','河南','450000'),(186,'开封市','河南','475000'),(187,'平顶山市','河南','467000'),(188,'洛阳市','河南','471000'),(189,'商丘市','河南','476000'),(190,'安阳市','河南','455000'),(191,'新乡市','河南','453000'),(192,'许昌市','河南','461000'),(193,'鹤壁市','河南','458000'),(194,'焦作市','河南','454000'),(195,'濮阳市','河南','457000'),(196,'漯河市','河南','462000'),(197,'三门峡市','河南','472000'),(198,'周口市','河南','466000'),(199,'驻马店市','河南','463000'),(200,'南阳市','河南','473000'),(201,'信阳市','河南','464000'),(202,'石家庄市','河北','050000'),(203,'唐山市','河北','063000'),(204,'秦皇岛市','河北','066000'),(205,'邯郸市','河北','056000'),(206,'邢台市','河北','054000'),(207,'保定市','河北','071000'),(208,'张家口市','河北','075000'),(209,'承德市','河北','067000'),(210,'沧州市','河北','061000'),(211,'廊坊市','河北','065000'),(212,'衡水市','河北','053000'),(213,'合肥市','安徽','230000'),(214,'蚌埠市','安徽','233000'),(215,'芜湖市','安徽','241000'),(216,'淮南市','安徽','232000'),(217,'马鞍山市','安徽','243000'),(218,'铜陵市','安徽','244000'),(219,'淮北市','安徽','235000'),(220,'安庆市','安徽','246000'),(221,'宿州市','安徽','234000'),(222,'亳州市','安徽','236000'),(223,'阜阳市','安徽','236000'),(224,'黄山市','安徽','245000'),(225,'滁州市','安徽','239000'),(226,'宣城市','安徽','242000'),(227,'六安市','安徽','237000'),(228,'池州市','安徽','247000'),(229,'沈阳市','辽宁','110000'),(230,'大连市','辽宁','116000'),(231,'鞍山市','辽宁','114000'),(232,'抚顺市','辽宁','113000'),(233,'本溪市','辽宁','117000'),(234,'丹东市','辽宁','118000'),(235,'朝阳市','辽宁','122000'),(236,'铁岭市','辽宁','112000'),(237,'锦州市','辽宁','121000'),(238,'葫芦岛市','辽宁','125000'),(239,'营口市','辽宁','115000'),(240,'阜新市','辽宁','123000'),(241,'辽阳市','辽宁','111000'),(242,'盘锦市','辽宁','124000'),(243,'长春市','吉林','130000'),(244,'市','吉林','132000'),(245,'四平市','吉林','136000'),(246,'辽源市','吉林','136200'),(247,'通化市','吉林','134000'),(248,'白山市','吉林','134300'),(249,'白城市','吉林','137000'),(250,'延边州','吉林','133000'),(251,'松原市','吉林','138000'),(252,'哈尔滨市','黑龙江','150000'),(253,'齐齐哈尔市','黑龙江','161000'),(254,'牡丹江市','黑龙江','157000'),(255,'佳木斯市','黑龙江','154000'),(256,'大庆市','黑龙江','163000'),(257,'鸡西市','黑龙江','158100'),(258,'双鸭山市','黑龙江','155100'),(259,'伊春市','黑龙江','153000'),(260,'七台河市','黑龙江','154600'),(261,'鹤岗市','黑龙江','154100'),(262,'黑河市','黑龙江','164300'),(263,'绥化市','黑龙江','152000'),(264,'大兴安岭地区','黑龙江','165000'),(265,'呼和浩特市','内蒙古','100100'),(266,'包头市','内蒙古','140100'),(267,'乌海市','内蒙古','160000'),(268,'赤峰市','内蒙古','240000'),(269,'通辽市','内蒙古','280000'),(270,'鄂尔多斯市','内蒙古','170000'),(271,'呼伦贝尔市','内蒙古','162600'),(272,'巴彦淖尔市','内蒙古','150000'),(273,'乌兰察布市','内蒙古','133500'),(274,'兴安盟','内蒙古','137400'),(275,'锡林郭勒盟','内蒙古','270000'),(276,'阿拉善盟','内蒙古','750300'),(277,'西安市','陕西','710000'),(278,'宝鸡市','陕西','721000'),(279,'咸阳市','陕西','711200'),(280,'渭南市','陕西','711700'),(281,'铜川市','陕西','727000'),(282,'延安市','陕西','715700'),(283,'榆林市','陕西','718000'),(284,'汉中市','陕西','723000'),(285,'安康市','陕西','725000'),(286,'商洛市','陕西','726000'),(287,'银川市','宁夏','750000'),(288,'石嘴市','宁夏','753000'),(289,'吴忠市','宁夏','751100'),(290,'固原市','宁夏','756000'),(291,'中卫市','宁夏','755000'),(292,'西宁市','青海','810000'),(293,'海东市','青海','810500'),(294,'海北州','青海','810200'),(295,'海南州','青海','811700'),(296,'海西州','青海','816000'),(297,'黄南州','青海','811200'),(298,'果洛州','青海','813500'),(299,'玉树州','青海','815000'),(300,'兰州市','甘肃','730000'),(301,'嘉峪关市','甘肃','735100'),(302,'金昌市','甘肃','737000'),(303,'白银市','甘肃','730400'),(304,'天水市','甘肃','741000'),(305,'武威市','甘肃','733000'),(306,'张掖市','甘肃','734000'),(307,'酒泉市','甘肃','735000'),(308,'平凉市','甘肃','743400'),(309,'庆阳市','甘肃','744500'),(310,'定西市','甘肃','743000'),(311,'陇南市','甘肃','742100'),(312,'临夏州','甘肃','730400'),(313,'甘南州','甘肃','746300'),(314,'乌鲁木齐市','新疆','830000'),(315,'克拉玛依市','新疆','833600'),(316,'吐鲁番地区','新疆','838000'),(317,'哈密地区','新疆','839000'),(318,'阿克苏地区','新疆','842000'),(319,'喀什地区','新疆','843800'),(320,'和田地区','新疆','845100'),(321,'阿勒泰地区','新疆','836000'),(322,'昌吉州','新疆','831100'),(323,'博尔塔拉州','新疆','833300'),(324,'巴音郭楞州','新疆','841000'),(325,'克孜勒苏柯尔克孜州','新疆','843500'),(326,'伊犁哈萨克州','新疆','833200'),(327,'石河子市','新疆','832000'),(328,'阿拉尔市','新疆','843300'),(329,'图木舒克市','新疆','843900'),(330,'五家渠市','新疆','831300'),(331,'拉萨市','西藏','850100'),(332,'昌都地区','西藏','854000'),(333,'林芝地区','西藏','860000'),(334,'山南地区','西藏','850700'),(335,'日喀则地区','西藏','857000'),(336,'那曲地区','西藏','852000'),(337,'阿里地区','西藏','859100'),(338,'北京市','北京','100000'),(339,'天津市','天津','300000'),(340,'上海市','上海','200000'),(341,'重庆市','重庆','400000');
/*!40000 ALTER TABLE `ims_eso_sale_zip_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_images_reply`
--

DROP TABLE IF EXISTS `ims_images_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_images_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_images_reply`
--

LOCK TABLES `ims_images_reply` WRITE;
/*!40000 ALTER TABLE `ims_images_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_images_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_izc_lightbox_comment`
--

DROP TABLE IF EXISTS `ims_izc_lightbox_comment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_izc_lightbox_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `weid` int(11) NOT NULL,
  `list_id` int(11) NOT NULL,
  `from` varchar(10) NOT NULL,
  `content` varchar(255) NOT NULL,
  `create_time` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `from_user` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_izc_lightbox_comment`
--

LOCK TABLES `ims_izc_lightbox_comment` WRITE;
/*!40000 ALTER TABLE `ims_izc_lightbox_comment` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_izc_lightbox_comment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_card`
--

DROP TABLE IF EXISTS `ims_mc_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(100) NOT NULL,
  `color` varchar(255) NOT NULL,
  `background` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `format` varchar(50) NOT NULL,
  `fields` varchar(1000) NOT NULL,
  `snpos` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `business` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_card`
--

LOCK TABLES `ims_mc_card` WRITE;
/*!40000 ALTER TABLE `ims_mc_card` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_card_members`
--

DROP TABLE IF EXISTS `ims_mc_card_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_card_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) DEFAULT NULL,
  `cid` int(10) NOT NULL,
  `cardsn` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_card_members`
--

LOCK TABLES `ims_mc_card_members` WRITE;
/*!40000 ALTER TABLE `ims_mc_card_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_card_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_chats_record`
--

DROP TABLE IF EXISTS `ims_mc_chats_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_chats_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `flag` tinyint(3) unsigned NOT NULL,
  `openid` varchar(32) NOT NULL,
  `msgtype` varchar(15) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`acid`),
  KEY `openid` (`openid`),
  KEY `createtime` (`createtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_chats_record`
--

LOCK TABLES `ims_mc_chats_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_chats_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_chats_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_credits_recharge`
--

DROP TABLE IF EXISTS `ims_mc_credits_recharge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_credits_recharge` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `tid` varchar(20) NOT NULL,
  `transid` varchar(30) NOT NULL,
  `fee` varchar(10) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_uniacid_uid` (`uniacid`,`uid`),
  KEY `idx_tid` (`tid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_credits_recharge`
--

LOCK TABLES `ims_mc_credits_recharge` WRITE;
/*!40000 ALTER TABLE `ims_mc_credits_recharge` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_credits_recharge` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_credits_record`
--

DROP TABLE IF EXISTS `ims_mc_credits_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_credits_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `uniacid` int(11) NOT NULL,
  `credittype` varchar(10) NOT NULL,
  `num` int(10) NOT NULL,
  `operator` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `remark` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_credits_record`
--

LOCK TABLES `ims_mc_credits_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_credits_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_credits_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_fans_groups`
--

DROP TABLE IF EXISTS `ims_mc_fans_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_fans_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groups` varchar(10000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_fans_groups`
--

LOCK TABLES `ims_mc_fans_groups` WRITE;
/*!40000 ALTER TABLE `ims_mc_fans_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_fans_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_groups`
--

DROP TABLE IF EXISTS `ims_mc_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_groups` (
  `groupid` int(11) NOT NULL AUTO_INCREMENT,
  `uniacid` int(11) NOT NULL,
  `title` varchar(20) NOT NULL,
  `orderlist` tinyint(4) unsigned NOT NULL,
  `isdefault` tinyint(4) NOT NULL,
  PRIMARY KEY (`groupid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_groups`
--

LOCK TABLES `ims_mc_groups` WRITE;
/*!40000 ALTER TABLE `ims_mc_groups` DISABLE KEYS */;
INSERT INTO `ims_mc_groups` VALUES (10,9,'默认会员组',0,1);
/*!40000 ALTER TABLE `ims_mc_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_handsel`
--

DROP TABLE IF EXISTS `ims_mc_handsel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_handsel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) NOT NULL,
  `touid` int(10) unsigned NOT NULL,
  `fromuid` varchar(32) NOT NULL,
  `module` varchar(30) NOT NULL,
  `sign` varchar(100) NOT NULL,
  `action` varchar(20) NOT NULL,
  `credit_value` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`touid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_handsel`
--

LOCK TABLES `ims_mc_handsel` WRITE;
/*!40000 ALTER TABLE `ims_mc_handsel` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_handsel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mapping_fans`
--

DROP TABLE IF EXISTS `ims_mc_mapping_fans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mapping_fans` (
  `fanid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `nickname` varchar(50) NOT NULL,
  `groupid` int(10) unsigned NOT NULL,
  `salt` char(8) NOT NULL,
  `follow` tinyint(1) unsigned NOT NULL,
  `followtime` int(10) unsigned NOT NULL,
  `unfollowtime` int(10) unsigned NOT NULL,
  `tag` varchar(1000) NOT NULL,
  `updatetime` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`fanid`),
  KEY `acid` (`acid`),
  KEY `uniacid` (`uniacid`),
  KEY `openid` (`openid`),
  KEY `updatetime` (`updatetime`),
  KEY `nickname` (`nickname`)
) ENGINE=MyISAM AUTO_INCREMENT=93 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mapping_fans`
--

LOCK TABLES `ims_mc_mapping_fans` WRITE;
/*!40000 ALTER TABLE `ims_mc_mapping_fans` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_mapping_fans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mapping_ucenter`
--

DROP TABLE IF EXISTS `ims_mc_mapping_ucenter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mapping_ucenter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `centeruid` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mapping_ucenter`
--

LOCK TABLES `ims_mc_mapping_ucenter` WRITE;
/*!40000 ALTER TABLE `ims_mc_mapping_ucenter` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_mapping_ucenter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_mass_record`
--

DROP TABLE IF EXISTS `ims_mc_mass_record`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_mass_record` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `groupname` varchar(50) NOT NULL,
  `fansnum` int(10) unsigned NOT NULL,
  `msgtype` varchar(10) NOT NULL,
  `content` varchar(10000) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`,`acid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_mass_record`
--

LOCK TABLES `ims_mc_mass_record` WRITE;
/*!40000 ALTER TABLE `ims_mc_mass_record` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mc_mass_record` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mc_members`
--

DROP TABLE IF EXISTS `ims_mc_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mc_members` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` varchar(8) NOT NULL,
  `groupid` int(11) NOT NULL,
  `credit1` decimal(10,2) unsigned NOT NULL,
  `credit2` decimal(10,2) unsigned NOT NULL,
  `credit3` decimal(10,2) unsigned NOT NULL,
  `credit4` decimal(10,2) unsigned NOT NULL,
  `credit5` decimal(10,2) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(255) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL,
  PRIMARY KEY (`uid`),
  KEY `groupid` (`groupid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mc_members`
--

LOCK TABLES `ims_mc_members` WRITE;
/*!40000 ALTER TABLE `ims_mc_members` DISABLE KEYS */;
INSERT INTO `ims_mc_members` VALUES (111,9,'13916933369','','ab896bf31cf9f92f5259b2bad4fd0b51','j9woitkI',10,0.00,0.00,0.00,0.00,0.00,1433227606,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(112,9,'18068061759','','e9e75462169ae5c888920ac797996aa5','to8mmFbb',10,0.00,0.00,0.00,0.00,0.00,1433228710,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(113,9,'18621816275','','89abc96ed88cf31549618e8f2dac8a58','KGdE5472',10,0.00,0.00,0.00,0.00,0.00,1433249580,'','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `ims_mc_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_menu_event`
--

DROP TABLE IF EXISTS `ims_menu_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_menu_event` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `keyword` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `picmd5` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `picmd5` (`picmd5`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_menu_event`
--

LOCK TABLES `ims_menu_event` WRITE;
/*!40000 ALTER TABLE `ims_menu_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_menu_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_mobilenumber`
--

DROP TABLE IF EXISTS `ims_mobilenumber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_mobilenumber` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(10) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `dateline` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_mobilenumber`
--

LOCK TABLES `ims_mobilenumber` WRITE;
/*!40000 ALTER TABLE `ims_mobilenumber` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_mobilenumber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_modules`
--

DROP TABLE IF EXISTS `ims_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_modules` (
  `mid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `version` varchar(10) NOT NULL,
  `ability` varchar(500) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `settings` tinyint(1) NOT NULL,
  `subscribes` varchar(500) NOT NULL,
  `handles` varchar(500) NOT NULL,
  `isrulefields` tinyint(1) NOT NULL,
  `issystem` tinyint(1) unsigned NOT NULL,
  `issolution` tinyint(1) unsigned NOT NULL,
  `target` int(10) unsigned NOT NULL,
  PRIMARY KEY (`mid`),
  KEY `idx_name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_modules`
--

LOCK TABLES `ims_modules` WRITE;
/*!40000 ALTER TABLE `ims_modules` DISABLE KEYS */;
INSERT INTO `ims_modules` VALUES (1,'basic','system','基本文字回复','1.0','和您进行简单对话','一问一答得简单对话. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的回复内容.','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(2,'news','system','基本混合图文回复','1.0','为你提供生动的图文资讯','一问一答得简单对话, 但是回复内容包括图片文字等更生动的媒体内容. 当访客的对话语句中包含指定关键字, 或对话语句完全等于特定关键字, 或符合某些特定的格式时. 系统自动应答设定好的图文回复内容.','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(3,'music','system','基本音乐回复','1.0','提供语音、音乐等音频类回复','在回复规则中可选择具有语音、音乐等音频类的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝，实现一问一答得简单对话。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(4,'userapi','system','自定义接口回复','1.1','更方便的第三方接口设置','自定义接口又称第三方接口，可以让开发者更方便的接入微擎系统，高效的与微信公众平台进行对接整合。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(5,'recharge','system','会员中心充值模块','1.0','提供会员充值功能','','WeEngine Team','http://www.we7.cc/',0,'','',0,1,0,0),(6,'custom','system','多客服转接','1.0.0','用来接入腾讯的多客服系统','','WeEngine Team','http://bbs.we7.cc',0,'a:0:{}','a:6:{i:0;s:5:\"image\";i:1;s:5:\"voice\";i:2;s:5:\"video\";i:3;s:8:\"location\";i:4;s:4:\"link\";i:5;s:4:\"text\";}',1,1,0,0),(7,'images','system','基本图片回复','1.0','提供图片回复','在回复规则中可选择具有图片的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝图片。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(8,'video','system','基本视频回复','1.0','提供图片回复','在回复规则中可选择具有视频的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝视频。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(9,'voice','system','基本语音回复','1.0','提供语音回复','在回复规则中可选择具有语音的回复内容，并根据用户所设置的特定关键字精准的返回给粉丝语音。','WeEngine Team','http://www.we7.cc/',0,'','',1,1,0,0),(10,'chats','system','发送客服消息','1.0','公众号可以在粉丝最后发送消息的48小时内无限制发送消息','','WeEngine Team','http://www.we7.cc/',0,'','',0,1,0,0),(13,'we7_demo','other','官方示例','1.0','此模块提供基本的功能展示','此模块提供基本的功能展示','微擎团队','http://bbs.we7.cc/',1,'a:13:{i:0;s:4:\"text\";i:1;s:5:\"image\";i:2;s:5:\"voice\";i:3;s:5:\"video\";i:4;s:8:\"location\";i:5;s:4:\"link\";i:6;s:9:\"subscribe\";i:7;s:11:\"unsubscribe\";i:8;s:2:\"qr\";i:9;s:5:\"trace\";i:10;s:5:\"click\";i:11;s:4:\"view\";i:12;s:5:\"enter\";}','a:2:{i:0;s:8:\"location\";i:1;s:4:\"text\";}',1,0,0,0),(20,'eso_sale','business','分销系统','2.0','分销系统，全民分佣啦！','分销系统，全民分佣啦！','陈凌俊','',1,'a:0:{}','a:1:{i:0;s:4:\"text\";}',0,0,0,0);
/*!40000 ALTER TABLE `ims_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_modules_bindings`
--

DROP TABLE IF EXISTS `ims_modules_bindings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_modules_bindings` (
  `eid` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(30) NOT NULL,
  `entry` varchar(10) NOT NULL,
  `call` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `do` varchar(30) NOT NULL,
  `state` varchar(200) NOT NULL,
  `direct` int(11) NOT NULL,
  PRIMARY KEY (`eid`),
  KEY `idx_module` (`module`)
) ENGINE=MyISAM AUTO_INCREMENT=105 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_modules_bindings`
--

LOCK TABLES `ims_modules_bindings` WRITE;
/*!40000 ALTER TABLE `ims_modules_bindings` DISABLE KEYS */;
INSERT INTO `ims_modules_bindings` VALUES (103,'eso_sale','menu','','基础设置','rules','',0),(104,'eso_sale','menu','','首页广告设置','adv','',0),(102,'eso_sale','menu','','物流配送设置','dispatch','',0),(101,'eso_sale','menu','','佣金审核','commission','',0),(100,'eso_sale','menu','','代理管理','fansmanager','',0),(98,'eso_sale','menu','','CRM会员管理','charge','',0),(99,'eso_sale','menu','','订单管理','order','',0),(97,'eso_sale','menu','','商品管理','goods','',0),(94,'eso_sale','menu','','积分兑换管理','credit','',0),(96,'eso_sale','menu','','商品分类','category','',0),(36,'we7_demo','shortcut','','快捷2','quick2','',0),(35,'we7_demo','shortcut','','快捷1','quick1','',0),(34,'we7_demo','profile','','中心2','uc2','',0),(33,'we7_demo','profile','','中心1','uc1','',0),(32,'we7_demo','home','','导航2','nav2','',0),(31,'we7_demo','home','','导航1','nav1','',0),(30,'we7_demo','menu','','管理2','manage2','',0),(29,'we7_demo','menu','','管理1','manage1','',0),(28,'we7_demo','cover','','入口2（不需登录）','index2','',1),(27,'we7_demo','cover','','入口1','index1','',0),(37,'we7_demo','function','','页面1','direct1','',0),(95,'eso_sale','menu','','积分商品设置','award','',0),(93,'eso_sale','cover','','购物入口设置','list','',0),(92,'eso_sale','cover','','代理入口','fansindex','',0),(91,'eso_sale','cover','','排行榜入口设置','phb','',0),(90,'eso_sale','cover','','积分兑换入口设置','award','',0);
/*!40000 ALTER TABLE `ims_modules_bindings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_music_reply`
--

DROP TABLE IF EXISTS `ims_music_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_music_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `url` varchar(300) NOT NULL,
  `hqurl` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_music_reply`
--

LOCK TABLES `ims_music_reply` WRITE;
/*!40000 ALTER TABLE `ims_music_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_music_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_news_reply`
--

DROP TABLE IF EXISTS `ims_news_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_news_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `author` varchar(64) NOT NULL,
  `description` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `url` varchar(255) NOT NULL,
  `displayorder` int(10) NOT NULL,
  `incontent` tinyint(1) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_news_reply`
--

LOCK TABLES `ims_news_reply` WRITE;
/*!40000 ALTER TABLE `ims_news_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_news_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_profile_fields`
--

DROP TABLE IF EXISTS `ims_profile_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_profile_fields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `field` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `displayorder` smallint(6) NOT NULL,
  `required` tinyint(1) NOT NULL,
  `unchangeable` tinyint(1) NOT NULL,
  `showinregister` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_profile_fields`
--

LOCK TABLES `ims_profile_fields` WRITE;
/*!40000 ALTER TABLE `ims_profile_fields` DISABLE KEYS */;
INSERT INTO `ims_profile_fields` VALUES (1,'realname',1,'真实姓名','',0,1,1,1),(2,'nickname',1,'昵称','',1,1,0,1),(3,'avatar',1,'头像','',1,0,0,0),(4,'qq',1,'QQ号','',0,0,0,1),(5,'mobile',1,'手机号码','',0,0,0,0),(6,'vip',1,'VIP级别','',0,0,0,0),(7,'gender',1,'性别','',0,0,0,0),(8,'birthyear',1,'出生生日','',0,0,0,0),(9,'constellation',1,'星座','',0,0,0,0),(10,'zodiac',1,'生肖','',0,0,0,0),(11,'telephone',1,'固定电话','',0,0,0,0),(12,'idcard',1,'证件号码','',0,0,0,0),(13,'studentid',1,'学号','',0,0,0,0),(14,'grade',1,'班级','',0,0,0,0),(15,'address',1,'邮寄地址','',0,0,0,0),(16,'zipcode',1,'邮编','',0,0,0,0),(17,'nationality',1,'国籍','',0,0,0,0),(18,'resideprovince',1,'居住地址','',0,0,0,0),(19,'graduateschool',1,'毕业学校','',0,0,0,0),(20,'company',1,'公司','',0,0,0,0),(21,'education',1,'学历','',0,0,0,0),(22,'occupation',1,'职业','',0,0,0,0),(23,'position',1,'职位','',0,0,0,0),(24,'revenue',1,'年收入','',0,0,0,0),(25,'affectivestatus',1,'情感状态','',0,0,0,0),(26,'lookingfor',1,' 交友目的','',0,0,0,0),(27,'bloodtype',1,'血型','',0,0,0,0),(28,'height',1,'身高','',0,0,0,0),(29,'weight',1,'体重','',0,0,0,0),(30,'alipay',1,'支付宝帐号','',0,0,0,0),(31,'msn',1,'MSN','',0,0,0,0),(32,'email',1,'电子邮箱','',0,0,0,0),(33,'taobao',1,'阿里旺旺','',0,0,0,0),(34,'site',1,'主页','',0,0,0,0),(35,'bio',1,'自我介绍','',0,0,0,0),(36,'interest',1,'兴趣爱好','',0,0,0,0);
/*!40000 ALTER TABLE `ims_profile_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_qrcode`
--

DROP TABLE IF EXISTS `ims_qrcode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_qrcode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `qrcid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `keyword` varchar(100) NOT NULL,
  `model` tinyint(1) unsigned NOT NULL,
  `ticket` varchar(250) NOT NULL,
  `expire` int(10) unsigned NOT NULL,
  `subnum` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_qrcid` (`qrcid`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_qrcode`
--

LOCK TABLES `ims_qrcode` WRITE;
/*!40000 ALTER TABLE `ims_qrcode` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_qrcode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_qrcode_stat`
--

DROP TABLE IF EXISTS `ims_qrcode_stat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_qrcode_stat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `acid` int(10) unsigned NOT NULL,
  `qid` int(10) unsigned NOT NULL,
  `openid` varchar(50) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `qrcid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_qrcode_stat`
--

LOCK TABLES `ims_qrcode_stat` WRITE;
/*!40000 ALTER TABLE `ims_qrcode_stat` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_qrcode_stat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_rule`
--

DROP TABLE IF EXISTS `ims_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_rule`
--

LOCK TABLES `ims_rule` WRITE;
/*!40000 ALTER TABLE `ims_rule` DISABLE KEYS */;
INSERT INTO `ims_rule` VALUES (1,0,'城市天气','userapi',255,1),(2,0,'百度百科','userapi',255,1),(3,0,'即时翻译','userapi',255,1),(4,0,'今日老黄历','userapi',255,1),(5,0,'看新闻','userapi',255,1),(6,0,'快递查询','userapi',255,1),(19,9,'代理入口','cover',0,1);
/*!40000 ALTER TABLE `ims_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_rule_keyword`
--

DROP TABLE IF EXISTS `ims_rule_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_rule_keyword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `content` varchar(255) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_content` (`content`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_rule_keyword`
--

LOCK TABLES `ims_rule_keyword` WRITE;
/*!40000 ALTER TABLE `ims_rule_keyword` DISABLE KEYS */;
INSERT INTO `ims_rule_keyword` VALUES (1,1,0,'userapi','^.+天气$',3,255,1),(2,2,0,'userapi','^百科.+$',3,255,1),(3,2,0,'userapi','^定义.+$',3,255,1),(4,3,0,'userapi','^@.+$',3,255,1),(5,4,0,'userapi','日历',1,255,1),(6,4,0,'userapi','万年历',1,255,1),(7,4,0,'userapi','黄历',1,255,1),(8,4,0,'userapi','几号',1,255,1),(9,5,0,'userapi','新闻',1,255,1),(10,6,0,'userapi','^(申通|圆通|中通|汇通|韵达|顺丰|EMS) *[a-z0-9]{1,}$',3,255,1),(30,19,9,'cover','test',1,0,1);
/*!40000 ALTER TABLE `ims_rule_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_article`
--

DROP TABLE IF EXISTS `ims_site_article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_article` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `iscommend` tinyint(1) NOT NULL,
  `ishot` tinyint(1) unsigned NOT NULL,
  `pcate` int(10) unsigned NOT NULL,
  `ccate` int(10) unsigned NOT NULL,
  `template` varchar(300) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `content` mediumtext NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `source` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `displayorder` int(10) unsigned NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `type` varchar(10) NOT NULL,
  `credit` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_iscommend` (`iscommend`),
  KEY `idx_ishot` (`ishot`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_article`
--

LOCK TABLES `ims_site_article` WRITE;
/*!40000 ALTER TABLE `ims_site_article` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_category`
--

DROP TABLE IF EXISTS `ims_site_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `nid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `parentid` int(10) unsigned NOT NULL,
  `displayorder` tinyint(3) unsigned NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `icon` varchar(100) NOT NULL,
  `description` varchar(100) NOT NULL,
  `template` varchar(300) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `templatefile` varchar(100) NOT NULL,
  `linkurl` varchar(500) NOT NULL,
  `ishomepage` tinyint(1) NOT NULL,
  `icontype` tinyint(1) unsigned NOT NULL,
  `css` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_category`
--

LOCK TABLES `ims_site_category` WRITE;
/*!40000 ALTER TABLE `ims_site_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_multi`
--

DROP TABLE IF EXISTS `ims_site_multi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_multi` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `title` varchar(30) NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `site_info` text NOT NULL,
  `quickmenu` varchar(2000) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_multi`
--

LOCK TABLES `ims_site_multi` WRITE;
/*!40000 ALTER TABLE `ims_site_multi` DISABLE KEYS */;
INSERT INTO `ims_site_multi` VALUES (9,9,'芝码客',9,'','a:2:{s:8:\"template\";s:7:\"default\";s:12:\"enablemodule\";a:0:{}}',0);
/*!40000 ALTER TABLE `ims_site_multi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_nav`
--

DROP TABLE IF EXISTS `ims_site_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_nav` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `section` tinyint(4) NOT NULL,
  `module` varchar(50) NOT NULL,
  `displayorder` smallint(5) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `url` varchar(255) NOT NULL,
  `icon` varchar(500) NOT NULL,
  `css` varchar(1000) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`),
  KEY `multiid` (`multiid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_nav`
--

LOCK TABLES `ims_site_nav` WRITE;
/*!40000 ALTER TABLE `ims_site_nav` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_slide`
--

DROP TABLE IF EXISTS `ims_site_slide`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_slide` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `multiid` int(10) unsigned NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `thumb` varchar(255) NOT NULL,
  `displayorder` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uniacid` (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_slide`
--

LOCK TABLES `ims_site_slide` WRITE;
/*!40000 ALTER TABLE `ims_site_slide` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_slide` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_styles`
--

DROP TABLE IF EXISTS `ims_site_styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_styles`
--

LOCK TABLES `ims_site_styles` WRITE;
/*!40000 ALTER TABLE `ims_site_styles` DISABLE KEYS */;
INSERT INTO `ims_site_styles` VALUES (9,9,1,'微站默认模板_eB82');
/*!40000 ALTER TABLE `ims_site_styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_styles_vars`
--

DROP TABLE IF EXISTS `ims_site_styles_vars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_styles_vars` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `templateid` int(10) unsigned NOT NULL,
  `styleid` int(10) unsigned NOT NULL,
  `variable` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `description` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_styles_vars`
--

LOCK TABLES `ims_site_styles_vars` WRITE;
/*!40000 ALTER TABLE `ims_site_styles_vars` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_site_styles_vars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_site_templates`
--

DROP TABLE IF EXISTS `ims_site_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_site_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `title` varchar(30) NOT NULL,
  `version` varchar(64) NOT NULL,
  `description` varchar(500) NOT NULL,
  `author` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `sections` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_site_templates`
--

LOCK TABLES `ims_site_templates` WRITE;
/*!40000 ALTER TABLE `ims_site_templates` DISABLE KEYS */;
INSERT INTO `ims_site_templates` VALUES (1,'default','微站默认模板','','由微擎提供默认微站模板套系','微擎团队','http://we7.cc','1',0);
/*!40000 ALTER TABLE `ims_site_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_solution_acl`
--

DROP TABLE IF EXISTS `ims_solution_acl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_solution_acl` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `module` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `eid` int(10) unsigned NOT NULL,
  `do` varchar(255) NOT NULL,
  `state` varchar(1000) NOT NULL,
  `enable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_module` (`module`),
  KEY `idx_eid` (`eid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_solution_acl`
--

LOCK TABLES `ims_solution_acl` WRITE;
/*!40000 ALTER TABLE `ims_solution_acl` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_solution_acl` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_keyword`
--

DROP TABLE IF EXISTS `ims_stat_keyword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_keyword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` varchar(10) NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_keyword`
--

LOCK TABLES `ims_stat_keyword` WRITE;
/*!40000 ALTER TABLE `ims_stat_keyword` DISABLE KEYS */;
INSERT INTO `ims_stat_keyword` VALUES (5,9,'19',30,1,1430897635,1430841600);
/*!40000 ALTER TABLE `ims_stat_keyword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_msg_history`
--

DROP TABLE IF EXISTS `ims_stat_msg_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_msg_history` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `kid` int(10) unsigned NOT NULL,
  `from_user` varchar(50) NOT NULL,
  `module` varchar(50) NOT NULL,
  `message` varchar(1000) NOT NULL,
  `type` varchar(10) NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_msg_history`
--

LOCK TABLES `ims_stat_msg_history` WRITE;
/*!40000 ALTER TABLE `ims_stat_msg_history` DISABLE KEYS */;
INSERT INTO `ims_stat_msg_history` VALUES (240,9,19,30,'fromUser','cover','a:4:{s:7:\"content\";s:4:\"test\";s:8:\"original\";N;s:11:\"redirection\";b:0;s:6:\"source\";N;}','text',1430897635);
/*!40000 ALTER TABLE `ims_stat_msg_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_stat_rule`
--

DROP TABLE IF EXISTS `ims_stat_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_stat_rule` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `rid` int(10) unsigned NOT NULL,
  `hit` int(10) unsigned NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_createtime` (`createtime`)
) ENGINE=MyISAM AUTO_INCREMENT=241 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_stat_rule`
--

LOCK TABLES `ims_stat_rule` WRITE;
/*!40000 ALTER TABLE `ims_stat_rule` DISABLE KEYS */;
INSERT INTO `ims_stat_rule` VALUES (240,9,19,1,1430897635,1430841600);
/*!40000 ALTER TABLE `ims_stat_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account`
--

DROP TABLE IF EXISTS `ims_uni_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account` (
  `uniacid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  PRIMARY KEY (`uniacid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account`
--

LOCK TABLES `ims_uni_account` WRITE;
/*!40000 ALTER TABLE `ims_uni_account` DISABLE KEYS */;
INSERT INTO `ims_uni_account` VALUES (9,-1,'芝码客','o2o');
/*!40000 ALTER TABLE `ims_uni_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account_modules`
--

DROP TABLE IF EXISTS `ims_uni_account_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account_modules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `module` varchar(50) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL,
  `settings` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_module` (`module`)
) ENGINE=MyISAM AUTO_INCREMENT=107 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account_modules`
--

LOCK TABLES `ims_uni_account_modules` WRITE;
/*!40000 ALTER TABLE `ims_uni_account_modules` DISABLE KEYS */;
INSERT INTO `ims_uni_account_modules` VALUES (95,9,'news',1,''),(96,9,'music',1,''),(97,9,'userapi',1,''),(98,9,'recharge',1,''),(99,9,'custom',1,''),(100,9,'images',1,''),(101,9,'video',1,''),(102,9,'voice',1,''),(103,9,'chats',1,''),(104,9,'we7_demo',1,''),(106,9,'eso_sale',1,''),(94,9,'basic',1,'');
/*!40000 ALTER TABLE `ims_uni_account_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_account_users`
--

DROP TABLE IF EXISTS `ims_uni_account_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_account_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_memberid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_account_users`
--

LOCK TABLES `ims_uni_account_users` WRITE;
/*!40000 ALTER TABLE `ims_uni_account_users` DISABLE KEYS */;
INSERT INTO `ims_uni_account_users` VALUES (9,9,1,'manager');
/*!40000 ALTER TABLE `ims_uni_account_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_group`
--

DROP TABLE IF EXISTS `ims_uni_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `modules` varchar(5000) NOT NULL,
  `templates` varchar(5000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_group`
--

LOCK TABLES `ims_uni_group` WRITE;
/*!40000 ALTER TABLE `ims_uni_group` DISABLE KEYS */;
INSERT INTO `ims_uni_group` VALUES (1,'体验套餐服务','a:1:{i:0;s:8:\"eso_sale\";}','N;'),(3,'测试啦','a:1:{i:0;s:8:\"eso_sale\";}','N;');
/*!40000 ALTER TABLE `ims_uni_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_settings`
--

DROP TABLE IF EXISTS `ims_uni_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_settings` (
  `uniacid` int(10) unsigned NOT NULL,
  `passport` varchar(200) NOT NULL,
  `oauth` varchar(100) NOT NULL,
  `jsauth_acid` int(10) unsigned NOT NULL,
  `uc` varchar(500) NOT NULL,
  `notify` varchar(2000) NOT NULL,
  `creditnames` varchar(500) NOT NULL,
  `creditbehaviors` varchar(500) NOT NULL,
  `welcome` varchar(60) NOT NULL,
  `default` varchar(60) NOT NULL,
  `default_message` varchar(1000) NOT NULL,
  `shortcuts` varchar(5000) NOT NULL,
  `payment` varchar(2000) NOT NULL,
  `groupdata` varchar(100) NOT NULL,
  `stat` varchar(300) NOT NULL,
  `menuset` text NOT NULL,
  `default_site` int(10) unsigned DEFAULT NULL,
  `sync` varchar(100) NOT NULL,
  PRIMARY KEY (`uniacid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_settings`
--

LOCK TABLES `ims_uni_settings` WRITE;
/*!40000 ALTER TABLE `ims_uni_settings` DISABLE KEYS */;
INSERT INTO `ims_uni_settings` VALUES (9,'','a:2:{s:6:\"status\";i:1;s:7:\"account\";i:10;}',0,'','a:1:{s:3:\"sms\";a:2:{s:7:\"balance\";i:0;s:9:\"signature\";s:0:\"\";}}','a:2:{s:7:\"credit1\";a:2:{s:5:\"title\";s:6:\"积分\";s:7:\"enabled\";i:1;}s:7:\"credit2\";a:2:{s:5:\"title\";s:6:\"余额\";s:7:\"enabled\";i:1;}}','a:2:{s:8:\"activity\";s:7:\"credit1\";s:8:\"currency\";s:7:\"credit2\";}','','','','','a:5:{s:6:\"credit\";a:1:{s:6:\"switch\";b:0;}s:6:\"alipay\";a:4:{s:6:\"switch\";b:1;s:7:\"account\";s:16:\"bamsion@sina.com\";s:7:\"partner\";s:16:\"2088911378703485\";s:6:\"secret\";s:32:\"jjabnuvtmlhy0jkqagiin0ni14bpz9mo\";}s:6:\"wechat\";a:8:{s:6:\"switch\";b:0;s:7:\"account\";b:0;s:7:\"signkey\";s:0:\"\";s:7:\"partner\";s:0:\"\";s:3:\"key\";s:0:\"\";s:7:\"version\";b:0;s:5:\"mchid\";b:0;s:6:\"apikey\";b:0;}s:8:\"delivery\";a:1:{s:6:\"switch\";b:0;}s:8:\"unionpay\";a:3:{s:6:\"switch\";b:0;s:11:\"signcertpwd\";s:0:\"\";s:5:\"merid\";s:0:\"\";}}','a:3:{s:8:\"isexpire\";i:0;s:10:\"oldgroupid\";s:0:\"\";s:7:\"endtime\";i:1430897573;}','','',9,'a:2:{s:6:\"switch\";i:0;s:4:\"acid\";s:0:\"\";}');
/*!40000 ALTER TABLE `ims_uni_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_uni_verifycode`
--

DROP TABLE IF EXISTS `ims_uni_verifycode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_uni_verifycode` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `receiver` varchar(50) NOT NULL,
  `verifycode` varchar(6) NOT NULL,
  `total` tinyint(3) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_uni_verifycode`
--

LOCK TABLES `ims_uni_verifycode` WRITE;
/*!40000 ALTER TABLE `ims_uni_verifycode` DISABLE KEYS */;
INSERT INTO `ims_uni_verifycode` VALUES (5,9,'15256595054','686864',1,1431858994);
/*!40000 ALTER TABLE `ims_uni_verifycode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_userapi_cache`
--

DROP TABLE IF EXISTS `ims_userapi_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_userapi_cache` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) NOT NULL,
  `content` text NOT NULL,
  `lastupdate` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_userapi_cache`
--

LOCK TABLES `ims_userapi_cache` WRITE;
/*!40000 ALTER TABLE `ims_userapi_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_userapi_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_userapi_reply`
--

DROP TABLE IF EXISTS `ims_userapi_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_userapi_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `description` varchar(300) NOT NULL,
  `apiurl` varchar(300) NOT NULL,
  `token` varchar(32) NOT NULL,
  `default_text` varchar(100) NOT NULL,
  `cachetime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_userapi_reply`
--

LOCK TABLES `ims_userapi_reply` WRITE;
/*!40000 ALTER TABLE `ims_userapi_reply` DISABLE KEYS */;
INSERT INTO `ims_userapi_reply` VALUES (1,1,'\"城市名+天气\", 如: \"北京天气\"','weather.php','','',0),(2,2,'\"百科+查询内容\" 或 \"定义+查询内容\", 如: \"百科姚明\", \"定义自行车\"','baike.php','','',0),(3,3,'\"@查询内容(中文或英文)\"','translate.php','','',0),(4,4,'\"日历\", \"万年历\", \"黄历\"或\"几号\"','calendar.php','','',0),(5,5,'\"新闻\"','news.php','','',0),(6,6,'\"快递+单号\", 如: \"申通1200041125\"','express.php','','',0);
/*!40000 ALTER TABLE `ims_userapi_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users`
--

DROP TABLE IF EXISTS `ims_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `groupid` int(10) unsigned NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(200) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `joindate` int(10) unsigned NOT NULL,
  `joinip` varchar(15) NOT NULL,
  `lastvisit` int(10) unsigned NOT NULL,
  `lastip` varchar(15) NOT NULL,
  `remark` varchar(500) NOT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users`
--

LOCK TABLES `ims_users` WRITE;
/*!40000 ALTER TABLE `ims_users` DISABLE KEYS */;
INSERT INTO `ims_users` VALUES (1,0,'admin','c9206c0277cc23ba7f6a9fd63130f26c1654c12b','3291ba89',0,1423820438,'',1433216654,'110.122.9.36',''),(7,1,'rocky','f5017bf47a00ef293869245fad710732368cd2f1','NiGcg890',1,1429348580,'223.240.134.247',1429348580,'223.240.134.247','');
/*!40000 ALTER TABLE `ims_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_group`
--

DROP TABLE IF EXISTS `ims_users_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `package` varchar(5000) NOT NULL,
  `maxaccount` int(10) unsigned NOT NULL,
  `maxsubaccount` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_group`
--

LOCK TABLES `ims_users_group` WRITE;
/*!40000 ALTER TABLE `ims_users_group` DISABLE KEYS */;
INSERT INTO `ims_users_group` VALUES (1,'体验用户组','a:1:{i:0;i:1;}',1,1),(2,'白金用户组','a:1:{i:0;i:1;}',2,2),(3,'黄金用户组','a:1:{i:0;i:1;}',3,3);
/*!40000 ALTER TABLE `ims_users_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_invitation`
--

DROP TABLE IF EXISTS `ims_users_invitation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_invitation` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(64) NOT NULL,
  `fromuid` int(10) unsigned NOT NULL,
  `inviteuid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_code` (`code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_invitation`
--

LOCK TABLES `ims_users_invitation` WRITE;
/*!40000 ALTER TABLE `ims_users_invitation` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_users_invitation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_permission`
--

DROP TABLE IF EXISTS `ims_users_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uniacid` int(10) unsigned NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_permission`
--

LOCK TABLES `ims_users_permission` WRITE;
/*!40000 ALTER TABLE `ims_users_permission` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_users_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_users_profile`
--

DROP TABLE IF EXISTS `ims_users_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_users_profile` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(10) unsigned NOT NULL,
  `createtime` int(10) unsigned NOT NULL,
  `realname` varchar(10) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `qq` varchar(15) NOT NULL,
  `mobile` varchar(11) NOT NULL,
  `fakeid` varchar(30) NOT NULL,
  `vip` tinyint(3) unsigned NOT NULL,
  `gender` tinyint(1) NOT NULL,
  `birthyear` smallint(6) unsigned NOT NULL,
  `birthmonth` tinyint(3) unsigned NOT NULL,
  `birthday` tinyint(3) unsigned NOT NULL,
  `constellation` varchar(10) NOT NULL,
  `zodiac` varchar(5) NOT NULL,
  `telephone` varchar(15) NOT NULL,
  `idcard` varchar(30) NOT NULL,
  `studentid` varchar(50) NOT NULL,
  `grade` varchar(10) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `nationality` varchar(30) NOT NULL,
  `resideprovince` varchar(30) NOT NULL,
  `residecity` varchar(30) NOT NULL,
  `residedist` varchar(30) NOT NULL,
  `graduateschool` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL,
  `education` varchar(10) NOT NULL,
  `occupation` varchar(30) NOT NULL,
  `position` varchar(30) NOT NULL,
  `revenue` varchar(10) NOT NULL,
  `affectivestatus` varchar(30) NOT NULL,
  `lookingfor` varchar(255) NOT NULL,
  `bloodtype` varchar(5) NOT NULL,
  `height` varchar(5) NOT NULL,
  `weight` varchar(5) NOT NULL,
  `alipay` varchar(30) NOT NULL,
  `msn` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `taobao` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `bio` text NOT NULL,
  `interest` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_users_profile`
--

LOCK TABLES `ims_users_profile` WRITE;
/*!40000 ALTER TABLE `ims_users_profile` DISABLE KEYS */;
INSERT INTO `ims_users_profile` VALUES (1,1,0,'李陌丰','李陌丰','','','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','',''),(6,7,1429348580,'李罗琦','白猫','','1285558992','','',0,0,0,0,0,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','');
/*!40000 ALTER TABLE `ims_users_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_video_reply`
--

DROP TABLE IF EXISTS `ims_video_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_video_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_video_reply`
--

LOCK TABLES `ims_video_reply` WRITE;
/*!40000 ALTER TABLE `ims_video_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_video_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_voice_reply`
--

DROP TABLE IF EXISTS `ims_voice_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_voice_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `mediaid` varchar(255) NOT NULL,
  `createtime` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_voice_reply`
--

LOCK TABLES `ims_voice_reply` WRITE;
/*!40000 ALTER TABLE `ims_voice_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_voice_reply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ims_we7_demo_reply`
--

DROP TABLE IF EXISTS `ims_we7_demo_reply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ims_we7_demo_reply` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rid` int(10) unsigned NOT NULL DEFAULT '0',
  `content` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_rid` (`rid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ims_we7_demo_reply`
--

LOCK TABLES `ims_we7_demo_reply` WRITE;
/*!40000 ALTER TABLE `ims_we7_demo_reply` DISABLE KEYS */;
/*!40000 ALTER TABLE `ims_we7_demo_reply` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-02 22:35:11
